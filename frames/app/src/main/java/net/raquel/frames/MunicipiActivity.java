package net.raquel.frames;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import net.raquel.frames.cosetes.Fitxers;
import net.raquel.frames.cosetes.Municipi;

public class MunicipiActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_LANDSCAPE) {
            finish();
            return;
        }

        setContentView(R.layout.activity_municipi);

        if (getIntent().hasExtra(Fitxers.MUNICIPI)) {
            MunipiciFragment fragmentMunicipi =
                    (MunipiciFragment) getFragmentManager().findFragmentById(R.id.FrgMunicipi);
            fragmentMunicipi.setMunicipi((Municipi) getIntent().getParcelableExtra(Fitxers.MUNICIPI));
        }
    }
}
