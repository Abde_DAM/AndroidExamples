package net.raquel.frames;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.raquel.frames.cosetes.Fitxers;
import net.raquel.frames.cosetes.Municipi;

import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnMunicipiListener}
 * interface.
 */
public class LlistaDeMunicipisFragment extends Fragment {

    private static final String ARG_NUMCOLUMNES = "NumeroColumnes";

    private int numColumnes = 1;
    private OnMunicipiListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public LlistaDeMunicipisFragment() {
    }


    @SuppressWarnings("unused")
    public static LlistaDeMunicipisFragment newInstance(int columnCount) {
        LlistaDeMunicipisFragment fragment = new LlistaDeMunicipisFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_NUMCOLUMNES, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            numColumnes = getArguments().getInt(ARG_NUMCOLUMNES);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_municipi_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (numColumnes <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, numColumnes));
            }
            recyclerView.setAdapter(new MunicipiRecyclerViewAdapter(Fitxers.obtenirMunicipis(context), mListener));
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMunicipiListener) {
            mListener = (OnMunicipiListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " cal implememntar OnMunicipiListener a l'activitat, burro!!");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMunicipiListener {

        void OnMunicipiTocat(Municipi municipi);
    }
}
