package net.raquel.frames;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import net.raquel.frames.cosetes.Fitxers;
import net.raquel.frames.cosetes.Municipi;

public class EstaticActivity extends AppCompatActivity implements LlistaDeMunicipisFragment.OnMunicipiListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estatic);
    }

    @Override
    public void OnMunicipiTocat(Municipi municipi) {
        MunipiciFragment fragmentMunicipi =
                (MunipiciFragment) getFragmentManager().findFragmentById(R.id.FrgMunicipi);
        if((fragmentMunicipi != null) && (fragmentMunicipi.isInLayout())) {
            fragmentMunicipi.setMunicipi(municipi);
        } else {
            Intent intent = new Intent(this,MunicipiActivity.class);
            intent.putExtra(Fitxers.MUNICIPI,municipi);
            startActivity(intent);
        }
    }
}
