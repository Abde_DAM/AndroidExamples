package net.raquel.frames;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.raquel.frames.cosetes.Fitxers;
import net.raquel.frames.cosetes.Municipi;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MunipiciFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MunipiciFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_MUNICIPI = "ARG_MUNICIPI";

    // TODO: Rename and change types of parameters
    private Municipi municipi;


    public MunipiciFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param municipi Parameter 1.
     * @return A new instance of fragment MunipiciFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MunipiciFragment newInstance(Municipi municipi) {
        MunipiciFragment fragment = new MunipiciFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_MUNICIPI, municipi);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            municipi = getArguments().getParcelable(ARG_MUNICIPI);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_munipici, container, false);
    }

    public void setMunicipi(Municipi municipi) {
        TextView tvMunicipi = (TextView)getView().findViewById(R.id.tvMunicipi);
        TextView tvProvincia = (TextView)getView().findViewById(R.id.tvProvincia);
        ImageView ivImatge = (ImageView)getView().findViewById(R.id.ivImatge);

        tvMunicipi.setText(municipi.getNomMunicipi());
        tvProvincia.setText(municipi.getNomProvincia());
        ivImatge.setImageResource(Fitxers.obtenirDrawableAleatori());
    }

}
