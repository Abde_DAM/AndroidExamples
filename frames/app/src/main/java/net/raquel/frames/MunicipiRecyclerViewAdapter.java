package net.raquel.frames;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.raquel.frames.cosetes.Municipi;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Municipi} and makes a call to the
 * specified {@link LlistaDeMunicipisFragment.OnMunicipiListener}.
 */
public class MunicipiRecyclerViewAdapter extends RecyclerView.Adapter<MunicipiRecyclerViewAdapter.ViewHolder> {

    private final List<Municipi> mValues;
    private final LlistaDeMunicipisFragment.OnMunicipiListener mListener;

    public MunicipiRecyclerViewAdapter(List<Municipi> items, LlistaDeMunicipisFragment.OnMunicipiListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_linia_llista_municipis, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.municipi = mValues.get(position);
        holder.tvMunicipi.setText(mValues.get(position).getNomMunicipi());
        holder.tvProvincia.setText(mValues.get(position).getNomProvincia());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.OnMunicipiTocat(holder.municipi);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvMunicipi;
        public final TextView tvProvincia;
        public Municipi municipi;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvMunicipi = (TextView) view.findViewById(R.id.tvMunicipi);
            tvProvincia = (TextView) view.findViewById(R.id.tvProvincia);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvMunicipi.getText() + "'";
        }
    }
}
