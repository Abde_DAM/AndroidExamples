package net.raquel.frames;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void btnEstatics_click(View view) {
        Intent intent = new Intent(this,EstaticActivity.class);
        startActivity(intent);
    }

    public void btnDinamics_click(View view) {
        Intent intent2 = new Intent(this,DinamicActivity.class);
        startActivity(intent2);
    }

    //region menus
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.btnDinamics:

                break;
            case R.id.btnEstatics:
                Intent intent = new Intent(this,EstaticActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
