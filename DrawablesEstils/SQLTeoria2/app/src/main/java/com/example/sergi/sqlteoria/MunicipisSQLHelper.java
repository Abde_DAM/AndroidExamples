package com.example.sergi.sqlteoria;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Sergi on 14/03/2016.
 */
public class MunicipisSQLHelper extends SQLiteOpenHelper
{
    //region CONSTRUCTORS
    public MunicipisSQLHelper(Context context)
    {
        super(context, AppConstants.NOM_DB, null, AppConstants.VERSIO_DB);
    }

    public MunicipisSQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
    }

    public MunicipisSQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler)
    {
        super(context, name, factory, version, errorHandler);
    }
    //endregion

    //region EVENTS
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(AppConstants.sqlCreateProvincies);
        db.execSQL(AppConstants.sqlCreateMunicipis);
        //db.execSQL(AppConstants.sqlRelacions);
        //db.execSQL(AppConstants.sqlPragma);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }
    //endregion
}
