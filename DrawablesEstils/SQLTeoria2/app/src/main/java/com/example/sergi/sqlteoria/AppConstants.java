package com.example.sergi.sqlteoria;

import android.view.textservice.SentenceSuggestionsInfo;

import Model.Municipi;

/**
 * Created by Sergi on 14/03/2016.
 */
public class AppConstants
{
    //region SQLITE
    public final static String sqlRelacions = "ALTER TABLE " + AppConstants.TAULA_MUNICIPIS +
            " add constraint fk_1 foreign key (" + AppConstants.COLUMNA_ID_PROVINCIA +
            ") references " + AppConstants.TAULA_PROVINCIES + "(" + AppConstants.COLUMNA_ID_PROVINCIA + ");";
    public static final String sqlPragma = "PRAGMA foreign_keys=ON;";
    public final static String NOM_DB = "BBDDMunicipis";
    public final static int VERSIO_DB = 1;

    //region PROVINCIES
    public static final String TAULA_PROVINCIES = "Provincies";
    public static final String COLUMNA_ID = "_id";//Primera columna per Android, SEMPRE _id!!!!
    public static final String COLUMNA_ID_PROVINCIA = "id_provincia";
    public static final String COLUMNA_NOM_PROVINCIA = "provincia";
    //endregion

    //region MUNICIPIS
    public static final String TAULA_MUNICIPIS = "Municipis";
    public static final String COLUMNA_ID_MUNICIPIS = "id_municipi";
    public static final String COLUMNA_NOM_MUNICIPIS = "municipi";
    //endregion

    //region CREATE SENTENCES
    public static final  String sqlCreateProvincies = "CREATE TABLE " + AppConstants.TAULA_PROVINCIES + " (" + AppConstants.COLUMNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            AppConstants.COLUMNA_ID_PROVINCIA + " TEXT NOT NULL, " + AppConstants.COLUMNA_NOM_PROVINCIA + " TEXT);";

    public static final  String sqlCreateMunicipis = "CREATE TABLE " + AppConstants.TAULA_MUNICIPIS + " (" + AppConstants.COLUMNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            AppConstants.COLUMNA_ID_MUNICIPIS + " TEXT NOT NULL, " +AppConstants.COLUMNA_ID_PROVINCIA +" TEXT NOT NULL, " + AppConstants.COLUMNA_NOM_MUNICIPIS + " TEXT);";
    //endregion

    //region DELETE SENTENCES
    public final static String deleteProvinicies = "DELETE FROM "+ AppConstants.TAULA_PROVINCIES;

    public final static String deleteMunicipis = "DELETE FROM "+ AppConstants.TAULA_MUNICIPIS;
    //endregion

    //endregion
}
