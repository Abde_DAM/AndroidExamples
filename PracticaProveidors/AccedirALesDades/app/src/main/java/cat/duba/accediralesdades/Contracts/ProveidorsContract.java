package cat.duba.accediralesdades.Contracts;

import android.content.UriMatcher;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by abde on 5/04/16.
 */
public class ProveidorsContract {
    private ProveidorsContract()
    {}

    public static final String ESQUEMA = "content://";

    public static final String AUTHORITY = "proveidor_dades.Proveidor";

    public static final String BASE_PATH = "proveidors";

    public static final Uri CONTENT_URI = Uri.parse(ESQUEMA+AUTHORITY+"/"+BASE_PATH);

    public static final int TOTES_LES_FILES = 1;

    public static final int UNA_FILA = 2;

    public static final UriMatcher sUriMatcher;
    static
    {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH, TOTES_LES_FILES);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH+"/#", UNA_FILA);
    }

    public final static class Columnes implements BaseColumns
    {
        public static final String PROVEEDORES_ID_PROVEEDOR="id_proveedor",
                PROVEEDORES_NOMBRE_COMPANIA="nombre_compania",
                PROVEEDORES_NOMBRE_CONTACTO="nombre_contacto",
                PROVEEDORES_CARGO_CONTACTO="cargo_contacto",
                PROVEEDORES_DIRECCION="direccion",
                PROVEEDORES_CIUDAD="ciudad",
                PROVEEDORES_REGION="region",
                PROVEEDORES_COD_POSTAL="codpostal",
                PROVEEDORES_PAIS="pais",
                PROVEEDORES_TELEFONO="telefono",
                PROVEEDORES_FAX="fax",
                PROVEEDORES_PAGINA_PRINCIPAL="pagina_principal";
    }

    public final static String MIME_UNA = "vnd.android.cursos.item/vnd." +AUTHORITY+"/"+BASE_PATH;

    public final static String MIME_MULTIPLE = "vnd.android.cursos.dir/vnd." +AUTHORITY+"/"+BASE_PATH;
}
