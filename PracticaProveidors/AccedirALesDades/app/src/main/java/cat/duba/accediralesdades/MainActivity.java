package cat.duba.accediralesdades;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btnCategorias, btnProveedores, btnProductos, btnPedidos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCategorias= (Button) findViewById(R.id.btnCategorias);
        btnProveedores= (Button) findViewById(R.id.btnProveedores);
        btnProductos= (Button) findViewById(R.id.btnProductos);
        btnPedidos= (Button) findViewById(R.id.btnPedidos);

        btnPedidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(),ComandesActivity.class));
            }
        });

        btnProductos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), ProductesActivity.class));
            }
        });

        btnProveedores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), Proveidors.class));
            }
        });


        btnCategorias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), Categories.class));
            }
        });

    }
}
