package cat.duba.accediralesdades.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import cat.duba.accediralesdades.Contracts.CategoriesContract;
import cat.duba.accediralesdades.R;

/**
 * Created by abde on 5/04/16.
 */
public class AdaptadorCategories extends CursorAdapter {
    private LayoutInflater cursorInflater;

    public AdaptadorCategories(Context context, Cursor c) {
        super(context, c);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public AdaptadorCategories(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public AdaptadorCategories(Context context, Cursor c, int flags) {
        super(context, c, flags);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return cursorInflater.inflate(R.layout.categories_view_adapter,parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tvCategoria=(TextView)view.findViewById(R.id.tvCategoria);
        TextView tvNombreCategoria=(TextView)view.findViewById(R.id.tvNombreCategoria);
        TextView tvDescripcion=(TextView)view.findViewById(R.id.tvDescripcion);

        tvCategoria.setText(cursor.getString(cursor.getColumnIndex(CategoriesContract.Columnes.CATEGORIAS_ID_CATEGORIA)));
        tvNombreCategoria.setText(cursor.getString(cursor.getColumnIndex(CategoriesContract.Columnes.CATEGORIAS_NOMBRE_CATEGORIA)));
        tvDescripcion.setText(cursor.getString(cursor.getColumnIndex(CategoriesContract.Columnes.CATEGORIAS_DESCRIPCION)));
    }
}
