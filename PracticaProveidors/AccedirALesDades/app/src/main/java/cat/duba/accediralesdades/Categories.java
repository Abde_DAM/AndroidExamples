package cat.duba.accediralesdades;

import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import cat.duba.accediralesdades.Fragments.CategoriesDetall;
import cat.duba.accediralesdades.Fragments.CategoriesMestre;

public class Categories extends AppCompatActivity {
    FrameLayout flPrimer;
    CategoriesMestre categoriesMestre;
    CategoriesDetall categoriesDetall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        flPrimer= (FrameLayout) findViewById(R.id.flPrimer);

        if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE)
        {
            categoriesDetall=new CategoriesDetall();

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(flPrimer.getId(), categoriesDetall).commit();
        }
        else
        {
            categoriesMestre=new CategoriesMestre();

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(flPrimer.getId(), categoriesMestre).commit();
        }
    }
}
