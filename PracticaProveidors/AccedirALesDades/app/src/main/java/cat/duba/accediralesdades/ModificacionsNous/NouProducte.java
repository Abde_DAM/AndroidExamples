package cat.duba.accediralesdades.ModificacionsNous;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import cat.duba.accediralesdades.Contracts.ProductesContract;
import cat.duba.accediralesdades.ProductesActivity;
import cat.duba.accediralesdades.Proveidors;
import cat.duba.accediralesdades.R;


public class NouProducte extends AppCompatActivity {
    private Button btnGuardar;
    private int idAnterior;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nou_producte);
        btnGuardar= (Button) findViewById(R.id.btnGuardar);

        final EditText etNombreProducto = (EditText) findViewById(R.id.etNombreProducto);
        final EditText etSuspendido= (EditText) findViewById(R.id.etSuspendido);
        final EditText etCantidadPorUnidad = (EditText) findViewById(R.id.etCantidadPorUnidad);
        final EditText etPrecioPorUnidad = (EditText) findViewById(R.id.etPrecioPorUnidad);
        final EditText etIdProducto= (EditText) findViewById(R.id.etIdProducto);
        final EditText etIdProveedor= (EditText) findViewById(R.id.etIdProveedor);
        final EditText etIdCategoria= (EditText) findViewById(R.id.etIdCategoria);
        final EditText etUnidadesEnExistencia= (EditText) findViewById(R.id.etUnidadesEnExistencia);
        final EditText etUnidadesEnPedido= (EditText) findViewById(R.id.etUnidadesEnPedido);
        final EditText etNivelNuevoPedido= (EditText) findViewById(R.id.etNivelNuevoPedido);


        btnGuardar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ContentValues contentValues=new ContentValues();
            contentValues.put(ProductesContract.Columnes.PRODUCTOS_ID_PRODUCTO, Integer.valueOf(etIdProducto.getText().toString()));
            contentValues.put(ProductesContract.Columnes.PRODUCTOS_NOMBRE_PRODUCTO, etNombreProducto.getText().toString());
            if(!etIdProveedor.getText().toString().equals(""))
                contentValues.put(ProductesContract.Columnes.PRODUCTOS_ID_PROVEEDOR, Integer.valueOf(etIdProveedor.getText().toString()));
            if(!etIdCategoria.getText().toString().equals(""))
                contentValues.put(ProductesContract.Columnes.PRODUCTOS_ID_CATEGORIA, Integer.valueOf(etIdCategoria.getText().toString()));
            contentValues.put(ProductesContract.Columnes.PRODUCTOS_CANTIDAD_POR_UNIDAD, etCantidadPorUnidad.getText().toString());
            if(!etPrecioPorUnidad.getText().toString().equals(""))
                contentValues.put(ProductesContract.Columnes.PRODUCTOS_PRECIO_UNIDAD, Double.valueOf(etPrecioPorUnidad.getText().toString()));
            if(!etUnidadesEnExistencia.getText().toString().equals(""))
                contentValues.put(ProductesContract.Columnes.PRODUCTOS_UNIDADES_EN_EXISTENCIA, Integer.valueOf(etUnidadesEnExistencia.getText().toString()));
            if(!etUnidadesEnPedido.getText().toString().equals(""))
                contentValues.put(ProductesContract.Columnes.PRODUCTOS_UNIDADES_EN_PEDIDO, Integer.valueOf(etUnidadesEnPedido.getText().toString()));
            if(!etNivelNuevoPedido.getText().toString().equals(""))
                contentValues.put(ProductesContract.Columnes.PRODUCTOS_NIVEL_NUEVO_PEDIDO, Integer.valueOf(etNivelNuevoPedido.getText().toString()));
            if(!etSuspendido.getText().toString().equals(""))
                contentValues.put(ProductesContract.Columnes.PRODUCTOS_SUSPENDIDO, Integer.valueOf(etSuspendido.getText().toString()));


            NouProducte.this.getContentResolver().insert(ProductesContract.CONTENT_URI, contentValues);

            finish();
        }
        });

    }
}
