package cat.duba.accediralesdades.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import cat.duba.accediralesdades.Contracts.CategoriesContract;
import cat.duba.accediralesdades.Contracts.DetallesPedidoContract;
import cat.duba.accediralesdades.R;

/**
 * Created by abde on 8/04/16.
 */
public class AdaptadorDetalles extends CursorAdapter {
    private LayoutInflater cursorInflater;

    public AdaptadorDetalles(Context context, Cursor c) {
        super(context, c);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public AdaptadorDetalles(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public AdaptadorDetalles(Context context, Cursor c, int flags) {
        super(context, c, flags);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return cursorInflater.inflate(R.layout.detalles_pedidos_view_adapter,parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tvIdProducto= (TextView) view.findViewById(R.id.tvIdProducto);
        TextView tvPrecioPorUnidad= (TextView) view.findViewById(R.id.tvPrecioPorUnidad);
        TextView tvCantidad= (TextView) view.findViewById(R.id.tvCantidad);
        TextView tvDescuento= (TextView) view.findViewById(R.id.tvDescuento);
        //TextView tvPrecioConDescuento= (TextView) view.findViewById(R.id.tvPrecioConDescuento);

        tvIdProducto.setText("ID Pedido: "+cursor.getString(cursor.getColumnIndex(DetallesPedidoContract.Columnes.DETALLES_ID_PEDIDO)));
        tvPrecioPorUnidad.setText("Precio por unidad: "+cursor.getString(cursor.getColumnIndex(DetallesPedidoContract.Columnes.DETALLES_PRECIO_UNIDAD)));
        tvCantidad.setText("Cantidad: "+cursor.getString(cursor.getColumnIndex(DetallesPedidoContract.Columnes.DETALLES_CANTIDAD)));
        tvDescuento.setText("Descuento del "+cursor.getString(cursor.getColumnIndex(DetallesPedidoContract.Columnes.DETALLES_DESCUENTO)));
        /*double precio=detalles.get(position).getCantidad()*detalles.get(position).getPrecioUnidad(), descuento=detalles.get(position).getDescuento();
        tvPrecioConDescuento.setText("Total con descuento: "+String.valueOf(precio-((precio*descuento)/100))+"€");*/
    }
}