package cat.duba.accediralesdades.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import cat.duba.accediralesdades.Contracts.CategoriesContract;
import cat.duba.accediralesdades.Contracts.ProductesContract;
import cat.duba.accediralesdades.R;

/**
 * Created by abde on 5/04/16.
 */
public class AdaptadorProductes extends CursorAdapter {
    private LayoutInflater cursorInflater;

    public AdaptadorProductes(Context context, Cursor c) {
        super(context, c);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public AdaptadorProductes(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public AdaptadorProductes(Context context, Cursor c, int flags) {
        super(context, c, flags);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return cursorInflater.inflate(R.layout.productes_per_categoria_view_adapter,parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tvIdProducto= (TextView) view.findViewById(R.id.tvIdProducto);
        TextView tvNombreProducto=(TextView)view.findViewById(R.id.tvNombreProducto);
        TextView tvSuspendido=(TextView)view.findViewById(R.id.tvSuspendido);
        TextView tvCantidadPorUnidad=(TextView)view.findViewById(R.id.tvCantidadPorUnidad);
        TextView tvPrecioPorUnidad=(TextView)view.findViewById(R.id.tvPrecioPorUnidad);


        tvIdProducto.setText(cursor.getString(cursor.getColumnIndex(ProductesContract.Columnes.PRODUCTOS_ID_PRODUCTO)));
        tvNombreProducto.setText(cursor.getString(cursor.getColumnIndex(ProductesContract.Columnes.PRODUCTOS_NOMBRE_PRODUCTO)));
        tvCantidadPorUnidad.setText(cursor.getString(cursor.getColumnIndex(ProductesContract.Columnes.PRODUCTOS_CANTIDAD_POR_UNIDAD)));
        tvPrecioPorUnidad.setText(cursor.getString(cursor.getColumnIndex(ProductesContract.Columnes.PRODUCTOS_PRECIO_UNIDAD)));
        if(cursor.getInt(cursor.getColumnIndex(ProductesContract.Columnes.PRODUCTOS_SUSPENDIDO))==1)
        {
            tvSuspendido.setText("SUSPENDIDO");
        }
        else
        {
            tvSuspendido.setText("NO SUSPENDIDO");
        }

        /*    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:id="@+id/tvNombreProducto"/>
    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:id="@+id/tvSuspendido"/>
    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:id="@+id/tvCantidadPorUnidad"/>
    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:id="@+id/tvPrecioPorUnidad"/>*/
    }
}
