package cat.duba.accediralesdades.ModificacionsNous;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import cat.duba.accediralesdades.Contracts.ProveidorsContract;
import cat.duba.accediralesdades.Proveidors;
import cat.duba.accediralesdades.R;

public class NouProveidor extends AppCompatActivity {
    EditText etIdProveidor, etNombreCompania, etNombreContacto, etCargoContacto, etDireccion, etCiudad, etRegion, etCodPostal, etPais, etTelefono, etFax, etPagPrincipal;
    Button btnGuardar;
    Intent intent;
    int idInicial;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nou_proveidor);
        etIdProveidor= (EditText) findViewById(R.id.etIdProveidor);
        etNombreCompania= (EditText) findViewById(R.id.etNombreCompania);
        etNombreContacto= (EditText) findViewById(R.id.etNombreContacto);
        etCargoContacto= (EditText) findViewById(R.id.etCargoContacto);
        etDireccion= (EditText) findViewById(R.id.etDireccion);
        etCiudad= (EditText) findViewById(R.id.etCiudad);
        etRegion= (EditText) findViewById(R.id.etRegion);
        etCodPostal= (EditText) findViewById(R.id.etCodPostal);
        etPais= (EditText) findViewById(R.id.etPais);
        etTelefono= (EditText) findViewById(R.id.etTelefono);
        etFax= (EditText) findViewById(R.id.etFax);
        etPagPrincipal= (EditText) findViewById(R.id.etPagPrincipal);
        btnGuardar= (Button) findViewById(R.id.btnGuardar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    ContentValues contentValues=new ContentValues();
                    contentValues.put(ProveidorsContract.Columnes.PROVEEDORES_ID_PROVEEDOR, Integer.parseInt(etIdProveidor.getText().toString()));
                    contentValues.put(ProveidorsContract.Columnes.PROVEEDORES_NOMBRE_COMPANIA, etNombreCompania.getText().toString());
                    contentValues.put(ProveidorsContract.Columnes.PROVEEDORES_NOMBRE_CONTACTO, etNombreContacto.getText().toString());
                    contentValues.put(ProveidorsContract.Columnes.PROVEEDORES_CARGO_CONTACTO, etCargoContacto.getText().toString());
                    contentValues.put(ProveidorsContract.Columnes.PROVEEDORES_DIRECCION, etDireccion.getText().toString());
                    contentValues.put(ProveidorsContract.Columnes.PROVEEDORES_CIUDAD, etCiudad.getText().toString());
                    contentValues.put(ProveidorsContract.Columnes.PROVEEDORES_REGION, etRegion.getText().toString());
                    contentValues.put(ProveidorsContract.Columnes.PROVEEDORES_COD_POSTAL, etCodPostal.getText().toString());
                    contentValues.put(ProveidorsContract.Columnes.PROVEEDORES_PAIS, etPais.getText().toString());
                    contentValues.put(ProveidorsContract.Columnes.PROVEEDORES_TELEFONO, etTelefono.getText().toString());
                    contentValues.put(ProveidorsContract.Columnes.PROVEEDORES_FAX, etNombreContacto.getText().toString());
                    contentValues.put(ProveidorsContract.Columnes.PROVEEDORES_PAGINA_PRINCIPAL, etPagPrincipal.getText().toString());

                    NouProveidor.this.getContentResolver().insert(ProveidorsContract.CONTENT_URI, contentValues);
                    finish();
            }
        });
    }
}
