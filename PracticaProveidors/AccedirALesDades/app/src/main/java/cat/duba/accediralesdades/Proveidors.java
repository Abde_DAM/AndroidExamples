package cat.duba.accediralesdades;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import cat.duba.accediralesdades.Adapters.AdaptadorCategories;
import cat.duba.accediralesdades.Adapters.AdaptadorProveidors;
import cat.duba.accediralesdades.Contracts.ProductesContract;
import cat.duba.accediralesdades.Contracts.ProveidorsContract;
import cat.duba.accediralesdades.ModificacionsNous.NouProveidor;

public class Proveidors extends AppCompatActivity {
    ListView lvProveedores;
    Cursor cursorProveidors, cursorProductes;
    public final static String MODIFICARPROVEIDOR="proveidorAModificar", ESMODIFICAT="EsModificat";
    public static final String IDPROVEIDOR ="id_proveidor";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proveidors);
        lvProveedores= (ListView) findViewById(R.id.lvProveedores);
        cursorProveidors=getContentResolver().query(ProveidorsContract.CONTENT_URI, null, null, null, null);
        lvProveedores.setAdapter(new AdaptadorProveidors(this, cursorProveidors));
        lvProveedores.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                String[] opcions = new String[]{"Eliminar"};
                ListView llista = new ListView(Proveidors.this);
                llista.setAdapter(new ArrayAdapter<String>(Proveidors.this, android.R.layout.simple_list_item_1, opcions));
                final AlertDialog dialog = (new AlertDialog.Builder(Proveidors.this)
                        .setTitle("Elige una opción")
                        .setView(llista)
                        .setCancelable(true)
                        .create());
                dialog.show();
                llista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int positionB, long id) {
                        switch (positionB) {
                            case 0:
                                AdaptadorProveidors adaptador = (AdaptadorProveidors) lvProveedores.getAdapter();
                                Cursor cursorSpinner = adaptador.getCursor();
                                /*categoriaSeleccionada =*/ //cursorSpinner.getString(1);


                                cursorProductes = Proveidors.this.getContentResolver().query(ProductesContract.CONTENT_URI, null, ProductesContract.Columnes.PRODUCTOS_ID_PROVEEDOR + " = " + cursorSpinner.getString(1), null, null);
                                int depenen = cursorProductes.getCount();// proveedoresDAO.EnDepenen((int) lvProveedores.getItemIdAtPosition(position));
                                if (depenen == 0) {
                                    Proveidors.this.getContentResolver().delete(ProveidorsContract.CONTENT_URI, ProveidorsContract.Columnes.PROVEEDORES_ID_PROVEEDOR + " = " + cursorSpinner.getString(1), null);//proveedoresDAO.eliminaProveedores((int) lvProveedores.getItemIdAtPosition(position));
                                    Toast.makeText(Proveidors.this, "Proveedor eliminado", Toast.LENGTH_LONG).show();
                                    lvProveedores.setAdapter(new AdaptadorProveidors(Proveidors.this, cursorProveidors));
                                    dialog.dismiss();
                                } else {
                                    Toast.makeText(Proveidors.this, "No se puede eliminar este proveedor " + depenen, Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }
                                break;
                        }
                    }
                });


                return false;
            }
        });
        lvProveedores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String proveidorSeleccionat;
                AdaptadorProveidors adaptador = (AdaptadorProveidors) lvProveedores.getAdapter();
                Cursor cursorSpinner = adaptador.getCursor();
                proveidorSeleccionat = cursorSpinner.getString(1);
                Intent intent = new Intent(Proveidors.this, ProductesPerProveidor.class);
                intent.putExtra(IDPROVEIDOR, proveidorSeleccionat);
                startActivity(intent);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_proveidors, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnuAlta:
                startActivity(new Intent(this, NouProveidor.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
