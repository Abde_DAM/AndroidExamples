package cat.duba.accediralesdades;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import cat.duba.accediralesdades.Adapters.AdaptadorProductes;
import cat.duba.accediralesdades.Adapters.AdaptadorProveidors;
import cat.duba.accediralesdades.Contracts.ProductesContract;
import cat.duba.accediralesdades.ModificacionsNous.NouProducte;
import cat.duba.accediralesdades.ModificacionsNous.NouProveidor;

public class ProductesActivity extends AppCompatActivity {
    ListView lvProductes;
    Cursor cursorProductes, cursorPedidos;
    public static final String MODIFICARPRODUCTE="producteAModificar", ESMODIFICAT="EsModificat";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producte);


        lvProductes= (ListView) findViewById(R.id.lvProductes);
        cursorProductes=getContentResolver().query(ProductesContract.CONTENT_URI, null, null, null, null);

        lvProductes.setAdapter(new AdaptadorProductes(this, cursorProductes, 0));

        lvProductes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                String[] opcions = new String[]{"Eliminar"};
                ListView llista = new ListView(ProductesActivity.this);
                llista.setAdapter(new ArrayAdapter<String>(ProductesActivity.this, android.R.layout.simple_list_item_1, opcions));
                final AlertDialog dialog = (new AlertDialog.Builder(ProductesActivity.this)
                        .setTitle("Elige una opción")
                        .setView(llista)
                        .setCancelable(true)
                        .create());
                dialog.show();
                llista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int positionB, long id) {
                        switch (positionB) {
                            case 0:
                                AdaptadorProductes adaptador = (AdaptadorProductes) lvProductes.getAdapter();
                                Cursor cursorSpinner = adaptador.getCursor();

                                cursorPedidos=ProductesActivity.this.getContentResolver().query(ProductesContract.CONTENT_URI, null, ProductesContract.Columnes.PRODUCTOS_ID_PROVEEDOR+ " = " + cursorSpinner.getString(1), null, null);
                                int depenen = cursorPedidos.getCount();//productosDAO.EnDepenen((int) lvProductes.getItemIdAtPosition(position));
                                if (depenen == 0) {
                                    ProductesActivity.this.getContentResolver().delete(ProductesContract.CONTENT_URI, ProductesContract.Columnes.PRODUCTOS_ID_PRODUCTO+" = "+cursorSpinner.getString(1),null);//proveedoresDAO.eliminaProveedores((int) lvProveedores.getItemIdAtPosition(position));
                                    //productosDAO.eliminaProductos((int) lvProductes.getItemIdAtPosition(position));
                                    Toast.makeText(ProductesActivity.this, "Producto eliminado", Toast.LENGTH_LONG).show();
                                    lvProductes.setAdapter(new AdaptadorProductes(ProductesActivity.this, cursorProductes, 0));
                                    dialog.dismiss();
                                } else {
                                    Toast.makeText(ProductesActivity.this, "No se puede eliminar este producto " + depenen, Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }
                                break;
                        }
                    }
                });
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_proveidors, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnuAlta:
                startActivity(new Intent(this, NouProducte.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
