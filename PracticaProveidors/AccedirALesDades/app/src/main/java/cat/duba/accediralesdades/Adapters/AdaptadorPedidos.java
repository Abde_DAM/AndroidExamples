package cat.duba.accediralesdades.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import cat.duba.accediralesdades.Contracts.ComandesContract;
import cat.duba.accediralesdades.R;

/**
 * Created by abde on 7/04/16.
 */
public class AdaptadorPedidos extends CursorAdapter {
    LayoutInflater cursorInflater;
    public AdaptadorPedidos(Context context, Cursor c) {
        super(context, c);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public AdaptadorPedidos(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public AdaptadorPedidos(Context context, Cursor c, int flags) {
        super(context, c, flags);
        cursorInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return cursorInflater.inflate(R.layout.pedidos_view_adapter,parent, false);

    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tvIdPedido=(TextView)view.findViewById(R.id.tvIdPedido);
        TextView tvIdEmpleado=(TextView)view.findViewById(R.id.tvIdEmpleado);
        TextView tvFechaPedido=(TextView)view.findViewById(R.id.tvFechaPedido);
        TextView tvIdCliente=(TextView)view.findViewById(R.id.tvIdCliente);
        TextView tvFechaEntrega=(TextView)view.findViewById(R.id.tvFechaEntrega);
        TextView tvFechaEnvio=(TextView)view.findViewById(R.id.tvFechaEnvio);
        TextView tvDesinatario=(TextView)view.findViewById(R.id.tvDesinatario);
        TextView tvDireccionDestinatario=(TextView)view.findViewById(R.id.tvDireccionDestinatario);
        TextView tvFormaEnvio=(TextView)view.findViewById(R.id.tvFormaEnvio);

        tvIdPedido.setText("ID Pedido: "+ cursor.getString(cursor.getColumnIndex(ComandesContract.Columnes.PEDIDOS_ID_PEDIDO)));
        tvIdEmpleado.setText("ID Empleado: "+cursor.getString(cursor.getColumnIndex(ComandesContract.Columnes.PEDIDOS_ID_EMPLEADO)));
        tvFechaPedido.setText("Fecha Pedido: "+cursor.getString(cursor.getColumnIndex(ComandesContract.Columnes.PEDIDOS_FECHA_PEDIDO)));
        tvIdCliente.setText("ID Cliente: "+cursor.getString(cursor.getColumnIndex(ComandesContract.Columnes.PEDIDOS_ID_CLIENTE)));
        tvFechaEntrega.setText("Fecha entrega: "+cursor.getString(cursor.getColumnIndex(ComandesContract.Columnes.PEDIDOS_FECHA_ENTREGA)));
        tvFechaEnvio.setText("Fecha envio: "+cursor.getString(cursor.getColumnIndex(ComandesContract.Columnes.PEDIDOS_FECHA_ENVIO)));
        tvDesinatario.setText("Destinatario: "+cursor.getString(cursor.getColumnIndex(ComandesContract.Columnes.PEDIDOS_DESTINATARIO)));
        tvDireccionDestinatario.setText("Dirección: " + cursor.getString(cursor.getColumnIndex(ComandesContract.Columnes.PEDIDOS_DIRECCION_DESTINATARIO)));
        tvFormaEnvio.setText("Forma de envío: "+cursor.getString(cursor.getColumnIndex(ComandesContract.Columnes.PEDIDOS_FORMA_ENVIO)));
    }
}
