package cat.duba.accediralesdades;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import cat.duba.accediralesdades.Adapters.AdaptadorDetalles;
import cat.duba.accediralesdades.Contracts.DetallesPedidoContract;

public class DetallesPorPedido extends AppCompatActivity {
    ListView lvDetallesPedido;
    Cursor cursorDetalles;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_por_pedido);
        Intent intent=getIntent();
        lvDetallesPedido= (ListView) findViewById(R.id.lvDetallesPedido);
        String pedidoSeleccionado;
        pedidoSeleccionado = intent.getStringExtra(ComandesActivity.IDPEDIDO);
        cursorDetalles=getContentResolver().query(DetallesPedidoContract.CONTENT_URI, null, DetallesPedidoContract.Columnes.DETALLES_ID_PEDIDO+ " = " + pedidoSeleccionado, null, null);
        lvDetallesPedido.setAdapter(new AdaptadorDetalles(this, cursorDetalles));
    }
}
