package cat.duba.accediralesdades.ModificacionsNous;

import android.content.ContentValues;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import cat.duba.accediralesdades.Contracts.ComandesContract;
import cat.duba.accediralesdades.R;

public class NouPedido extends AppCompatActivity {
    Button btnGuardar;
    int idAnterior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nou_pedido);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        final EditText etIdPedido = (EditText) findViewById(R.id.etIdPedido);
        final EditText etIdEmpleado = (EditText) findViewById(R.id.etIdEmpleado);
        final EditText etFechaPedido = (EditText) findViewById(R.id.etFechaPedido);
        final EditText etIdCliente = (EditText) findViewById(R.id.etIdCliente);
        final EditText etFechaEntrega = (EditText) findViewById(R.id.etFechaEntrega);
        final EditText etFechaEnvio = (EditText) findViewById(R.id.etFechaEnvio);
        final EditText etFormaEnvio = (EditText) findViewById(R.id.etFormaEnvio);
        final EditText etDestinatario = (EditText) findViewById(R.id.etDestinatario);
        final EditText etDireccionDestinatario = (EditText) findViewById(R.id.etDireccionDestinatario);
        final EditText etCiudadDestinatario = (EditText) findViewById(R.id.etCiudadDestinatario);
        final EditText etRegionDestinatario = (EditText) findViewById(R.id.etRegionDestinatario);
        final EditText etCodPostalDestinatario = (EditText) findViewById(R.id.etCodPostalDestinatario);
        final EditText etPaisDestinatario = (EditText) findViewById(R.id.etPaisDestinatario);
        final EditText etCargo = (EditText) findViewById(R.id.etCargo);


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues contentValues=new ContentValues();
                if(!etIdPedido.getText().toString().equals(""))
                    contentValues.put(ComandesContract.Columnes.PEDIDOS_ID_PEDIDO, Integer.parseInt(etIdPedido.getText().toString()));
                contentValues.put(ComandesContract.Columnes.PEDIDOS_ID_CLIENTE, etIdCliente.getText().toString());
                if(!etIdEmpleado.getText().toString().equals(""))
                    contentValues.put(ComandesContract.Columnes.PEDIDOS_ID_EMPLEADO, Integer.valueOf(etIdEmpleado.getText().toString()));

                contentValues.put(ComandesContract.Columnes.PEDIDOS_FECHA_PEDIDO, etFechaPedido.getText().toString());
                contentValues.put(ComandesContract.Columnes.PEDIDOS_FECHA_ENTREGA, etFechaEntrega.getText().toString());
                contentValues.put(ComandesContract.Columnes.PEDIDOS_FECHA_ENVIO, etFechaEnvio.getText().toString());
                contentValues.put(ComandesContract.Columnes.PEDIDOS_FORMA_ENVIO, etFormaEnvio.getText().toString());
                if(!etCargo.getText().toString().equals(""))
                    contentValues.put(ComandesContract.Columnes.PEDIDOS_CARGO, Double.valueOf(etCargo.getText().toString()));
                contentValues.put(ComandesContract.Columnes.PEDIDOS_CIUDAD_DESTINATARIO, etDestinatario.getText().toString());
                contentValues.put(ComandesContract.Columnes.PEDIDOS_DIRECCION_DESTINATARIO, etDireccionDestinatario.getText().toString());
                contentValues.put(ComandesContract.Columnes.PEDIDOS_CIUDAD_DESTINATARIO, etCiudadDestinatario.getText().toString());
                contentValues.put(ComandesContract.Columnes.PEDIDOS_REGION_DESTINATARIO, etRegionDestinatario.getText().toString());
                contentValues.put(ComandesContract.Columnes.PEDIDOS_CODPOSTAL_DESTINATARIO, etCodPostalDestinatario.getText().toString());
                contentValues.put(ComandesContract.Columnes.PEDIDOS_PAIS_DESTINATARIO, etPaisDestinatario.getText().toString());

                NouPedido.this.getContentResolver().insert(ComandesContract.CONTENT_URI, contentValues);
                finish();
            }

        });
    }
}
