package com.example.informatica.practicaneptuno;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.informatica.practicaneptuno.Adaptadors.AdaptadorProductesComplet;
import com.example.informatica.practicaneptuno.DAO.ProductosDAO;
import com.example.informatica.practicaneptuno.POJO.Productos;

public class ProductesActivity extends AppCompatActivity {
    ListView lvProductes;
    ProductosDAO productosDAO;
    static final String MODIFICARPRODUCTE="producteAModificar", ESMODIFICAT="EsModificat";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productes);
        productosDAO=new ProductosDAO(this);
        productosDAO.obre();
        lvProductes= (ListView) findViewById(R.id.lvProductes);
        lvProductes.setAdapter(new AdaptadorProductesComplet(productosDAO.obteProductos(), this));
        lvProductes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                String[] opcions=new String[]{"Modificar", "Eliminar"};
                ListView llista=new ListView(ProductesActivity.this);
                llista.setAdapter(new ArrayAdapter<String>(ProductesActivity.this, android.R.layout.simple_list_item_1, opcions));
                final AlertDialog dialog=(new AlertDialog.Builder(ProductesActivity.this)
                        .setTitle("Elige una opción")
                        .setView(llista)
                        .setCancelable(true)
                        .create());
                dialog.show();
                llista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int positionB, long id) {
                        switch (positionB) {
                            case 0:
                                Intent intent = new Intent(ProductesActivity.this, NouProducte.class);
                                intent.putExtra(MODIFICARPRODUCTE, (Productos) lvProductes.getItemAtPosition(position));
                                intent.putExtra(ESMODIFICAT, true);
                                startActivity(intent);
                                dialog.dismiss();
                                break;
                            case 1:
                                int depenen=productosDAO.EnDepenen((int) lvProductes.getItemIdAtPosition(position));
                                if (depenen == 0) {
                                    productosDAO.eliminaProductos((int) lvProductes.getItemIdAtPosition(position));
                                    Toast.makeText(ProductesActivity.this, "Producto eliminado", Toast.LENGTH_LONG).show();
                                    lvProductes.setAdapter(new AdaptadorProductesComplet(productosDAO.obteProductos(), ProductesActivity.this));
                                    dialog.dismiss();
                                } else {
                                    Toast.makeText(ProductesActivity.this, "No se puede eliminar este producto "+depenen, Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }
                                break;
                        }
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        lvProductes.setAdapter(new AdaptadorProductesComplet(productosDAO.obteProductos(), this));
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        productosDAO.tanca();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_proveidors, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnuAlta:
                startActivity(new Intent(this, NouProducte.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
