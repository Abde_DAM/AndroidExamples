package com.example.informatica.practicaneptuno.Adaptadors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.informatica.practicaneptuno.POJO.Proveedores;
import com.example.informatica.practicaneptuno.R;

import java.util.List;

/**
 * Created by abde on 29/03/16.
 */
public class AdaptadorProveedores extends BaseAdapter {
    Context context;
    List<Proveedores> proveedores;
    LayoutInflater layoutInflater;

    public AdaptadorProveedores(List<Proveedores> proveedores, Context context) {
        this.proveedores = proveedores;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return proveedores.size();
    }

    @Override
    public Object getItem(int position) {
        return proveedores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return proveedores.get(position).getIdProveedor();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //TODO: crear el view
        View view= layoutInflater.inflate(R.layout.proveedores_view_adapter, null);

        TextView tvIdProveedor=(TextView)view.findViewById(R.id.tvIdProveedor);
        TextView tvNombreCompania=(TextView)view.findViewById(R.id.tvNombreCompania);


        tvIdProveedor.setText(String.valueOf(proveedores.get(position).getIdProveedor()));
        tvNombreCompania.setText(proveedores.get(position).getNombreCompania());


        return view;
    }
}