package com.example.informatica.practicaneptuno;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.informatica.practicaneptuno.Adaptadors.AdaptadorPedidos;
import com.example.informatica.practicaneptuno.DAO.PedidosDAO;
import com.example.informatica.practicaneptuno.POJO.Pedidos;

public class ComandesActivity extends AppCompatActivity {
    ListView lvPedidos;
    PedidosDAO pedidosDAO;
    public static String IDPEDIDO="idPedido", CARGO="cargo", MODIFICARPEDIDO="pedidoAModificar", ESMODIFICAT="EsModificat";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comandes);

        pedidosDAO =new PedidosDAO(this);
        pedidosDAO.obre();
        lvPedidos= (ListView) findViewById(R.id.lvPedidos);
        lvPedidos.setAdapter(new AdaptadorPedidos(pedidosDAO.obtePedidos(), this));
        lvPedidos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getBaseContext(), DetallePedidoActivity.class);
                intent.putExtra(IDPEDIDO, lvPedidos.getItemIdAtPosition(position));
                intent.putExtra(CARGO, ((Pedidos) lvPedidos.getItemAtPosition(position)).getCargo());
                startActivity(intent);
                Toast.makeText(getBaseContext(), String.valueOf(lvPedidos.getItemIdAtPosition(position)), Toast.LENGTH_LONG).show();

            }
        });
        lvPedidos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                String[] opcions = new String[]{"Modificar", "Eliminar"};
                ListView llista = new ListView(ComandesActivity.this);
                llista.setAdapter(new ArrayAdapter<String>(ComandesActivity.this, android.R.layout.simple_list_item_1, opcions));
                final AlertDialog dialog = (new AlertDialog.Builder(ComandesActivity.this)
                        .setTitle("Elige una opción")
                        .setView(llista)
                        .setCancelable(true)
                        .create());
                dialog.show();
                llista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int positionB, long id) {
                        switch (positionB) {
                            case 0:
                                Intent intent = new Intent(ComandesActivity.this, NouPedido.class);
                                intent.putExtra(MODIFICARPEDIDO, (Pedidos) lvPedidos.getItemAtPosition(position));
                                intent.putExtra(ESMODIFICAT, true);
                                startActivity(intent);
                                dialog.dismiss();
                                break;
                            case 1:
                                int depenen = pedidosDAO.EnDepenen((int) lvPedidos.getItemIdAtPosition(position));
                                if (depenen == 0) {
                                    pedidosDAO.eliminaPedido((int) lvPedidos.getItemIdAtPosition(position));
                                    Toast.makeText(ComandesActivity.this, "Pedido eliminado", Toast.LENGTH_LONG).show();
                                    lvPedidos.setAdapter(new AdaptadorPedidos(pedidosDAO.obtePedidos(), ComandesActivity.this));
                                    dialog.dismiss();
                                } else {
                                    Toast.makeText(ComandesActivity.this, "No se puede eliminar este pedido " + depenen, Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }
                                break;
                        }
                    }
                });

                return false;
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_proveidors, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnuAlta:
                startActivity(new Intent(getBaseContext(), NouPedido.class));
                return true;
            default:
                break;
        }

        return false;
    }
}

