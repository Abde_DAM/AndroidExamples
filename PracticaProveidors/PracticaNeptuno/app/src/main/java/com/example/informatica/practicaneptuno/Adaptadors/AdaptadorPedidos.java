package com.example.informatica.practicaneptuno.Adaptadors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.informatica.practicaneptuno.POJO.Pedidos;
import com.example.informatica.practicaneptuno.R;

import java.util.List;

/**
 * Created by abde on 1/04/16.
 */
public class AdaptadorPedidos extends BaseAdapter {
    private List<Pedidos> pedidos;
    private Context context;
    LayoutInflater layoutInflater;

    public AdaptadorPedidos(List<Pedidos> categories, Context context) {
        this.pedidos = categories;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);

    }
    @Override
    public int getCount() {
        return pedidos.size();
    }

    @Override
    public Object getItem(int position) {
        return pedidos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return pedidos.get(position).getIdPedido();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view= layoutInflater.inflate(R.layout.pedidos_view_adapter, null);
        TextView tvIdPedido=(TextView)view.findViewById(R.id.tvIdPedido);
        TextView tvIdEmpleado=(TextView)view.findViewById(R.id.tvIdEmpleado);
        TextView tvFechaPedido=(TextView)view.findViewById(R.id.tvFechaPedido);
        TextView tvIdCliente=(TextView)view.findViewById(R.id.tvIdCliente);
        TextView tvFechaEntrega=(TextView)view.findViewById(R.id.tvFechaEntrega);
        TextView tvFechaEnvio=(TextView)view.findViewById(R.id.tvFechaEnvio);
        TextView tvDesinatario=(TextView)view.findViewById(R.id.tvDesinatario);
        TextView tvDireccionDestinatario=(TextView)view.findViewById(R.id.tvDireccionDestinatario);
        TextView tvFormaEnvio=(TextView)view.findViewById(R.id.tvFormaEnvio);

        tvIdPedido.setText("ID Pedido: "+String.valueOf(pedidos.get(position).getIdPedido()));
        tvIdEmpleado.setText("ID Empleado: "+String.valueOf(pedidos.get(position).getIdEmpleado()));
        tvFechaPedido.setText("Fecha Pedido: "+pedidos.get(position).getFechaPedido());
        tvIdCliente.setText("ID Cliente: "+pedidos.get(position).getIdCliente());
        tvFechaEntrega.setText("Fecha entrega: "+pedidos.get(position).getFechaEntrega());
        tvFechaEnvio.setText("Fecha envio: "+pedidos.get(position).getFechaEnvio());
        tvDesinatario.setText("Destinatario: "+pedidos.get(position).getDestinatario());
        tvDireccionDestinatario.setText("Dirección: " + pedidos.get(position).getDireccionDestintatario());
        tvFormaEnvio.setText("Forma de envío: "+pedidos.get(position).getFormaEnvio());
        return view;
    }
}
