package com.example.informatica.practicaneptuno;

import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.example.informatica.practicaneptuno.Fragments.CategoriesDetall;
import com.example.informatica.practicaneptuno.Fragments.CategoriesMestre;

public class Categories extends AppCompatActivity {
    FrameLayout flPrimer;
    CategoriesMestre categoriesMestre;
    CategoriesDetall categoriesDetall;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        flPrimer= (FrameLayout) findViewById(R.id.flPrimer);

        if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE)
        {
            categoriesDetall=new CategoriesDetall();

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(flPrimer.getId(), categoriesDetall).commit();
        }
        else
        {
            categoriesMestre=new CategoriesMestre();

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(flPrimer.getId(), categoriesMestre).commit();
        }
    }
}
