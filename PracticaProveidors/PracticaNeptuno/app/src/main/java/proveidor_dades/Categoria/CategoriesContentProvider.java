package proveidor_dades.Categoria;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.example.informatica.practicaneptuno.Categories;
import com.example.informatica.practicaneptuno.Constants;
import com.example.informatica.practicaneptuno.Helpers.PrincipalSQLiteHelper;

/**
 * Created by abde on 4/04/16.
 */
public class CategoriesContentProvider extends ContentProvider {
    PrincipalSQLiteHelper openHelper;
    @Override
    public boolean onCreate() {
        openHelper=new PrincipalSQLiteHelper(getContext());
        return openHelper!=null;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        queryBuilder.setTables(Constants.TAULA_CATEGORIAS);

        int tipusUri = CategoriesContract.sUriMatcher.match(uri);

        switch(tipusUri)
        {
            case CategoriesContract.TOTES_LES_FILES:
                break;
            case CategoriesContract.UNA_FILA:
                queryBuilder.appendWhere(Constants.CATEGORIAS_ID_CATEGORIA+ " = " +uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        SQLiteDatabase bd = openHelper.getReadableDatabase();
        Cursor cursor = queryBuilder.query(bd, projection, selection, selectionArgs, null, null, sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        String resultat;
        int tipusUri=CategoriesContract.sUriMatcher.match(uri);

        switch (tipusUri)
        {
            case CategoriesContract.TOTES_LES_FILES:
                resultat=CategoriesContract.MIME_MULTIPLE;
                break;
            case CategoriesContract.UNA_FILA:
                resultat=CategoriesContract.MIME_UNA;
                break;
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }
        return resultat;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int tipusUri = CategoriesContract.sUriMatcher.match(uri);
        long id = 0;

        switch(tipusUri)
        {
            case CategoriesContract.TOTES_LES_FILES:
                SQLiteDatabase db = openHelper.getWritableDatabase();
                id = db.insert(Constants.TAULA_CATEGORIAS, null, values);
                break;
            case CategoriesContract.UNA_FILA:
                throw new IllegalArgumentException("Uri incorrecte: " + uri);
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return ContentUris.withAppendedId(CategoriesContract.CONTENT_URI, id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int tipusUri = CategoriesContract.sUriMatcher.match(uri);
        long filesEsborrades = 0;
        SQLiteDatabase db = openHelper.getWritableDatabase();

        switch(tipusUri)
        {
            case CategoriesContract.TOTES_LES_FILES:
                filesEsborrades = db.delete(Constants.TAULA_CATEGORIAS, selection, selectionArgs);
                break;
            case CategoriesContract.UNA_FILA:
                filesEsborrades = db.delete(Constants.TAULA_CATEGORIAS, Constants.CATEGORIAS_ID_CATEGORIA + " = " +uri.getLastPathSegment(), null);
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int tipusUri = CategoriesContract.sUriMatcher.match(uri);
        int filesActualitzades = 0;
        SQLiteDatabase db = openHelper.getWritableDatabase();

        switch(tipusUri)
        {
            case CategoriesContract.TOTES_LES_FILES:
                filesActualitzades = db.update(Constants.TAULA_CATEGORIAS, values, selection, selectionArgs);
                break;
            case CategoriesContract.UNA_FILA:
                filesActualitzades = db.update(Constants.TAULA_CATEGORIAS, values, Constants.CATEGORIAS_ID_CATEGORIA+ " = " + uri.getLastPathSegment(), null);
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return filesActualitzades;
    }
}
