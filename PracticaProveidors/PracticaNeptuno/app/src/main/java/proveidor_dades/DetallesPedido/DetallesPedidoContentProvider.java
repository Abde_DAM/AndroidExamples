package proveidor_dades.DetallesPedido;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.example.informatica.practicaneptuno.Constants;
import com.example.informatica.practicaneptuno.Helpers.PrincipalSQLiteHelper;

/**
 * Created by abde on 8/04/16.
 */
public class DetallesPedidoContentProvider extends ContentProvider {
    PrincipalSQLiteHelper openHelper;
    @Override
    public boolean onCreate() {
        openHelper=new PrincipalSQLiteHelper(getContext());
        return openHelper!=null;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        queryBuilder.setTables(Constants.TAULA_DETALLES_PEDIDO);

        int tipusUri = DetallesPedidoContract.sUriMatcher.match(uri);

        switch(tipusUri)
        {
            case DetallesPedidoContract.TOTES_LES_FILES:
                break;
            case DetallesPedidoContract.UNA_FILA:
                queryBuilder.appendWhere(Constants.DETALLES_ID_PEDIDO+ " = " +uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        SQLiteDatabase bd = openHelper.getReadableDatabase();
        Cursor cursor = queryBuilder.query(bd, projection, selection, selectionArgs, null, null, sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        String resultat;
        int tipusUri=DetallesPedidoContract.sUriMatcher.match(uri);

        switch (tipusUri)
        {
            case DetallesPedidoContract.TOTES_LES_FILES:
                resultat=DetallesPedidoContract.MIME_MULTIPLE;
                break;
            case DetallesPedidoContract.UNA_FILA:
                resultat=DetallesPedidoContract.MIME_UNA;
                break;
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }
        return resultat;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int tipusUri = DetallesPedidoContract.sUriMatcher.match(uri);
        long id = 0;

        switch(tipusUri)
        {
            case DetallesPedidoContract.TOTES_LES_FILES:
                SQLiteDatabase db = openHelper.getWritableDatabase();
                id = db.insert(Constants.TAULA_DETALLES_PEDIDO, null, values);
                break;
            case DetallesPedidoContract.UNA_FILA:
                throw new IllegalArgumentException("Uri incorrecte: " + uri);
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return ContentUris.withAppendedId(DetallesPedidoContract.CONTENT_URI, id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int tipusUri = DetallesPedidoContract.sUriMatcher.match(uri);
        long filesEsborrades = 0;
        SQLiteDatabase db = openHelper.getWritableDatabase();

        switch(tipusUri)
        {
            case DetallesPedidoContract.TOTES_LES_FILES:
                filesEsborrades = db.delete(Constants.TAULA_DETALLES_PEDIDO, selection, selectionArgs);
                break;
            case DetallesPedidoContract.UNA_FILA:
                filesEsborrades = db.delete(Constants.TAULA_DETALLES_PEDIDO, Constants.DETALLES_ID_PEDIDO + " = " +uri.getLastPathSegment(), null);
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int tipusUri = DetallesPedidoContract.sUriMatcher.match(uri);
        int filesActualitzades = 0;
        SQLiteDatabase db = openHelper.getWritableDatabase();

        switch(tipusUri)
        {
            case DetallesPedidoContract.TOTES_LES_FILES:
                filesActualitzades = db.update(Constants.TAULA_DETALLES_PEDIDO, values, selection, selectionArgs);
                break;
            case DetallesPedidoContract.UNA_FILA:
                filesActualitzades = db.update(Constants.TAULA_DETALLES_PEDIDO, values, Constants.DETALLES_ID_PEDIDO+ " = " + uri.getLastPathSegment(), null);
            default:
                throw new IllegalArgumentException("Uri desconeguda: " +uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return filesActualitzades;
    }
}
