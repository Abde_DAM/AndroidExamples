package proveidor_dades.Comandes;

import android.content.UriMatcher;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by abde on 6/04/16.
 */
public class ComandesContract {
    private ComandesContract()
    {}

    public static final String ESQUEMA = "content://";

    public static final String AUTHORITY = "proveidor_dades.Comanda";

    public static final String BASE_PATH = "comandes";

    public static final Uri CONTENT_URI = Uri.parse(ESQUEMA+AUTHORITY+"/"+BASE_PATH);

    public static final int TOTES_LES_FILES = 1;

    public static final int UNA_FILA = 2;

    public static final UriMatcher sUriMatcher;
    static
    {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH, TOTES_LES_FILES);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH+"/#", UNA_FILA);
    }

    public final static class Columnes implements BaseColumns
    {
        public static final String TAULA_PEDIDOS="pedidos",
                PEDIDOS_ID_PEDIDO="id_pedido",
                PEDIDOS_ID_CLIENTE="id_cliente",
                PEDIDOS_ID_EMPLEADO="id_empleado",
                PEDIDOS_FECHA_PEDIDO="fecha_pedido",
                PEDIDOS_FECHA_ENTREGA="fecha_entrega",
                PEDIDOS_FECHA_ENVIO="fecha_envio",
                PEDIDOS_FORMA_ENVIO="forma_envio",
                PEDIDOS_CARGO="cargo",
                PEDIDOS_DESTINATARIO="destinatario",
                PEDIDOS_DIRECCION_DESTINATARIO="direccion_destinatario",
                PEDIDOS_CIUDAD_DESTINATARIO="ciudad_destinatario",
                PEDIDOS_REGION_DESTINATARIO="region_destinatario",
                PEDIDOS_CODPOSTAL_DESTINATARIO="codpostal_destinatario",
                PEDIDOS_PAIS_DESTINATARIO="pais_destinatario";
    }

    public final static String MIME_UNA = "vnd.android.cursos.item/vnd." +AUTHORITY+"/"+BASE_PATH;

    public final static String MIME_MULTIPLE = "vnd.android.cursos.dir/vnd." +AUTHORITY+"/"+BASE_PATH;
}