package cat.duba.multimedia;

import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

public class MediaPlayerActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnEndavant, btnEndarrera, btnReprodueix, btnPausa;
    ImageView ivImatge;
    SeekBar sbPosicio;
    TextView tvTempsActual, tvTitol, tvTempsTotal;


    MediaPlayer mediaPlayer;
    double tempsActual=0;
    double tempsTotal=0;

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_player);
        handler=new Handler();

        btnEndarrera= (Button) findViewById(R.id.btnEndarrere);
        btnEndavant= (Button) findViewById(R.id.btnEndavant);
        btnPausa= (Button) findViewById(R.id.btnPausa);
        btnReprodueix= (Button) findViewById(R.id.btnReprodueix);

        ivImatge= (ImageView) findViewById(R.id.ivCaratula);

        tvTempsActual= (TextView) findViewById(R.id.tvTempsActual);
        tvTempsTotal= (TextView) findViewById(R.id.tvTempsTotal);
        tvTitol= (TextView) findViewById(R.id.tvTitol);

        sbPosicio= (SeekBar) findViewById(R.id.sbPosicio);
        sbPosicio.setClickable(false);

        btnPausa.setEnabled(false);

        btnPausa.setOnClickListener(this);
        btnEndarrera.setOnClickListener(this);
        btnEndavant.setOnClickListener(this);
        btnReprodueix.setOnClickListener(this);
        tvTitol.setText("Tuturu.mp3");

        mediaPlayer=MediaPlayer.create(this, R.raw.tuturu_century_fox);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnReprodueix:
                Toast.makeText(this, "Reproduint cançó", Toast.LENGTH_SHORT).show();

                mediaPlayer.start();

                tempsTotal=mediaPlayer.getDuration();
                tempsActual=mediaPlayer.getCurrentPosition();

                sbPosicio.setMax((int) tempsTotal);

                String cadena=String.format("%d min, %d seg", TimeUnit.MILLISECONDS.toMinutes((long) tempsTotal), TimeUnit.MILLISECONDS.toSeconds((long) (tempsTotal%60)));

                tvTempsTotal.setText(cadena);

                cadena=String.format("%d min, %d seg", TimeUnit.MILLISECONDS.toMinutes((long) tempsActual), TimeUnit.MILLISECONDS.toSeconds((long) (tempsActual%60)));
                tvTempsActual.setText(cadena);
                sbPosicio.setProgress((int) tempsActual);
                btnPausa.setEnabled(true);
                btnReprodueix.setEnabled(false);
                handler.postDelayed(actualitzaTemps,100);
                break;
            case R.id.btnPausa:
                Toast.makeText(this, "Pausant cançó", Toast.LENGTH_SHORT).show();
                mediaPlayer.pause();
                btnPausa.setEnabled(false);
                btnReprodueix.setEnabled(true);
                handler.removeCallbacks(actualitzaTemps);
                break;
            case R.id.btnEndavant:
                if(tempsActual+5000<tempsTotal) {
                    Toast.makeText(this, "Avançant 5 segons", Toast.LENGTH_SHORT).show();

                    mediaPlayer.seekTo((int) tempsActual + 5000);
                }
                else
                    Toast.makeText(this, "No es pot avançar", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnEndarrere:
                if(tempsActual-5000>0) {
                    Toast.makeText(this, "Retrocedint 5 segons", Toast.LENGTH_SHORT).show();

                    mediaPlayer.seekTo((int) tempsActual - 5000);
                }
                else
                    Toast.makeText(this, "No es pot retrocedir", Toast.LENGTH_SHORT).show();
                break;
        }

    }
    private Runnable actualitzaTemps=new Runnable() {
        @Override
        public void run() {
            tempsActual=mediaPlayer.getCurrentPosition();
            String cadena=String.format("%d min, %d seg", TimeUnit.MILLISECONDS.toMinutes((long) tempsActual), TimeUnit.MILLISECONDS.toSeconds((long) (tempsActual % 60)));
            tvTempsActual.setText(cadena);
            sbPosicio.setProgress((int) tempsActual);
        }
    };
}
