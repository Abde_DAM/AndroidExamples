package cat.duba.multimedia;

import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

public class VideoActivity extends AppCompatActivity {
    private Button btnReprodueix;
    private SurfaceView sfcPantalla;
    private MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        btnReprodueix= (Button) findViewById(R.id.btnReprodueix);
        sfcPantalla= (SurfaceView) findViewById(R.id.sfcPantalla);

        btnReprodueix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if(mediaPlayer!=null)
                    {
                        mediaPlayer.reset();
                        mediaPlayer.release();
                    }
                    else{
                        mediaPlayer= MediaPlayer.create(getApplicationContext(), R.raw.tuturu);
                        SurfaceHolder surfaceHolder=sfcPantalla.getHolder();
                        mediaPlayer.setDisplay(surfaceHolder);
                        mediaPlayer.start();
                    }
                }
                catch (Exception ex)
                {
                    Log.e("VIDEO", ex.getMessage());
                }
            }
        });
    }
}
