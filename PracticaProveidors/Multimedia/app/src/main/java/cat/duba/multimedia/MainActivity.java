package cat.duba.multimedia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        Intent intent;

        if(id==R.id.mnuAudio)
        {
            intent=new Intent(this, AudioActivity.class);
            startActivity(intent);
        }
        else if(id==R.id.mnuMediaPlayer)
        {
            intent=new Intent(this, MediaPlayerActivity.class);
            startActivity(intent);
        }
        else if(id==R.id.mnuVideoPlayer)
        {
            intent=new Intent(this, VideoActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
