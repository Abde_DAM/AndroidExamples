package cat.duba.controlspersonalitzats;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by abde on 7/03/16.
 */
public class TwitterEditText extends EditText{

    private final int MAX_CARACTERS=140;
    float escala;
    Paint pinzellFons;
    Paint pinzellLletra;

    public TwitterEditText(Context context) {
        super(context);
        Inicialitzacio();
    }

    public TwitterEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        Inicialitzacio();
    }

    public TwitterEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Inicialitzacio();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float amplada=30*escala;
        float alçada=20*escala;
        canvas.drawRoundRect(new RectF(0f, 0f, amplada, 20 * escala), 5f * escala, 5f * escala, pinzellFons);
        String texte= Integer.toString(MAX_CARACTERS-getText().toString().length());
        canvas.drawText(texte, amplada/2, alçada, pinzellLletra);
    }


    private void Inicialitzacio()
    {
        setFilters((new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(140)}));
        pinzellFons=new Paint(Paint.ANTI_ALIAS_FLAG);
        pinzellLletra=new Paint(Paint.ANTI_ALIAS_FLAG);
        pinzellLletra.setColor(Color.WHITE);
        pinzellLletra.setTextSize(30);
        pinzellLletra.setTextAlign(Paint.Align.CENTER);

        escala=getResources().getDisplayMetrics().density;

        int color_vermell = (int)(getText().length() / (float)MAX_CARACTERS) * 255;
        int color_verd = 255-color_vermell;

        if (pinzellFons != null)
        {
            pinzellFons.setColor(Color.rgb(color_vermell, color_verd, 0));
        }
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        int color_vermell = (int)(getText().length() / (float)MAX_CARACTERS) * 255;
        int color_verd = 255-color_vermell;

        if (pinzellFons != null)
        {
            pinzellFons.setColor(Color.rgb(color_vermell, color_verd, 0));
        }

    }
}
