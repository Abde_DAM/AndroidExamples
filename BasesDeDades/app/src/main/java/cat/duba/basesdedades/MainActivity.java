package cat.duba.basesdedades;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import cat.duba.basesdedades.DAO.ProvinciaDAO;
import cat.duba.basesdedades.Model.Municipi;

public class MainActivity extends AppCompatActivity {

    MunicipisSQLiteHelper bdHelper;
    SQLiteDatabase bd;
    Spinner spnProvincies;
    ListView lstMunicipis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spnProvincies= (Spinner) findViewById(R.id.spnProvincies);
        lstMunicipis= (ListView) findViewById(R.id.lstMunicipis);

        bdHelper=new MunicipisSQLiteHelper(this);
        bd=bdHelper.getWritableDatabase();

        carregaProvinciesASpinner();
        spnProvincies.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                carregaMunicipisAlListView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
                lstMunicipis.setAdapter(null);
            }
        });
    }

    private void carregaProvinciesASpinner(){
        Cursor cursor=bd.rawQuery("SELECT * FROM "+MunicipisSQLiteHelper.TAULA_PROVINCIES, null);

        SimpleCursorAdapter adaptadorSpinner= new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, new String[]{
                MunicipisSQLiteHelper.COLUMNA_ID_PROVINCIA, MunicipisSQLiteHelper.COLUMNA_NOM_PROVINCIA}, new int[]{
                android.R.id.text1, android.R.id.text2
        }, 0);
        spnProvincies.setAdapter(adaptadorSpinner);
    }
    public void carregaMunicipisAlListView()
    {
        String provinciaSeleccionada;
        int posicio=spnProvincies.getSelectedItemPosition();

        SimpleCursorAdapter adaptadorSpn= (SimpleCursorAdapter) spnProvincies.getAdapter();
        Cursor cursorSpinner=adaptadorSpn.getCursor();

        provinciaSeleccionada =cursorSpinner.getString(1);

        Cursor cursorListView=bd.query(MunicipisSQLiteHelper.TAULA_MUNICIPIS,null,MunicipisSQLiteHelper.COLUMNA_ID_PROVINCIA+" = ? ",new String[]{provinciaSeleccionada},null,null,MunicipisSQLiteHelper.COLUMNA_NOM_MUNICIPI);

        SimpleCursorAdapter adaptadorListView=new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursorListView, new String[]{
                MunicipisSQLiteHelper.COLUMNA_ID_MUNICIPI, MunicipisSQLiteHelper.COLUMNA_NOM_MUNICIPI}, new int[]{android.R.id.text1, android.R.id.text2
        }, 0);
        lstMunicipis.setAdapter(adaptadorListView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.mnuOmple:
                omple();
                break;
            case R.id.mnuModifica:
                break;
            case R.id.mnuElimina:
                break;
        }
        return true;
    }

    private void omple() {
        try {
            bd.execSQL("DELETE FROM " + MunicipisSQLiteHelper.TAULA_MUNICIPIS);
            bd.execSQL("DELETE FROM " + MunicipisSQLiteHelper.TAULA_PROVINCIES);
        }
        catch (Exception ex)
        {
            Log.d("NoEsborra", ex.getMessage());
        }
        ompleProvincies();
        ompleMunicipis();
    }

    private void ompleMunicipis() {
        ArrayList<Municipi> municipis=new ArrayList<Municipi>();
        municipis=Fitxers.obtenirMunicipis(this);
        if(bd!=null)
        {
            ContentValues nouRegistre;
            for (Municipi m:municipis)
            {
                nouRegistre=new ContentValues();
                nouRegistre.put(MunicipisSQLiteHelper.COLUMNA_ID_MUNICIPI, m.getCodiIne());
                nouRegistre.put(MunicipisSQLiteHelper.COLUMNA_ID_PROVINCIA, m.getCodiProvincia());
                nouRegistre.put(MunicipisSQLiteHelper.COLUMNA_NOM_MUNICIPI, m.getNomMunicipi());

                bd.insert(MunicipisSQLiteHelper.TAULA_MUNICIPIS, null, nouRegistre);
            }
        }
    }

    private void ompleProvincies() {
        Map<Integer, String> provincies=new Hashtable<Integer,String>();
        provincies=Fitxers.obtenirTaulaProvincies(this);
        /*if(bd!=null)
        {
            for (Integer i:provincies.keySet())
            {
                bd.execSQL("INSERT INTO "+MunicipisSQLiteHelper.TAULA_PROVINCIES+" ("+MunicipisSQLiteHelper.COLUMNA_ID_PROVINCIA+", "+MunicipisSQLiteHelper.COLUMNA_NOM_PROVINCIA+") VALUES ('"+i+"', '"+provincies.get(i)+"');");
            }
        }*/

        ProvinciaDAO provinciaDAO=new ProvinciaDAO(this);
        try
        {
            provinciaDAO.obre();
            for (Integer i :
                    provincies.keySet()) {
                provinciaDAO.creaProvincia(i, provincies.get(i));
            }
        }
        catch (SQLiteException e)
        {
            Log.e("SQLERROR", "ProvinciaDAO - "+e.getMessage());
        }
        finally {
            provinciaDAO.tanca();
        }
    }
}
