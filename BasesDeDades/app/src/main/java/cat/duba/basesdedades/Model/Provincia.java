package cat.duba.basesdedades.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abde on 14/03/16.
 */
public class Provincia implements Parcelable {
    private int codiProvincia;
    private String nomProvincia;

    public Provincia(int codiProvincia, String nomProvincia) {
        this.codiProvincia = codiProvincia;
        this.nomProvincia = nomProvincia;
    }

    public Provincia() {
    }

    protected Provincia(Parcel in) {
        readFromParcel(in);
    }

    public int getCodiProvincia() {
        return codiProvincia;
    }

    public void setCodiProvincia(int codiProvincia) {
        this.codiProvincia = codiProvincia;
    }

    public String getNomProvincia() {
        return nomProvincia;
    }

    public void setNomProvincia(String nomProvincia) {
        this.nomProvincia = nomProvincia;
    }


    public static final Creator<Provincia> CREATOR = new Creator<Provincia>() {
        @Override
        public Provincia createFromParcel(Parcel in) {
            return new Provincia(in);
        }

        @Override
        public Provincia[] newArray(int size) {
            return new Provincia[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(codiProvincia);
        dest.writeString(nomProvincia);
    }

    public void readFromParcel (Parcel dades)
    {
        codiProvincia = dades.readInt();
        nomProvincia = dades.readString();
    }
}
