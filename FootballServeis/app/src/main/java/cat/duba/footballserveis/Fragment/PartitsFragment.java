package cat.duba.footballserveis.Fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONException;

import cat.duba.footballserveis.Adapters.JornadesAdapter;
import cat.duba.footballserveis.FootballHttpClient;
import cat.duba.footballserveis.JSONParser.ParserJSON;
import cat.duba.footballserveis.Pojo.Jornades;
import cat.duba.footballserveis.R;

/**
 * Created by abde on 19/04/16.
 */
public class PartitsFragment extends Fragment {
    ListView lvJornades;
    int idLliga;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_partits, container, false);
        lvJornades = (ListView) v.findViewById(R.id.lvJornades);
        idLliga = (Integer) container.getTag(R.string.SELECTED_LEAGUE_ID);
        new JornadaTasca().execute();
        return v;
    }

    class JornadaTasca extends AsyncTask<String, Void, Jornades> {
        Jornades jornades;

        @Override
        protected Jornades doInBackground(String... params) {
            FootballHttpClient connexio = new FootballHttpClient();
            try {
                jornades = ParserJSON.obteLlistaJornades(connexio.obtenirJornades(idLliga));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jornades;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Jornades equip) {
            lvJornades.setAdapter(new JornadesAdapter(equip, PartitsFragment.this.getContext()));

        }
    }
}