package cat.duba.footballserveis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by abde on 12/04/16.
 */
public class FootballHttpClient {
    private final static String BASE_URL="http://api.football-data.org/v1/";
    private final static String TOTES_LLIGUES="soccerseasons", EQUIPS="/teams", LEAGUETABLE="/leagueTable", JORNADES="/fixtures";
    public String obtenirDadesLligues()
    {
        HttpURLConnection connexio=null;
        InputStream inputStream=null;
        try{
            connexio=(HttpURLConnection)(new URL(BASE_URL+TOTES_LLIGUES).openConnection());
            connexio.setRequestMethod("GET");
            connexio.setRequestProperty("X-Auth-Token", "b0478f20fa1641d496b6d668b64627d5");
            connexio.connect();

            StringBuilder buffer=new StringBuilder();
            inputStream=connexio.getInputStream();

            BufferedReader br=new BufferedReader(new InputStreamReader(inputStream));

            String linia=null;
            while ((linia=br.readLine())!=null)
            {
                buffer.append(linia);
            }

            br.close();
            inputStream.close();
            connexio.disconnect();

            return buffer.toString();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
  /*  public String obtenirDadesLliga(int idLliga)
    {
        HttpURLConnection connexio=null;
        InputStream inputStream=null;
        try{
            connexio=(HttpURLConnection)(new URL(BASE_URL+TOTES_LLIGUES+"/"+idLliga+EQUIPS).openConnection());
            connexio.setRequestMethod("GET");
            connexio.setRequestProperty("X-Auth-Token", "b0478f20fa1641d496b6d668b64627d5");
            connexio.connect();

            StringBuilder buffer=new StringBuilder();
            inputStream=connexio.getInputStream();

            BufferedReader br=new BufferedReader(new InputStreamReader(inputStream));

            String linia=null;
            while ((linia=br.readLine())!=null)
            {
                buffer.append(linia);
            }

            br.close();
            inputStream.close();
            connexio.disconnect();

            return buffer.toString();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }*/
  public String obtenirClassificacio(int idLliga)
  {
      HttpURLConnection connexio=null;
      InputStream inputStream=null;
      try{
          connexio=(HttpURLConnection)(new URL(BASE_URL+TOTES_LLIGUES+"/"+idLliga+LEAGUETABLE).openConnection());
          connexio.setRequestMethod("GET");
          connexio.setRequestProperty("X-Auth-Token", "b0478f20fa1641d496b6d668b64627d5");
          connexio.connect();

          StringBuilder buffer=new StringBuilder();
          inputStream=connexio.getInputStream();

          BufferedReader br=new BufferedReader(new InputStreamReader(inputStream));

          String linia=null;
          while ((linia=br.readLine())!=null)
          {
              buffer.append(linia);
          }

          br.close();
          inputStream.close();
          connexio.disconnect();

          return buffer.toString();


      } catch (MalformedURLException e) {
          e.printStackTrace();
      } catch (IOException e) {
          e.printStackTrace();
      }
      return null;
  }

    public String obtenirEquips(int idLliga)
    {
        HttpURLConnection connexio=null;
        InputStream inputStream=null;
        try{
            connexio=(HttpURLConnection)(new URL(BASE_URL+TOTES_LLIGUES+"/"+idLliga+EQUIPS).openConnection());
            connexio.setRequestMethod("GET");
            connexio.setRequestProperty("X-Auth-Token", "b0478f20fa1641d496b6d668b64627d5");
            connexio.connect();

            StringBuilder buffer=new StringBuilder();
            inputStream=connexio.getInputStream();

            BufferedReader br=new BufferedReader(new InputStreamReader(inputStream));

            String linia=null;
            while ((linia=br.readLine())!=null)
            {
                buffer.append(linia);
            }

            br.close();
            inputStream.close();
            connexio.disconnect();

            return buffer.toString();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String obtenirJornades(int idLliga)
    {
        HttpURLConnection connexio=null;
        InputStream inputStream=null;
        try{
            connexio=(HttpURLConnection)(new URL(BASE_URL+TOTES_LLIGUES+"/"+idLliga+JORNADES).openConnection());
            connexio.setRequestMethod("GET");
            connexio.setRequestProperty("X-Auth-Token", "b0478f20fa1641d496b6d668b64627d5");
            connexio.connect();

            StringBuilder buffer=new StringBuilder();
            inputStream=connexio.getInputStream();

            BufferedReader br=new BufferedReader(new InputStreamReader(inputStream));

            String linia=null;
            while ((linia=br.readLine())!=null)
            {
                buffer.append(linia);
            }

            br.close();
            inputStream.close();
            connexio.disconnect();

            return buffer.toString();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
