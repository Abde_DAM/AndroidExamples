package cat.duba.footballserveis.Pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by abde on 19/04/16.
 */
public class Classificacions implements Parcelable {
    private String self;
    private String soccerSeason;
    private String leagueCaption;

    private int matchDay;

    public ArrayList<Equip> getEquip() {
        return equip;
    }

    public void setEquip(ArrayList<Equip> equip) {
        this.equip = equip;
    }

    private ArrayList<Equip> equip=new ArrayList<Equip>();

    public Classificacions()
    {}

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getSoccerSeason() {
        return soccerSeason;
    }

    public void setSoccerSeason(String soccerSeason) {
        this.soccerSeason = soccerSeason;
    }

    public String getLeagueCaption() {
        return leagueCaption;
    }

    public void setLeagueCaption(String leagueCaption) {
        this.leagueCaption = leagueCaption;
    }

    public int getMatchDay() {
        return matchDay;
    }

    public void setMatchDay(int matchDay) {
        this.matchDay = matchDay;
    }


    public static class Equip implements Parcelable {
        private String link;
        private int position;
        private String nom;
        private String linkLogo;
        private int playedGames;
        private int points;
        private int goals;
        private int goalsAgainst;
        private int wins;
        private int draws;
        private int losses;

        public Equip(){

        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public String getLinkLogo() {
            return linkLogo;
        }

        public void setLinkLogo(String linkLogo) {
            this.linkLogo = linkLogo;
        }

        public int getPlayedGames() {
            return playedGames;
        }

        public void setPlayedGames(int playedGames) {
            this.playedGames = playedGames;
        }

        public int getPoints() {
            return points;
        }

        public void setPoints(int points) {
            this.points = points;
        }

        public int getGoals() {
            return goals;
        }

        public void setGoals(int goals) {
            this.goals = goals;
        }

        public int getGoalsAgainst() {
            return goalsAgainst;
        }

        public void setGoalsAgainst(int goalsAgainst) {
            this.goalsAgainst = goalsAgainst;
        }

        public int getWins() {
            return wins;
        }

        public void setWins(int wins) {
            this.wins = wins;
        }

        public int getDraws() {
            return draws;
        }

        public void setDraws(int draws) {
            this.draws = draws;
        }

        public int getLosses() {
            return losses;
        }

        public void setLosses(int losses) {
            this.losses = losses;
        }

        protected Equip(Parcel in) {
            link = in.readString();
            position = in.readInt();
            nom = in.readString();
            linkLogo = in.readString();
            playedGames = in.readInt();
            points = in.readInt();
            goals = in.readInt();
            goalsAgainst = in.readInt();
            wins = in.readInt();
            draws = in.readInt();
            losses = in.readInt();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(link);
            dest.writeInt(position);
            dest.writeString(nom);
            dest.writeString(linkLogo);
            dest.writeInt(playedGames);
            dest.writeInt(points);
            dest.writeInt(goals);
            dest.writeInt(goalsAgainst);
            dest.writeInt(wins);
            dest.writeInt(draws);
            dest.writeInt(losses);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Equip> CREATOR = new Parcelable.Creator<Equip>() {
            @Override
            public Equip createFromParcel(Parcel in) {
                return new Equip(in);
            }

            @Override
            public Equip[] newArray(int size) {
                return new Equip[size];
            }
        };
    }


    protected Classificacions(Parcel in) {
        self = in.readString();
        soccerSeason = in.readString();
        leagueCaption = in.readString();
        matchDay = in.readInt();
        if (in.readByte() == 0x01) {
            equip = new ArrayList<Equip>();
            in.readList(equip, Equip.class.getClassLoader());
        } else {
            equip = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(self);
        dest.writeString(soccerSeason);
        dest.writeString(leagueCaption);
        dest.writeInt(matchDay);
        if (equip == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(equip);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Classificacions> CREATOR = new Parcelable.Creator<Classificacions>() {
        @Override
        public Classificacions createFromParcel(Parcel in) {
            return new Classificacions(in);
        }

        @Override
        public Classificacions[] newArray(int size) {
            return new Classificacions[size];
        }
    };
}