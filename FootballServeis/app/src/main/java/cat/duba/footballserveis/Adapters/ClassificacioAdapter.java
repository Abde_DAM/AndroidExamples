package cat.duba.footballserveis.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.PictureDrawable;
import android.graphics.drawable.VectorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import cat.duba.footballserveis.Fragment.ClassificacioFragment;
import cat.duba.footballserveis.Pojo.Classificacions;
import cat.duba.footballserveis.R;
import cat.duba.footballserveis.SVGStuff.SvgDecoder;
import cat.duba.footballserveis.SVGStuff.SvgDrawableTranscoder;
import cat.duba.footballserveis.SVGStuff.SvgSoftwareLayerSetter;

import static cat.duba.footballserveis.R.id.ivLogo;
import static cat.duba.footballserveis.R.id.tvGoalsAgainst;

/**
 * Created by abde on 20/04/16.
 */
public class ClassificacioAdapter extends BaseAdapter {
    Classificacions classificacions;
    LayoutInflater layoutInflater;
    Context context;

    public ClassificacioAdapter(Classificacions classificacions, Context context) {
        this.classificacions = classificacions;
        layoutInflater=LayoutInflater.from(context);
        this.context=context;
    }

    @Override
    public int getCount() {
        return classificacions.getEquip().size();
    }

    @Override
    public Object getItem(int position) {
        return classificacions.getEquip().get(position);
    }

    @Override
    public long getItemId(int position) {
        return classificacions.getEquip().get(position).getPosition();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;
        if(vi==null)
        {
            vi=layoutInflater.inflate(R.layout.classificacio_adapter,parent, false);
            holder=new ViewHolder();
            holder.ivLogo= (ImageView) vi.findViewById(R.id.ivLogo);
            holder.tvNomEquip= (TextView) vi.findViewById(R.id.tvNomEquip);
            holder.tvPlayedGames= (TextView) vi.findViewById(R.id.tvPlayedGames);
            holder.tvPoints= (TextView) vi.findViewById(R.id.tvPoints);
            holder.tvGoals= (TextView) vi.findViewById(R.id.tvGoals);
            holder.tvGoalsAgainst= (TextView) vi.findViewById(tvGoalsAgainst);
            holder.tvWDL= (TextView) vi.findViewById(R.id.tvWDL);
            vi.setTag(holder);
        }
        else {
            holder = (ViewHolder) vi.getTag();

        }
        Classificacions.Equip item = (Classificacions.Equip) getItem(position);


        if(item.getLinkLogo().substring(item.getLinkLogo().length()-3).equals("svg"))
        {
            GenericRequestBuilder requestBuilder = Glide.with(context)
                    .using(Glide.buildStreamModelLoader(Uri.class, context), InputStream.class)
                    .from(Uri.class)
                    .as(SVG.class)
                    .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                    .sourceEncoder(new StreamEncoder())
                    .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                    .decoder(new SvgDecoder())
                    .animate(android.R.anim.fade_in)
                    .listener(new SvgSoftwareLayerSetter<Uri>());
            Uri uri = Uri.parse(item.getLinkLogo());
            requestBuilder
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            // SVG cannot be serialized so it's not worth to cache it
                    .load(uri)
                    .into(holder.ivLogo);
        }
        else{
    /*   Glide
                .with(context)
                .load(item.getLink())
                .centerCrop()
                .crossFade()
                .into(holder.ivLogo);*/


      /*      URL url = null;
            try {
                url = new URL(item.getLinkLogo());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Bitmap bmp = null;
            try {
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            holder.ivLogo.setImageBitmap(bmp);*/

        }

//        Glide.with(context).load(item.getLinkLogo()).into(holder.ivLogo);

        holder.tvNomEquip.setText(item.getPosition()+".- "+item.getNom());
        holder.tvPlayedGames.setText("Partidos Jugados: "+item.getPlayedGames());
        holder.tvPoints.setText("Puntos: "+item.getPoints());
        holder.tvGoals.setText("Goles a favor: "+item.getGoals());
        holder.tvGoalsAgainst.setText("Goles en contra: "+item.getGoalsAgainst());
        holder.tvWDL.setText("Victorias/Empates/Derrotas: "+item.getWins()+"/"+item.getDraws()+"/"+item.getLosses());

        return vi;
    }
    static class ViewHolder{
        ImageView ivLogo;
        TextView tvNomEquip, tvPlayedGames, tvPoints, tvGoals, tvGoalsAgainst,tvWDL;
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
