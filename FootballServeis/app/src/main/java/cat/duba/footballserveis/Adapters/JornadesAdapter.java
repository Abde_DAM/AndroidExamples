package cat.duba.footballserveis.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;

import java.io.InputStream;

import cat.duba.footballserveis.Pojo.Equips;
import cat.duba.footballserveis.Pojo.Jornades;
import cat.duba.footballserveis.R;
import cat.duba.footballserveis.SVGStuff.SvgDecoder;
import cat.duba.footballserveis.SVGStuff.SvgDrawableTranscoder;
import cat.duba.footballserveis.SVGStuff.SvgSoftwareLayerSetter;

/**
 * Created by abde on 22/04/16.
 */
public class JornadesAdapter extends BaseAdapter {
    Jornades jornades;
    LayoutInflater layoutInflater;
    Context context;

    public JornadesAdapter(Jornades jornades, Context context) {
        this.jornades = jornades;
        layoutInflater=LayoutInflater.from(context);
        this.context=context;
    }

    @Override
    public int getCount() {
        return jornades.getJornada().size();
    }

    @Override
    public Object getItem(int position) {
        return jornades.getJornada().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;
        if(vi==null)
        {
            vi=layoutInflater.inflate(R.layout.jornada_adapter,parent, false);
            holder=new ViewHolder();
            holder.tvDate= (TextView) vi.findViewById(R.id.tvDate);
            holder.tvGolsLocal= (TextView) vi.findViewById(R.id.tvGolsLocal);
            holder.tvGolsVisitant= (TextView) vi.findViewById(R.id.tvGolsVisitant);
            holder.tvLocal= (TextView) vi.findViewById(R.id.tvLocal);
            holder.tvVisitant= (TextView) vi.findViewById(R.id.tvVisitant);
            holder.tvStatus= (TextView) vi.findViewById(R.id.tvStatus);
            holder.tvJornada= (TextView) vi.findViewById(R.id.tvJornada);
            vi.setTag(holder);
        }
        else {
            holder = (ViewHolder) vi.getTag();

        }
        Jornades.Jornada item = (Jornades.Jornada) getItem(position);

        holder.tvDate.setText(item.getDate());

        holder.tvGolsVisitant.setText(String.valueOf(item.getGoalsAwayTeam()));

        holder.tvGolsLocal.setText(String.valueOf(item.getGoalsHomeTeam()));

        holder.tvLocal.setText(item.getHomeTeamName());

        holder.tvVisitant.setText(item.getAwayTeamName());

        holder.tvStatus.setText("Estado: "+item.getStatus());

        holder.tvJornada.setText("Jornada: "+item.getMatchday());

        return vi;
    }
    static class ViewHolder{
        TextView tvLocal, tvGolsLocal, tvVisitant,tvGolsVisitant,tvStatus,tvDate, tvJornada;
    }
}
