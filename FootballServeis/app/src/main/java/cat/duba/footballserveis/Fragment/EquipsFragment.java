package cat.duba.footballserveis.Fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONException;

import cat.duba.footballserveis.Adapters.EquipsLligaAdapter;
import cat.duba.footballserveis.FootballHttpClient;
import cat.duba.footballserveis.JSONParser.ParserJSON;
import cat.duba.footballserveis.Pojo.Equips;
import cat.duba.footballserveis.R;

/**
 * Created by abde on 19/04/16.
 */
public class EquipsFragment extends Fragment {
    ListView lvEquips;
    int idLliga;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_equips, container, false);
        lvEquips = (ListView) v.findViewById(R.id.lvEquips);
        idLliga = (Integer) container.getTag(R.string.SELECTED_LEAGUE_ID);
        new EquipsTasca().execute();
        return v;
    }

    class EquipsTasca extends AsyncTask<String, Void, Equips> {
        Equips classi;

        @Override
        protected Equips doInBackground(String... params) {
            FootballHttpClient connexio = new FootballHttpClient();
            try {
                classi = ParserJSON.obteLlistaEquips(connexio.obtenirEquips(idLliga));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return classi;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Equips equip) {
            lvEquips.setAdapter(new EquipsLligaAdapter(equip, EquipsFragment.this.getContext()));

        }
    }
}