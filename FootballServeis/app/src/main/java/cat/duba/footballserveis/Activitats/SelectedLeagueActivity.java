package cat.duba.footballserveis.Activitats;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import cat.duba.footballserveis.R;
import cat.duba.footballserveis.ViewPagerAdapter;

public class SelectedLeagueActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_league);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TabLayout tabs= (TabLayout) findViewById(R.id.tabs);
        final ViewPager viewPager= (ViewPager) findViewById(R.id.viewpager);
        Intent intent= getIntent();

        viewPager.setTag(R.string.SELECTED_LEAGUE_ID,intent.getIntExtra(LigasActivity.CODI_LLIGA, 0));
        tabs.addTab(tabs.newTab().setText("Classificació"),0);
        tabs.addTab(tabs.newTab().setText("Equips"),1);
        tabs.addTab(tabs.newTab().setText("Jornada"),2);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectedLeagueActivity.this.finish();
            }
        });
    }


}
