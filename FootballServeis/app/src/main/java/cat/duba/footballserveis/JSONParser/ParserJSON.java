package cat.duba.footballserveis.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cat.duba.footballserveis.Pojo.Classificacions;
import cat.duba.footballserveis.Pojo.Equips;
import cat.duba.footballserveis.Pojo.Jornades;
import cat.duba.footballserveis.Pojo.LlistaLligues;

/**
 * Created by abde on 19/04/16.
 */
public class ParserJSON {
    public static LlistaLligues obteLlistaLligues(String dades) throws JSONException {
        JSONArray jsonArray= new JSONArray(dades);
        LlistaLligues lligues=new LlistaLligues();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject=jsonArray.getJSONObject(i);
            JSONObject links=jsonObject.getJSONObject("_links");
            JSONObject self, teams, fixtures, leagueTable;
            self=links.getJSONObject("self");
            teams=links.getJSONObject("teams");
            fixtures= links.getJSONObject("fixtures");
            leagueTable=links.getJSONObject("leagueTable");
            LlistaLligues.Lligues lliga=new LlistaLligues.Lligues();
            lliga.setSelf(self.getString("href"));
            lliga.setTeams(teams.getString("href"));
            lliga.setFixtures(fixtures.getString("href"));
            lliga.setLeagueTable(leagueTable.getString("href"));
            lliga.setId(jsonObject.getInt("id"));
            lliga.setCaption(jsonObject.getString("caption"));
            lliga.setLeague(jsonObject.getString("league"));
            lliga.setYear(jsonObject.getString("year"));
            lliga.setCurrentMatchday(jsonObject.getInt("currentMatchday"));
            lliga.setNumberOfMatchdays(jsonObject.getInt("numberOfMatchdays"));
            lliga.setNumberOfTeams(jsonObject.getInt("numberOfTeams"));
            lliga.setNumberOfGames(jsonObject.getInt("numberOfGames"));

            lligues.getLliguesArrayList().add(lliga);
        }

        return lligues;
    }

    public static Classificacions obteLlistaClassificacions(String dades) throws JSONException {
        Classificacions classificacions=new Classificacions();
        JSONObject jsonObject=new JSONObject(dades);
        JSONObject links=jsonObject.getJSONObject("_links");
        JSONObject self, soccerseason;
        self=links.getJSONObject("self");
        soccerseason=links.getJSONObject("soccerseason");
        classificacions.setSelf(self.getString("href"));;
        classificacions.setSoccerSeason(soccerseason.getString("href"));
        classificacions.setLeagueCaption(jsonObject.getString("leagueCaption"));
        classificacions.setMatchDay(jsonObject.getInt("matchday"));


        JSONArray standing=jsonObject.getJSONArray("standing");
        Classificacions.Equip equip;
        for (int i = 0; i < standing.length(); i++) {
            equip = new Classificacions.Equip();
            JSONObject teamArray=standing.getJSONObject(i);
            JSONObject linksTeam=teamArray.getJSONObject("_links");
            JSONObject team=linksTeam.getJSONObject("team");
            equip.setLink(team.getString("href"));
            equip.setPosition(teamArray.getInt("position"));
            equip.setNom(teamArray.getString("teamName"));
            equip.setLinkLogo(teamArray.getString("crestURI"));
            equip.setPlayedGames(teamArray.getInt("playedGames"));
            equip.setPoints(teamArray.getInt("points"));
            equip.setGoals(teamArray.getInt("goals"));
            equip.setGoalsAgainst(teamArray.getInt("goalsAgainst"));
            equip.setWins(teamArray.getInt("wins"));
            equip.setDraws(teamArray.getInt("draws"));
            equip.setLosses(teamArray.getInt("losses"));
            classificacions.getEquip().add(equip);
        }

        return classificacions;
    }

    public static Equips obteLlistaEquips(String dades) throws JSONException {
        Equips equips=new Equips();
        JSONObject jsonObject=new JSONObject(dades);
        JSONObject links=jsonObject.getJSONObject("_links");
        JSONObject self, soccerseason;
        self=links.getJSONObject("self");
        soccerseason=links.getJSONObject("soccerseason");
        equips.setSelf(self.getString("href"));;
        equips.setSoccerseason(soccerseason.getString("href"));
        equips.setCount(jsonObject.getInt("count"));
        JSONArray equipsArray=jsonObject.getJSONArray("teams");
        for (int i = 0; i < equipsArray.length(); i++) {
            Equips.Equip equip= new Equips.Equip();
            equip.setSelf(equipsArray.getJSONObject(i).getJSONObject("_links").getJSONObject("self").getString("href"));
            equip.setFixtures(equipsArray.getJSONObject(i).getJSONObject("_links").getJSONObject("fixtures").getString("href"));
            equip.setPlayers(equipsArray.getJSONObject(i).getJSONObject("_links").getJSONObject("players").getString("href"));
            equip.setName(equipsArray.getJSONObject(i).getString("name"));
            equip.setCode(equipsArray.getJSONObject(i).getString("code"));
            equip.setShortName(equipsArray.getJSONObject(i).getString("shortName"));
            equip.setSquadMarketValue(equipsArray.getJSONObject(i).getString("name"));
            equip.setCrestUrl(equipsArray.getJSONObject(i).getString("crestUrl"));
            equips.getEquips().add(equip);
        }
        return equips;
    }

    public static Jornades obteLlistaJornades(String dades) throws JSONException {
        Jornades jornades=new Jornades();
        JSONObject jsonObject=new JSONObject(dades);
        JSONArray fixtures=jsonObject.getJSONArray("fixtures");
        for (int i = 0; i < fixtures.length(); i++) {
            Jornades.Jornada jornada= new Jornades.Jornada();
            jornada.setDate(fixtures.getJSONObject(i).getString("date"));
            jornada.setStatus(fixtures.getJSONObject(i).getString("status"));
            jornada.setMatchday(fixtures.getJSONObject(i).getInt("matchday"));
            jornada.setHomeTeamName(fixtures.getJSONObject(i).getString("homeTeamName"));
            jornada.setAwayTeamName(fixtures.getJSONObject(i).getString("awayTeamName"));
            if(!fixtures.getJSONObject(i).getJSONObject("result").isNull("goalsHomeTeam"))
                jornada.setGoalsHomeTeam(fixtures.getJSONObject(i).getJSONObject("result").getInt("goalsHomeTeam"));
            else
                jornada.setGoalsHomeTeam(-1);
            if(!fixtures.getJSONObject(i).getJSONObject("result").isNull("goalsAwayTeam"))
                jornada.setGoalsAwayTeam(fixtures.getJSONObject(i).getJSONObject("result").getInt("goalsAwayTeam"));
            else
                jornada.setGoalsAwayTeam(-1);
            jornades.getJornada().add(jornada);
        }
        return jornades;
    }
}
