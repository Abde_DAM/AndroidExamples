package cat.duba.footballserveis.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import cat.duba.footballserveis.Pojo.LlistaLligues;
import cat.duba.footballserveis.R;

/**
 * Created by abde on 19/04/16.
 */
public class LlistaLliguesAdapter extends BaseAdapter {
    LlistaLligues lligues;
    LayoutInflater layoutInflater;

    public LlistaLliguesAdapter(LlistaLligues lligues, Context context) {
        this.lligues = lligues;
        layoutInflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return lligues.getLliguesArrayList().size();
    }

    @Override
    public Object getItem(int position) {
        return lligues.getLliguesArrayList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return lligues.getLliguesArrayList().get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;
        if(vi==null)
        {
            vi=layoutInflater.inflate(R.layout.llista_lligues_adapter,parent, false);
            holder=new ViewHolder();
            holder.tvCaption= (TextView) vi.findViewById(R.id.tvCaption);
            holder.tvCurrentMatchDay= (TextView) vi.findViewById(R.id.tvCurrentMatchday);
            holder.tvNumberOfGames= (TextView) vi.findViewById(R.id.tvNumberOfGames);
            holder.tvNumberOfTeams= (TextView) vi.findViewById(R.id.tvNumberOfTeams);
            holder.tvNumberMatchdays= (TextView) vi.findViewById(R.id.tvNumberMatchdays);
            vi.setTag(holder);
        }
        else {

            // View recycled !

            // no need to inflate

            // no need to findViews by id

            holder = (ViewHolder) vi.getTag();

        }
        LlistaLligues.Lligues item = (LlistaLligues.Lligues) getItem(position);

        holder.tvCaption.setText("Liga: "+item.getCaption());
        holder.tvCurrentMatchDay.setText("Jornada: "+item.getCurrentMatchday());
        holder.tvNumberMatchdays.setText("Total de jornadas: "+item.getNumberOfMatchdays());
        holder.tvNumberOfGames.setText("Partidos jugados: "+item.getNumberOfGames());
        holder.tvNumberOfTeams.setText("Número de equipos: "+item.getNumberOfTeams());

        return vi;
    }
    static class ViewHolder{
        TextView tvCurrentMatchDay, tvNumberOfTeams, tvNumberOfGames, tvCaption, tvNumberMatchdays;
    }
}
