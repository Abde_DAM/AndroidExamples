package com.example.sergi.sqlteoria;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import DAO.ProvinciaDAO;
import Model.Municipi;
import Model.Provincia;

public class MainActivity extends AppCompatActivity
{
    MunicipisSQLHelper dbHelper;
    SQLiteDatabase db;
    Spinner spnProvincies;
    ListView lstMunicipis;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spnProvincies = (Spinner) findViewById(R.id.spnProvincies);
        lstMunicipis = (ListView) findViewById(R.id.lstMunicipis);

        dbHelper = new MunicipisSQLHelper(this);
        db = dbHelper.getWritableDatabase();

        carregarProvinciesASpinner();
        spnProvincies.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                carregaMunicipisAlListview();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
                lstMunicipis.setAdapter(null);
            }
        });

    }

    @Override
    protected void onStop()
    {
        if(db != null)
        {
            db.close();
        }
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.mnuOmple:
                omple();
            case R.id.mnuModifica:
                return true;
            case R.id.mnuElimina:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    //region METODES
    private void omple()
    {
        try
        {
            db.execSQL(AppConstants.deleteProvinicies);
            db.execSQL(AppConstants.deleteMunicipis);
        }
        catch (Exception e)
        {
            Log.d("NoEsborra", e.getMessage());
        }
        ompleProvincies();
        ompleMunicipis();
    }

    private void ompleMunicipis()
    {
        ArrayList<Municipi> municipis = new ArrayList<>();
        municipis = Fitxers.obtenirMunicipis(this);

        if(db != null)
        {
            ContentValues nouRegistre;
            for(Municipi m : municipis)
            {
                nouRegistre = new ContentValues();
                nouRegistre.put(AppConstants.COLUMNA_ID_MUNICIPIS, m.getCodiIne());
                nouRegistre.put(AppConstants.COLUMNA_ID_PROVINCIA, m.getCodiProvincia());
                nouRegistre.put(AppConstants.COLUMNA_NOM_MUNICIPIS, m.getNomMunicipi());


                db.insert(AppConstants.TAULA_MUNICIPIS, null, nouRegistre);
            }
        }
    }

   private void ompleProvincies()
    {
        Map<Integer, String> llistaProvincies = new HashMap<Integer,String>();
        llistaProvincies = Fitxers.obtenirTaulaProvincies(this);

        /*if(db != null)
        {
            for(Integer codiProvincia : llistaProvincies.keySet())
            {
                db.execSQL("INSERT INTO " + AppConstants.TAULA_PROVINCIES + " (" +
                        AppConstants.COLUMNA_ID_PROVINCIA + ", " +
                        AppConstants.COLUMNA_NOM_PROVINCIA + ") " + "VALUES ('" + codiProvincia + "', '" + llistaProvincies.get(codiProvincia) + "');");
            }
        }*/
        ProvinciaDAO provDAO = new ProvinciaDAO(this);
        try
        {
            provDAO.obre();

            for(Integer i:llistaProvincies.keySet())
            {
                provDAO.creaProvincia(i, llistaProvincies.get(i));
            }
        }
        catch (SQLiteException e)
        {
            Log.e("SQLERROR", "ProvinciaDAO - " +e.getMessage());
        }
        finally
        {
            provDAO.tanca();
        }
    }

    private void carregarProvinciesASpinner()
    {
        Cursor cursor = db.rawQuery("SELECT * FROM "+ AppConstants.TAULA_PROVINCIES, null);

        SimpleCursorAdapter adaptadorSpinner = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor,
                new String[]{AppConstants.COLUMNA_ID_PROVINCIA, AppConstants.COLUMNA_NOM_PROVINCIA},
                new int[]{android.R.id.text2, android.R.id.text1}, 0);

        spnProvincies.setAdapter(adaptadorSpinner);
    }

    private void carregaMunicipisAlListview()
    {
        String provinciaSeleccionada;
        int posicio = spnProvincies.getSelectedItemPosition();

        SimpleCursorAdapter adaptSpn = (SimpleCursorAdapter) spnProvincies.getAdapter();

        Cursor cursorSpinner = adaptSpn.getCursor();
        provinciaSeleccionada = cursorSpinner.getString(1);// 0 es _id

        Cursor cursorListView = db.query(AppConstants.TAULA_MUNICIPIS, null, AppConstants.COLUMNA_ID_PROVINCIA + " = ? ", new String [] {provinciaSeleccionada}, null, null, AppConstants.COLUMNA_NOM_MUNICIPIS);

        SimpleCursorAdapter adapt = new SimpleCursorAdapter(this, android.R.layout.activity_list_item, cursorListView, new String[]
                {AppConstants.COLUMNA_ID_MUNICIPIS, AppConstants.COLUMNA_NOM_MUNICIPIS}, new int[]{android.R.id.text2, android.R.id.text1}, 0);
        lstMunicipis.setAdapter(adapt);
    }
    //endregion
}
