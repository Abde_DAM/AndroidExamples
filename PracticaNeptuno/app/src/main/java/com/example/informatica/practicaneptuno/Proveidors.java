package com.example.informatica.practicaneptuno;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.informatica.practicaneptuno.Adaptadors.AdaptadorProveedores;
import com.example.informatica.practicaneptuno.DAO.ProveedoresDAO;
import com.example.informatica.practicaneptuno.POJO.Proveedores;

public class Proveidors extends AppCompatActivity {
    ListView lvProveedores;
    ProveedoresDAO proveedoresDAO;
    static String MODIFICARPROVEIDOR="proveidorAModificar", ESMODIFICAT="EsModificat";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proveidors);
        proveedoresDAO=new ProveedoresDAO(this);
        proveedoresDAO.obre();
        lvProveedores= (ListView) findViewById(R.id.lvProveedores);

        lvProveedores.setAdapter(new AdaptadorProveedores(proveedoresDAO.obteProveedores(), this));

        lvProveedores.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                String[] opcions = new String[]{"Modificar", "Eliminar"};
                ListView llista = new ListView(Proveidors.this);
                llista.setAdapter(new ArrayAdapter<String>(Proveidors.this, android.R.layout.simple_list_item_1, opcions));
                final AlertDialog dialog = (new AlertDialog.Builder(Proveidors.this)
                        .setTitle("Elige una opción")
                        .setView(llista)
                        .setCancelable(true)
                        .create());
                dialog.show();
                llista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int positionB, long id) {
                        switch (positionB) {
                            case 0:
                                Intent intent = new Intent(Proveidors.this, NouProveidor.class);
                                intent.putExtra(MODIFICARPROVEIDOR, (Proveedores) lvProveedores.getItemAtPosition(position));
                                intent.putExtra(ESMODIFICAT, true);
                                startActivity(intent);
                                dialog.dismiss();
                                break;
                            case 1:
                                int depenen = proveedoresDAO.EnDepenen((int) lvProveedores.getItemIdAtPosition(position));
                                if (depenen == 0) {
                                    proveedoresDAO.eliminaProveedores((int) lvProveedores.getItemIdAtPosition(position));
                                    Toast.makeText(Proveidors.this, "Proveedor eliminado", Toast.LENGTH_LONG).show();
                                    lvProveedores.setAdapter(new AdaptadorProveedores(proveedoresDAO.obteProveedores(), Proveidors.this));
                                    dialog.dismiss();
                                } else {
                                    Toast.makeText(Proveidors.this, "No se puede eliminar este proveedor " + depenen, Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }
                                break;
                        }
                    }
                });


                return false;
            }
        });

    }


    @Override
    protected void onResume() {
        lvProveedores.setAdapter(new AdaptadorProveedores(proveedoresDAO.obteProveedores(), Proveidors.this));
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        proveedoresDAO.tanca();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_proveidors, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnuAlta:
                startActivity(new Intent(this, NouProveidor.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
