package com.example.informatica.practicaneptuno;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.informatica.practicaneptuno.Adaptadors.AdaptadorProductes;
import com.example.informatica.practicaneptuno.DAO.ProductosDAO;
import com.example.informatica.practicaneptuno.POJO.Productos;

import java.util.ArrayList;

public class ProductesPerCategoria extends AppCompatActivity {
    ListView lvProductes;
    ProductosDAO productosDAO;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productes_per_categoria);
        Intent intent=getIntent();
        productosDAO=new ProductosDAO(this);
        productosDAO.obre();
        ArrayList<Productos> productos= (ArrayList<Productos>) productosDAO.obteProductosAmbCategoria((int)intent.getLongExtra("idCategoriaProducte", -1));
        lvProductes= (ListView) findViewById(R.id.lvProductes);
        lvProductes.setAdapter(new AdaptadorProductes(productos, this));
        productosDAO.tanca();
    }
}
