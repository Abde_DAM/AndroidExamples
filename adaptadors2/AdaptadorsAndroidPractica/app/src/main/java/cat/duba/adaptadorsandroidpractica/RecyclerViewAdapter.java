package cat.duba.adaptadorsandroidpractica;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class RecyclerViewAdapter extends AppCompatActivity {
    RecyclerView rvLlista;
    ArrayList municipis;
    int [] dibuixos;
    Random rnd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_adapter);
//Municipi producte
        //provincia categoria
        Intent intent=getIntent();
        municipis = intent.getParcelableArrayListExtra(MainActivity.PRODUCTES);
        dibuixos = new int[] {android.R.drawable.ic_delete,
                android.R.drawable.ic_dialog_alert,
                android.R.drawable.ic_dialog_map,
                android.R.drawable.ic_lock_silent_mode_off,
                android.R.drawable.ic_popup_sync,
                android.R.drawable.ic_lock_lock,
                android.R.drawable.ic_lock_silent_mode_off};
        rnd = new Random();
        rvLlista = (RecyclerView) findViewById(R.id.rcvLlista);
        rvLlista.setHasFixedSize(true);
        AdaptadorReciclable adaptadorMunicipis = new AdaptadorReciclable(this, municipis);
        rvLlista.setAdapter(adaptadorMunicipis);
        adaptadorMunicipis.asignaEscoltadorDeClics(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView nom = (TextView) v.findViewById(R.id.tvMunicipi);
                String nomMunicipi = nom.getText().toString();
                Toast.makeText(ReciclerView.this, nomMunicipi, Toast.LENGTH_SHORT).show();
            }
        });
        rvLlista.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private class AdaptadorReciclable extends RecyclerView.Adapter<AdaptadorReciclable.ContenidorReciclable>{
        ArrayList<Municipi> dades;
        Context context;
        View.OnClickListener escoltador;

        public AdaptadorReciclable (Context context, ArrayList<Municipi> dades)
        {
            this.dades = dades;
            this.context = context;
        }
        @Override
        public ContenidorReciclable onCreateViewHolder(ViewGroup parent, int viewType) {
            View vista = getLayoutInflater().inflate(R.layout.municipi, null);
            vista.setOnClickListener(escoltador);
            return new ContenidorReciclable(vista);
        }

        @Override
        public void onBindViewHolder(ContenidorReciclable holder, int position) {
            Municipi m = dades.get(position);
            holder.EnllaçaMunicipi(m);
        }

        @Override
        public int getItemCount() {
            return dades.size();
        }


        public void asignaEscoltadorDeClics(View.OnClickListener escoltador) {

            this.escoltador = escoltador;
        }

        public class ContenidorReciclable extends RecyclerView.ViewHolder{
            ImageView ivImatge;
            TextView tvMunicipi, tvProcincia;
            public ContenidorReciclable(View itemView) {
                super(itemView);
                ivImatge= (ImageView) itemView.findViewById(R.id.ivImatge);
                tvMunicipi = (TextView) itemView.findViewById(R.id.tvMunicipi);
                tvProcincia = (TextView) itemView.findViewById(R.id.tvProvincia);
            }

            public void EnllaçaMunicipi (Municipi m)
            {
                ivImatge.setImageResource(dibuixos[rnd.nextInt(dibuixos.length)]);
                tvMunicipi.setText(m.getNomMunicipi());
                tvProcincia.setText(m.getNomProvincia());
            }
        }
    }
}