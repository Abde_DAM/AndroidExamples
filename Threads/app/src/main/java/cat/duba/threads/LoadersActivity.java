package cat.duba.threads;

import android.app.LoaderManager;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import cat.duba.threads.Pojo.LoaderMunicipis;
import cat.duba.threads.Pojo.Municipi;

public class LoadersActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<Municipi>> {
    ListView lvLlista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loaders);
        lvLlista= (ListView) findViewById(R.id.lvLlista);
        getLoaderManager().initLoader(0,null, this);
    }

    @Override
    public Loader<ArrayList<Municipi>> onCreateLoader(int id, Bundle args) {
        return new LoaderMunicipis(this);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Municipi>> loader, ArrayList<Municipi> data) {
        ArrayAdapter<Municipi> adapter=new ArrayAdapter<Municipi>(this, android.R.layout.simple_list_item_1, data);
        lvLlista.setAdapter(adapter);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Municipi>> loader) {

    }
}
