package cat.duba.threads.Pojo;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.ArrayList;

/**
 * Created by abde on 11/04/16.
 */
public class LoaderMunicipis extends AsyncTaskLoader<ArrayList<Municipi>> {

    Context context;
    ArrayList<Municipi> municipis=null;

    public LoaderMunicipis(Context context) {
        super(context);
        this.context=context;
    }

    @Override
    public ArrayList<Municipi> loadInBackground() {
        municipis=Fitxers.obtenirMunicipis(context);
        return municipis;
    }

    @Override
    protected void onStartLoading() {
        if(municipis!=null)
        {
            deliverResult(municipis);
        }
        else if(takeContentChanged()||municipis==null)
        {
            forceLoad();
        }
    }
}
