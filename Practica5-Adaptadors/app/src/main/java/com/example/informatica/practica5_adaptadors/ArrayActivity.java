package com.example.informatica.practica5_adaptadors;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class ArrayActivity extends AppCompatActivity {

    Spinner spnCategories;
    ArrayList<Producte> productes;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_array);

        Intent intent = getIntent();
//        productes = intent.getParcelableArrayListExtra(MainActivity.PRODUCTES);

        spnCategories = (Spinner) findViewById(R.id.spnCategories);
        spnCategories.setAdapter(ArrayAdapter.createFromResource(this,
                R.array.categories,
                android.R.layout.simple_dropdown_item_1line));
    }
}