package com.example.informatica.practica5_adaptadors;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;

public class BaseActivity extends AppCompatActivity {

    Spinner spnCategories;
    ArrayList<Producte> productes;
    ListView lstLlista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        lstLlista= (ListView) findViewById(R.id.lstLlista);
        Intent intent = getIntent();
        productes = intent.getParcelableArrayListExtra(MainActivity.PRODUCTES);

        spnCategories = (Spinner) findViewById(R.id.spnCategories);
        spnCategories.setAdapter(ArrayAdapter.createFromResource(this,
                R.array.categories,
                android.R.layout.simple_dropdown_item_1line));
    }
}