package cat.duba.toast;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SegonActivity extends AppCompatActivity implements View.OnClickListener {
    EditText etAdreça, etTelefon, etEmail;
    Button btnAcceptar, btnCancelar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segon);
        etAdreça= (EditText) findViewById(R.id.etAdreça);
        etTelefon= (EditText) findViewById(R.id.etTelefon);
        etEmail= (EditText) findViewById(R.id.etEmail);
        btnAcceptar= (Button) findViewById(R.id.btnAccept);
        btnCancelar= (Button) findViewById(R.id.btnCancel);
        btnAcceptar.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);
    }
    public void onClick(View v) {
        Intent intent=getIntent();
        switch (v.getId())
        {
            case R.id.btnAccept:
                intent.putExtra("ADREÇA", etAdreça.getText().toString());
                intent.putExtra("TELEFON", etTelefon.getText().toString());
                intent.putExtra("EMAIL", etEmail.getText().toString());
                break;
        }
        setResult(RESULT_OK, intent);
        finish();
    }
}
