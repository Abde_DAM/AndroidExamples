package cat.duba.serveis;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONException;

public class MainActivity extends AppCompatActivity {
    TextView tvTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvTextView= (TextView) findViewById(R.id.tvText);
        new MeteoTasca().execute();


    }
    class MeteoTasca extends AsyncTask<String, Void, Meteo>
    {
        String cadena;
        Meteo meteo;
        @Override
        protected Meteo doInBackground(String... params) {
            MeteoHttpClient client=new MeteoHttpClient();
             cadena=client.obtenirDadesMeteo("Girona");
            try {
                meteo=JSONMeteoParser.obteMeteo(cadena);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return meteo;
        }

        @Override
        protected void onPostExecute(Meteo meteo) {
            setTitle(cadena);
            tvTextView.setText(meteo.toString());

            super.onPostExecute(meteo);
        }
    }
}
