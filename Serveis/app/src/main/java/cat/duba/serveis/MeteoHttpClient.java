package cat.duba.serveis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by abde on 12/04/16.
 */
public class MeteoHttpClient {
    private final static String BASE_URL="http://api.openweathermap.org/data/2.5/weather?q=", APPID="&appid=b221fb28bf2f8e25cf4b16d41489e525";

    public String obtenirDadesMeteo(String ciutat)
    {
//http://www.omdbapi.com/?
        HttpURLConnection connexio=null;
        InputStream inputStream=null;
        try{
            connexio=(HttpURLConnection)(new URL(BASE_URL+ciutat+APPID).openConnection());
            connexio.setRequestMethod("GET");
            connexio.connect();

            StringBuilder buffer=new StringBuilder();
            inputStream=connexio.getInputStream();

            BufferedReader br=new BufferedReader(new InputStreamReader(inputStream));

            String linia=null;
            while ((linia=br.readLine())!=null)
            {
                buffer.append(linia);
            }

            br.close();
            inputStream.close();
            connexio.disconnect();
            return buffer.toString();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
