package cat.duba.serveis;

import java.util.ArrayList;

/**
 * Created by abde on 12/04/16.
 */
public class Meteo {
    private long id;
    private String nom;
    private int code;
    private long data;
    private String base;
    private Coordenada coordenada=new Coordenada();
    private ArrayList<Temps> temps=new ArrayList<>();
    private Principal principal=new Principal();
    private Vent vent=new Vent();

    @Override
    public String toString() {
        return "Meteo{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", code=" + code +
                ", data=" + data +
                ", base='" + base + '\'' +
                ", coordenada=" + coordenada +
                ", temps=" + temps +
                ", principal=" + principal +
                ", vent=" + vent +
                ", nuvols=" + nuvols +
                ", sistema=" + sistema +
                '}';
    }

    private Nuvols nuvols=new Nuvols();
    private Sistema sistema=new Sistema();

    public Meteo()
    {
        temps.add(new Temps());
    }

//region POJO
    public class Coordenada{
        double longitut, latitud;

    @Override
    public String toString() {
        return "Coordenada{" +
                "longitut=" + longitut +
                ", latitud=" + latitud +
                '}';
    }

    public double getLongitut() {
            return longitut;
        }

        public void setLongitut(double longitut) {
            this.longitut = longitut;
        }

        public double getLatitud() {
            return latitud;
        }

        public void setLatitud(double latitud) {
            this.latitud = latitud;
        }
    }

    public class Temps{
        @Override
        public String toString() {
            return "Temps{" +
                    "id=" + id +
                    ", main='" + main + '\'' +
                    ", descripcio='" + descripcio + '\'' +
                    ", icona='" + icona + '\'' +
                    '}';
        }

        private int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMain() {
            return main;
        }

        public void setMain(String main) {
            this.main = main;
        }

        public String getDescripcio() {
            return descripcio;
        }

        public void setDescripcio(String descripcio) {
            this.descripcio = descripcio;
        }

        public String getIcona() {
            return icona;
        }

        public void setIcona(String icona) {
            this.icona = icona;
        }

        private String main, descripcio, icona;
    }

    public class Principal{
        @Override
        public String toString() {
            return "Principal{" +
                    "temperatura=" + temperatura +
                    ", pressio=" + pressio +
                    ", humitat=" + humitat +
                    ", temperaturaMinima=" + temperaturaMinima +
                    ", temperaturaMaxima=" + temperaturaMaxima +
                    '}';
        }

        public double getTemperatura() {
            return temperatura;
        }

        public void setTemperatura(double temperatura) {
            this.temperatura = temperatura;
        }

        public double getPressio() {
            return pressio;
        }

        public void setPressio(double pressio) {
            this.pressio = pressio;
        }

        public double getHumitat() {
            return humitat;
        }

        public void setHumitat(double humitat) {
            this.humitat = humitat;
        }

        public double getTemperaturaMinima() {
            return temperaturaMinima;
        }

        public void setTemperaturaMinima(double temperaturaMinima) {
            this.temperaturaMinima = temperaturaMinima;
        }

        public double getTemperaturaMaxima() {
            return temperaturaMaxima;
        }

        public void setTemperaturaMaxima(double temperaturaMaxima) {
            this.temperaturaMaxima = temperaturaMaxima;
        }

        private double temperatura, pressio, humitat, temperaturaMinima, temperaturaMaxima;
    }
    public class Vent
    {
        private double velocitat;

        public double getGraus() {
            return graus;
        }

        public void setGraus(double graus) {
            this.graus = graus;
        }

        public double getVelocitat() {
            return velocitat;
        }

        public void setVelocitat(double velocitat) {
            this.velocitat = velocitat;
        }

        private double graus;
    }

    public class Nuvols
    {
        @Override
        public String toString() {
            return "Nuvols{" +
                    "tot=" + tot +
                    '}';
        }

        private int tot;

        public int getTot() {
            return tot;
        }

        public void setTot(int tot) {
            this.tot = tot;
        }
    }

    public class Sistema{
        @Override
        public String toString() {
            return "Sistema{" +
                    "tipus=" + tipus +
                    ", id=" + id +
                    ", missatge=" + missatge +
                    ", Pais='" + Pais + '\'' +
                    ", alba=" + alba +
                    ", ocas=" + ocas +
                    '}';
        }

        private int tipus, id;

        public int getTipus() {
            return tipus;
        }

        public void setTipus(int tipus) {
            this.tipus = tipus;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public double getMissatge() {
            return missatge;
        }

        public void setMissatge(double missatge) {
            this.missatge = missatge;
        }

        public String getPais() {
            return Pais;
        }

        public void setPais(String pais) {
            Pais = pais;
        }

        public long getAlba() {
            return alba;
        }

        public void setAlba(long alba) {
            this.alba = alba;
        }

        public long getOcas() {
            return ocas;
        }

        public void setOcas(long ocas) {
            this.ocas = ocas;
        }

        private double missatge;
        private String Pais;
        private long alba;
        private long ocas;
    }
    //endregion
//region GETTERS AND SETTERS
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public long getData() {
        return data;
    }

    public void setData(long data) {
        this.data = data;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Coordenada getCoordenada() {
        return coordenada;
    }

    public void setCoordenada(Coordenada coordenada) {
        this.coordenada = coordenada;
    }

    public ArrayList<Temps> getTemps() {
        return temps;
    }

    public void setTemps(ArrayList<Temps> temps) {
        this.temps = temps;
    }

    public Principal getPrincipal() {
        return principal;
    }

    public void setPrincipal(Principal principal) {
        this.principal = principal;
    }

    public Vent getVent() {
        return vent;
    }

    public void setVent(Vent vent) {
        this.vent = vent;
    }

    public Nuvols getNuvols() {
        return nuvols;
    }

    public void setNuvols(Nuvols nuvols) {
        this.nuvols = nuvols;
    }

    public Sistema getSistema() {
        return sistema;
    }

    public void setSistema(Sistema sistema) {
        this.sistema = sistema;
    }
    //endregion
}
