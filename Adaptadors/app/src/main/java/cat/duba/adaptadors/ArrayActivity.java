package cat.duba.adaptadors;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class ArrayActivity extends AppCompatActivity {
    Button btnDades, btnColor;
    Spinner spnDades, spnColors;
    TextView tvTitol;
    ListView lstLlista;
    View titol,peu;
    AdapterView.OnItemSelectedListener escoltadorItemSelected;
    final String[] DADES={"Pata","Peta","Pota","Pita", "Pilota"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_array);

        btnDades= (Button) findViewById(R.id.btnDades);
        btnColor= (Button) findViewById(R.id.btnColors);
        spnDades= (Spinner) findViewById(R.id.spnDades);
        spnColors= (Spinner) findViewById(R.id.spnColors);
        tvTitol= (TextView) findViewById(R.id.tvTitol);
        lstLlista= (ListView) findViewById(R.id.lstLlista);

        spnDades.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, DADES));

        spnColors.setAdapter(ArrayAdapter.createFromResource(this, R.array.colors, android.R.layout.simple_dropdown_item_1line));

        escoltadorItemSelected = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String clicat=parent.getItemAtPosition(position).toString();
                tvTitol.setText("Llistes ("+clicat+")");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                tvTitol.setText("Llistes");
            }
        };

        spnColors.setOnItemSelectedListener(escoltadorItemSelected);
        spnDades.setOnItemSelectedListener(escoltadorItemSelected);


        btnDades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    /*lstLlista.setAdapter(new ArrayAdapter<String>(
                            ArrayActivity.this, android.R.layout.simple_list_item_1,DADES
                    ));*/
                lstLlista.setAdapter((ArrayAdapter<String>) spnDades.getAdapter());
                afegeixTitol("Dades");
                afegeixPeu(String.valueOf(lstLlista.getAdapter().getCount())+" elements");
            }
        });
        btnColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    /*lstLlista.setAdapter(new ArrayAdapter<String>(
                            ArrayActivity.this, android.R.layout.simple_list_item_1,DADES
                    ));*/
                lstLlista.setAdapter((ArrayAdapter<String>) spnColors.getAdapter());
                afegeixTitol("Colors");
                afegeixPeu(String.valueOf(lstLlista.getAdapter().getCount()) + " elements");
            }
        });



    }

    private void afegeixPeu(String dades) {
        lstLlista.removeFooterView(peu);
        peu=getLayoutInflater().inflate(R.layout.peu,null);
        TextView tvPeu= (TextView) peu.findViewById(R.id.tvElements);
        tvPeu.setText(dades);
        lstLlista.addFooterView(peu);
    }

    private void afegeixTitol(String dades) {
        lstLlista.removeHeaderView(titol);
        titol=getLayoutInflater().inflate(R.layout.cabecera,null);
        TextView tvCabecera= (TextView) titol.findViewById(R.id.tvTipus);
        tvCabecera.setText(dades);
        lstLlista.addHeaderView(titol);
    }
}

