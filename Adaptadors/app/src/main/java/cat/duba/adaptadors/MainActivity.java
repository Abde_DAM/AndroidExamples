package cat.duba.adaptadors;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button btnArray, btnBase,  btnExpandable, btnReciclable;
    ArrayList<Municipi> municipis;
    final static String MUNICIPIS="Municipis";
    private void omplirMunicipis()
    {
        try
        {
            String linia;
            String[] camps;

            // LLEGIR EL FITXER
            InputStream fitxerRecursos = getResources().openRawResource(R.raw.provincies_municipis);
            BufferedReader brEntrada = new BufferedReader(new InputStreamReader(fitxerRecursos));

            while ((linia = brEntrada.readLine()) != null)
            {
                camps = linia.split(";");
                municipis.add(
                        new Municipi(Integer.parseInt(camps[0]), Integer.parseInt(camps[3]), camps[1], camps[2]));
            }

            brEntrada.close();
        }
        catch (Exception e)
        {
            Log.e("Lectura Municipis", "Error en la lectura dels municipis");
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        municipis=new ArrayList<Municipi>();
        btnArray= (Button) findViewById(R.id.btnArray);
        btnArray.setOnClickListener(this);
        btnBase= (Button) findViewById(R.id.btnBase);
        btnBase.setOnClickListener(this);
        btnExpandable= (Button) findViewById(R.id.btnExpandable);
        btnExpandable.setOnClickListener(this);
        btnReciclable= (Button) findViewById(R.id.btnReciclable);
        btnReciclable.setOnClickListener(this);

        omplirMunicipis();

    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId())
        {
            case R.id.btnArray:
                startActivity(new Intent(this,ArrayActivity.class));
                break;
            case R.id.btnBase:
                intent=new Intent(this, BaseActivity.class);
                intent.putParcelableArrayListExtra(MUNICIPIS, municipis);
                startActivity(intent);
                break;
            case R.id.btnExpandable:
                break;
            case R.id.btnReciclable:
                break;
        }
    }
}
