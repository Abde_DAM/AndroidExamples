package cat.duba.adaptadors;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class BaseActivity extends AppCompatActivity {

    ListView lstLlista;
    ArrayList<Municipi> municipis;
    int[] dibuixos;
    Random rnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        lstLlista= (ListView) findViewById(R.id.lstLlista);

        Intent intent=getIntent();
        municipis=intent.getParcelableArrayListExtra(MainActivity.MUNICIPIS);
        dibuixos =new int[]{
                android.R.drawable.ic_delete,
                android.R.drawable.ic_btn_speak_now,
                android.R.drawable.ic_dialog_alert,
                android.R.drawable.ic_dialog_info,
                android.R.drawable.ic_input_get,
                android.R.drawable.ic_media_next,
                android.R.drawable.ic_menu_sort_by_size
        };
        rnd=new Random();

        lstLlista= (ListView) findViewById(R.id.lstLlista);
        lstLlista.setAdapter(new AdaptadorMunicipis(this,municipis));
    }

    private class AdaptadorMunicipis extends BaseAdapter
    {
        Context contexte;
        ArrayList<Municipi> municipis;
        public AdaptadorMunicipis(Context contexte, ArrayList<Municipi> dades)
        {
            this.contexte=contexte;
            municipis=dades;

        }

        @Override
        public int getCount() {
            return municipis.size();
        }

        @Override
        public Object getItem(int position) {
            return municipis.get(position);
        }

        @Override
        public long getItemId(int position) {
            return municipis.get(position).getCodiINE();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflador=getLayoutInflater();
            convertView=inflador.inflate(R.layout.municipi, null);

            ImageView ivImatge= (ImageView) convertView.findViewById(R.id.ivImatge);
            TextView tvMunicipi= (TextView) convertView.findViewById(R.id.tvMunicipi);
            TextView tvProvincia= (TextView) convertView.findViewById(R.id.tvPrivincia);

            ivImatge.setImageResource(dibuixos[rnd.nextInt(dibuixos.length)]);
            tvMunicipi.setText(((Municipi) getItem(position)).getNomMunicipi());
            tvProvincia.setText(((Municipi)getItem(position)).getNomProvincia());
            return null;
        }
    }
}
