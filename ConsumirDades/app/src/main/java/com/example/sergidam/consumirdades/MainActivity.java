package com.example.sergidam.consumirdades;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {
    Spinner spnProvincies;
    ListView lstMunicipis;

    Cursor cursorProvincies;
    Cursor cursorMunicipis;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spnProvincies = (Spinner) findViewById(R.id.spnProvincies);
        lstMunicipis = (ListView) findViewById(R.id.lstMunicipis);

        cursorProvincies = getContentResolver().query(ProvinciaContract.CONTENT_URI, null/*new String[]{ProvinciaContract.Columnes._ID ,ProvinciaContract.Columnes.COLUMNA_NOM_PROVINCIA}*/
        ,null, null, null);

        spnProvincies.setAdapter(new SimpleCursorAdapter(this, android.R.layout.simple_expandable_list_item_1, cursorProvincies, new String[]{ProvinciaContract.Columnes.COLUMNA_NOM_PROVINCIA},
                new int[]{android.R.id.text1},0));

        spnProvincies.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    carregaMunicipisAlListView(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                lstMunicipis.setAdapter(null);
            }
        });
    }

    private void carregaMunicipisAlListView(int position)
    {
        String provinciaSeleccionada;
        SimpleCursorAdapter adaptador = (SimpleCursorAdapter) spnProvincies.getAdapter();
        Cursor cursorSpinner = adaptador.getCursor();
        provinciaSeleccionada = cursorSpinner.getString(1);

        cursorMunicipis = this.getContentResolver().query(MunicipiContract.CONTENT_URI, null, MunicipiContract.Columnes.COLUMNA_ID_PROVINCIA + " = " + provinciaSeleccionada, null, null);

        lstMunicipis.setAdapter(new SimpleCursorAdapter(this, android.R.layout.activity_list_item, cursorMunicipis, new String[]{MunicipiContract.Columnes.COLUMNA_NOM_MUNICIPI},
                new int[]{android.R.id.text1},0));
    }
}
