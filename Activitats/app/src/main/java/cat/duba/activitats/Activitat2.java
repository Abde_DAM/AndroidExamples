package cat.duba.activitats;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class
        Activitat2 extends AppCompatActivity  implements View.OnClickListener {
    TextView tvSalutacio;
    Button btnRetorna;
    EditText etNom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activitat2);
        tvSalutacio= (TextView) findViewById(R.id.tvSalutacio);
        btnRetorna= (Button) findViewById(R.id.btnRetorna);
        etNom= (EditText) findViewById(R.id.etNom);
        Intent intent=getIntent();
        String nom=intent.getStringExtra("NOM");
        btnRetorna.setOnClickListener(this);

        tvSalutacio.setText("Hola "+nom+"!!!");


    }

    @Override
    public void onClick(View v) {
        Intent intent=getIntent();
        intent.putExtra("NOM", etNom.getText().toString());
        //setResult(RESULT_OK, intent);
        setResult(RESULT_CANCELED, intent);
        finish();
    }
}
