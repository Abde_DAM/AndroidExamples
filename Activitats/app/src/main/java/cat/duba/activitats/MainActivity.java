package cat.duba.activitats;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener {

    TextView text;
    EditText etEntrada;
    final int OBTENIR_NOM=1;
    Button btNovaActivitat, btnPreguntaNom, btnAccio;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        switch (requestCode)
        {
            case OBTENIR_NOM:
                if(resultCode==RESULT_OK)
                {
                    text.setText("Hola"+data.getStringExtra("NOM"));
                }
                else
                {
                    /*Toast.makeText(this, "No han entrat cap nom", Toast.LENGTH_LONG).show();
                    Toast torrada=Toast.makeText(this, "No han entrat cap nom", Toast.LENGTH_LONG);
                    torrada.show();*/
                    /*Toast torrada=new Toast(this);
                    LayoutInflater inflador=getLayoutInflater();
                    View vista=inflador.inflate(R.layout.torrada, (ViewGroup) findViewById(R.id.llLayout));
                    TextView tvText= (TextView) findViewById(R.id.tvTextTorrada);
                    tvText.setText("No han entrat cap nom");

                    torrada.setDuration(Toast.LENGTH_LONG);
                    torrada.setView(vista);
                    torrada.show();*/
//---------------------------------------------------------
                   /* LayoutInflater inflater = getLayoutInflater();

                    View layout = inflater.inflate(R.layout.torrada,
                            (ViewGroup) findViewById(R.id.custom_toast_layout_id));

                    // set a message
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Nom: ");

                    // Toast...
                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();*/
                }
                break;
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text= (TextView) findViewById(R.id.tvMissatges);
        //text.setText((text.getText()+"\n"+"Sóc a l'onCreate"));
        etEntrada= (EditText) findViewById(R.id.etEntrada);
        btnPreguntaNom= (Button) findViewById(R.id.btnPreguntaNom);
        btNovaActivitat= (Button) findViewById(R.id.btnNovaActivity);
        btnAccio= (Button) findViewById(R.id.btnAccio);
        btNovaActivitat.setOnClickListener(this);
        btnPreguntaNom.setOnClickListener(this);
        //text.setOnLongClickListener(this);
    }
/*
    @Override
    protected void onPause() {
        super.onPause();
        text.setText((text.getText() + "\n" + "Sóc a l'onPause"));

    }

    @Override
    protected void onStop() {
        super.onStop();
        text.setText((text.getText()+"\n"+"Sóc a l'onStop"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        text.setText((text.getText()+"\n"+"Sóc a l'onDestroy"));
    }

    @Override
    protected void onStart() {
        super.onStart();
        text.setText((text.getText()+"\n"+"Sóc a l'onStart"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        text.setText((text.getText()+"\n"+"Sóc a l'onResume"));

    }*/

    @Override
    public boolean onLongClick(View v) {
        Intent intent=new Intent(this, Activitat2.class);
        startActivity(intent);
        return true;
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId())
        {
            case R.id.btnNovaActivity:
                intent=new Intent(this, Activitat2.class);
                intent.putExtra("NOM",etEntrada.getText().toString());
                startActivity(intent);
                break;
            case R.id.btnPreguntaNom:
                intent=new Intent(this, Activitat2.class);
                startActivityForResult(intent, OBTENIR_NOM);
                break;
            case R.id.btnAccio:
                StringBuilder sbUri=new StringBuilder();
                sbUri.append("tel: ");
                sbUri.append(etEntrada.getText().toString());

                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sbUri.toString()));

                startActivity(intent);
                break;
        }

    }
}