package cat.duba.fitxersactivity;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.jar.Manifest;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button btnRecursos, btnInterna, btnExterna;
    TextView tvMissatges;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnRecursos= (Button) findViewById(R.id.btnRecursos);
        btnRecursos.setOnClickListener(this);
        btnInterna= (Button) findViewById(R.id.btnInterna);
        btnInterna.setOnClickListener(this);
        btnExterna= (Button) findViewById(R.id.btnExterna);
        btnExterna.setOnClickListener(this);
        tvMissatges= (TextView) findViewById(R.id.tvMissatge);
    }

    @Override
    public void onClick(View v) {
        switch ((v.getId()))
        {
            case R.id.btnRecursos:
                ObreFitxerRecursos();
                break;
            case R.id.btnInterna:
                ObreFitxerInterna();
                break;
            case R.id.btnExterna:
                break;
        }
    }

    private  void ObreFitxerRecursos(){
        String cadena="";
        String linia="";
        int nLinies=0;
        try{
            InputStream fitxerRaw=getResources().openRawResource(R.raw.provincia_municipis);
            BufferedReader brEntrada =new BufferedReader(new InputStreamReader(fitxerRaw));
            while((linia=brEntrada.readLine())!=null)
            {
                cadena+=linia+"\n";
            }
            fitxerRaw.close();

            tvMissatges.setText("Fitxer recuperat dels recursos\n"+
                    "======================================================\n\n"+
                    cadena);
        }catch(Exception ex)
        {
            Log.e("Fitxer de recursos: ", "L'error ha sigut al provar de llegir del fitxer de recursos: "+ex.getMessage());
        }

        //escrivim en memòria interna
        try{
            BufferedWriter bwSortida=new BufferedWriter(new OutputStreamWriter(openFileOutput("provincia_municipis", MODE_PRIVATE)));

            bwSortida.write(cadena);
            bwSortida.close();

            tvMissatges.setText("Fitxer recuperat dels recursos\n"+
                    "======================================================\n\n"+
                    cadena);
        }
        catch(Exception ex)
        {
            Log.e("Fitxer intern: ", "L'error ha sigut al provar de llegir del fitxer a memòria interna: "+ex.getMessage());
        }

        //escrivim en memòria externa
        if ((Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) && !(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY))) {
            try {
                File ruta_sd = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
                File f = new File(ruta_sd.getAbsolutePath(), "provincia_municipis");

                BufferedWriter bwSortida = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f)));

                bwSortida.write(cadena);

                bwSortida.close();
            } catch (Exception ex) {
                Log.e("Fitxer extern: ", "Error al provar d'escriure el fitxer a memòria externa - " +
                        ex.getMessage()); // ESCRIURE EN EL FITXER DE LOG (Log.d) fa debug, (Log.e) fa exception
            }
        }
    }
    private  void ObreFitxerInterna(){
        String cadena="";
        String linia="";
        try{
            BufferedReader brEntrada =new BufferedReader(new InputStreamReader(openFileInput("provincia_municipis")));

            while((linia=brEntrada.readLine())!=null)
            {
                cadena+=linia+"\n";
            }
            brEntrada.close();

            tvMissatges.setText("Fitxer recuperat de la memòria interna\n"+
                    "======================================================\n\n"+
                    cadena);
        }catch(Exception ex)
        {
            Log.e("Fitxer de recursos: ", "L'error ha sigut al provar de llegir del fitxer de recursos: "+ex.getMessage());
        }
    }


    //per obtenir permisos en api recents
  /*  private static  final int DEMANA_EMMAGATZEMAMENT_EXTERN=1;
    private static  String[] PERMISOS_EMMAGATZEMAMENT={
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static void verificaPermisos(Activity activitat)
    {
        int permis= ActivityCompat.checkSelfPermission(activitat, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if(permis!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activitat, PERMISOS_EMMAGATZEMAMENT, DEMANA_EMMAGATZEMAMENT_EXTERN);
        }
    }*/
}
