package cat.duba.adaptadorsandroidpractica;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class RecyclerViewAdapter extends AppCompatActivity {

    Intent pasarProducte;
    RecyclerView rvLlista;
    final static String PRODUCTE="Producte";
    Spinner spnCategoria, spnTipusRecycle;
    ArrayList<Producte> Bebidas;
    ArrayList<Producte> Condimentos;
    ArrayList<Producte> FrutasVerduras;
    ArrayList<Producte> Carnes;
    ArrayList<Producte> PescadoMarisco;
    ArrayList<Producte> Lacteos;
    ArrayList<Producte> Reposteria;
    ArrayList<Producte> GranosCereales;
    ArrayList<Producte> productes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_adapter);
        //Municipi producte
        //provincia categoria
        Bebidas = new ArrayList<>();
        Condimentos = new ArrayList<>();
        FrutasVerduras = new ArrayList<>();
        Carnes = new ArrayList<>();
        PescadoMarisco = new ArrayList<>();
        Lacteos = new ArrayList<>();
        Reposteria = new ArrayList<>();
        GranosCereales = new ArrayList<>();
        Intent intent=getIntent();
        productes = intent.getParcelableArrayListExtra(MainActivity.PRODUCTES);
        pasarProducte=new Intent(RecyclerViewAdapter.this, dades.class);

        rvLlista = (RecyclerView) findViewById(R.id.rcvLlista);
        rvLlista.setHasFixedSize(true);

        //region spnCategories
        spnTipusRecycle= (Spinner) findViewById(R.id.spnTipusRecycle);
        spnCategoria = (Spinner) findViewById(R.id.spnCategoriaList);
        spnCategoria.setAdapter(ArrayAdapter.createFromResource(this, R.array.categories, android.R.layout.simple_spinner_item));
        spnTipusRecycle.setAdapter(ArrayAdapter.createFromResource(this, R.array.recicle, android.R.layout.simple_spinner_item));

        productes = intent.getParcelableArrayListExtra(MainActivity.PRODUCTES);
        for (Producte p : productes) {
            switch (p.getCategoria()) {
                case "Bebidas":
                    Bebidas.add(p);
                    break;
                case "Condimentos":
                    Condimentos.add(p);
                    break;
                case "Frutas/Verduras":
                    FrutasVerduras.add(p);
                    break;
                case "Carnes":
                    Carnes.add(p);
                    break;
                case "Pescado/Marisco":
                    PescadoMarisco.add(p);
                    break;
                case "Lácteos":
                    Lacteos.add(p);
                    break;
                case "Repostería":
                    Reposteria.add(p);
                    break;
                case "Granos/Cereales":
                    GranosCereales.add(p);
                    break;
            }
        }
        spnCategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                AdaptadorReciclable adaptadorProductes = new AdaptadorReciclable(RecyclerViewAdapter.this, Bebidas);
                switch (spnCategoria.getSelectedItem().toString()) {
                    case "Bebidas":
                        adaptadorProductes = new AdaptadorReciclable(RecyclerViewAdapter.this, Bebidas);
                        break;
                    case "Condimentos":
                        adaptadorProductes = new AdaptadorReciclable(RecyclerViewAdapter.this, Condimentos);
                        break;
                    case "Frutas/Verduras":
                        adaptadorProductes = new AdaptadorReciclable(RecyclerViewAdapter.this, FrutasVerduras);
                        break;
                    case "Carnes":
                        adaptadorProductes = new AdaptadorReciclable(RecyclerViewAdapter.this, Carnes);
                        break;
                    case "Pescado/Marisco":
                        adaptadorProductes = new AdaptadorReciclable(RecyclerViewAdapter.this, PescadoMarisco);
                        break;
                    case "Lácteos":
                        adaptadorProductes = new AdaptadorReciclable(RecyclerViewAdapter.this, Lacteos);
                        break;
                    case "Repostería":
                        adaptadorProductes = new AdaptadorReciclable(RecyclerViewAdapter.this, Reposteria);
                        break;
                    case "Granos/Cereales":
                        adaptadorProductes = new AdaptadorReciclable(RecyclerViewAdapter.this, GranosCereales);
                        break;
                }
                adaptadorProductes.asignaEscoltadorDeClics(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ArrayList<Producte> productePassar = new ArrayList<Producte>();

                        productePassar.add((Producte)v.getTag() );
                        pasarProducte.putParcelableArrayListExtra(PRODUCTE, productePassar);
                        startActivity(pasarProducte);
                    }
                });
                rvLlista.setAdapter(adaptadorProductes);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
        spnTipusRecycle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (spnTipusRecycle.getSelectedItem().toString()) {
                    case "Vertical":
                        rvLlista.setLayoutManager(new LinearLayoutManager(RecyclerViewAdapter.this, LinearLayoutManager.VERTICAL, false));
                        break;
                    case "Horizontal":
                        rvLlista.setLayoutManager(new LinearLayoutManager(RecyclerViewAdapter.this, LinearLayoutManager.HORIZONTAL, false));
                        break;
                    case "Graella":
                        rvLlista.setLayoutManager(new GridLayoutManager(RecyclerViewAdapter.this, 2));
                        break;
                    case "Staggered":
                        rvLlista.setLayoutManager(new StaggeredGridLayoutManager(3, 1));
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //endregion


    }

    private class AdaptadorReciclable extends RecyclerView.Adapter<AdaptadorReciclable.ContenidorReciclable>{
        ArrayList<Producte> dades;
        Context context;
        View.OnClickListener escoltador;

        public AdaptadorReciclable (Context context, ArrayList<Producte> dades)
        {
            this.dades = dades;
            this.context = context;
        }
        @Override
        public ContenidorReciclable onCreateViewHolder(ViewGroup parent, int viewType) {
            View vista = getLayoutInflater().inflate(R.layout.producte, null);
            vista.setOnClickListener(escoltador);
            return new ContenidorReciclable(vista);
        }

        @Override
        public void onBindViewHolder(ContenidorReciclable holder, int position) {
            Producte m = dades.get(position);
            holder.EnllaçaProducte(m);
        }

        @Override
        public int getItemCount() {
            return dades.size();
        }


        public void asignaEscoltadorDeClics(View.OnClickListener escoltador) {

            this.escoltador = escoltador;
        }

        public class ContenidorReciclable extends RecyclerView.ViewHolder{
            TextView tvProducte, tvCategoria;
            View view;
            public ContenidorReciclable(View itemView) {
                super(itemView);
                tvProducte = (TextView) itemView.findViewById(R.id.tvNomProducte);
               tvCategoria = (TextView) itemView.findViewById(R.id.tvCategoria);
                view=itemView;
            }

            public void EnllaçaProducte(Producte p)
            {
                tvProducte.setText(p.getNomProducte());
                tvCategoria.setText(p.getCategoria());
                view.setTag(p);
            }
        }
    }
}