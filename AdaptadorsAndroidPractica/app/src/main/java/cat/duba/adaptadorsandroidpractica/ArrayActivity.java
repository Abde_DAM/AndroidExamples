package cat.duba.adaptadorsandroidpractica;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.EmptyStackException;

public class ArrayActivity extends AppCompatActivity {
    Spinner spnCategoria;
    final static String PRODUCTE="Producte";
    ListView lstLlista;
    AdapterView.OnItemSelectedListener escoltadorItemSelected;
    ArrayList<Producte> productes;
    ArrayList<Producte> Bebidas;
    ArrayList<Producte> Condimentos;
    ArrayList<Producte> FrutasVerduras;
    ArrayList<Producte> Carnes;
    ArrayList<Producte> PescadoMarisco;
    ArrayList<Producte> Lacteos;
    ArrayList<Producte> Reposteria;
    ArrayList<Producte> GranosCereales;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.array_adapter);
        final Intent pasarProducte=new Intent(this, dades.class);
        Bebidas=new ArrayList<>();
        Condimentos=new ArrayList<>();
        FrutasVerduras=new ArrayList<>();
        Carnes=new ArrayList<>();
        PescadoMarisco=new ArrayList<>();
        Lacteos=new ArrayList<>();
        Reposteria=new ArrayList<>();
        GranosCereales=new ArrayList<>();
        spnCategoria= (Spinner) findViewById(R.id.spnCategoria);
        lstLlista= (ListView) findViewById(R.id.lstLlista);
        spnCategoria.setAdapter(ArrayAdapter.createFromResource(this, R.array.categories, android.R.layout.simple_spinner_item));
        Intent intent=getIntent();
        productes=intent.getParcelableArrayListExtra(MainActivity.PRODUCTES);
        for (Producte p : productes) {
            switch (p.getCategoria()) {
                case "Bebidas":
                    Bebidas.add(p);
                    break;
                case "Condimentos":
                    Condimentos.add(p);
                    break;
                case "Frutas/Verduras":
                    FrutasVerduras.add(p);
                    break;
                case "Carnes":
                    Carnes.add(p);
                    break;
                case "Pescado/Marisco":
                    PescadoMarisco.add(p);
                    break;
                case "Lácteos":
                    Lacteos.add(p);
                    break;
                case "Repostería":
                    Reposteria.add(p);
                    break;
                case "Granos/Cereales":
                    GranosCereales.add(p);
                    break;
            }
        }
        spnCategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                switch (spnCategoria.getSelectedItem().toString()) {
                    case "Bebidas":
                        lstLlista.setAdapter(new ArrayAdapter(ArrayActivity.this, android.R.layout.simple_list_item_1, Bebidas.toArray()));
                        break;
                    case "Condimentos":
                        lstLlista.setAdapter(new ArrayAdapter(ArrayActivity.this, android.R.layout.simple_list_item_1, Condimentos.toArray()));
                        break;
                    case "Frutas/Verduras":
                        lstLlista.setAdapter(new ArrayAdapter(ArrayActivity.this, android.R.layout.simple_list_item_1, FrutasVerduras.toArray()));
                        break;
                    case "Carnes":
                        lstLlista.setAdapter(new ArrayAdapter(ArrayActivity.this, android.R.layout.simple_list_item_1, Carnes.toArray()));
                        break;
                    case "Pescado/Marisco":
                        lstLlista.setAdapter(new ArrayAdapter(ArrayActivity.this, android.R.layout.simple_list_item_1, PescadoMarisco.toArray()));
                        break;
                    case "Lácteos":
                        lstLlista.setAdapter(new ArrayAdapter(ArrayActivity.this, android.R.layout.simple_list_item_1, Lacteos.toArray()));
                        break;
                    case "Repostería":
                        lstLlista.setAdapter(new ArrayAdapter(ArrayActivity.this, android.R.layout.simple_list_item_1, Reposteria.toArray()));
                        break;
                    case "Granos/Cereales":
                        lstLlista.setAdapter(new ArrayAdapter(ArrayActivity.this, android.R.layout.simple_list_item_1, GranosCereales.toArray()));
                        break;
                    default: throw new EmptyStackException();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
        lstLlista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<Producte> productePassar = new ArrayList<Producte>();
                productePassar.add((Producte) lstLlista.getItemAtPosition(position));
                pasarProducte.putParcelableArrayListExtra(PRODUCTE, productePassar);
                startActivity(pasarProducte);
            }
        });
    }
}
