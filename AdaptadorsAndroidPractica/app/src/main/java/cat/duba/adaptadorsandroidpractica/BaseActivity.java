package cat.duba.adaptadorsandroidpractica;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class BaseActivity extends AppCompatActivity {

    Spinner spnCategoria;
    final static String PRODUCTES="Productes";
    ListView lstLlista;
    AdapterView.OnItemSelectedListener escoltadorItemSelected;
    ArrayList<Producte> productes;
    ArrayList<Producte> Bebidas;
    ArrayList<Producte> Condimentos;
    ArrayList<Producte> FrutasVerduras;
    ArrayList<Producte> Carnes;
    ArrayList<Producte> PescadoMarisco;
    ArrayList<Producte> Lacteos;
    ArrayList<Producte> Reposteria;
    ArrayList<Producte> GranosCereales;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_adapter);
        Bebidas = new ArrayList<>();
        Condimentos = new ArrayList<>();
        FrutasVerduras = new ArrayList<>();
        Carnes = new ArrayList<>();
        PescadoMarisco = new ArrayList<>();
        Lacteos = new ArrayList<>();
        Reposteria = new ArrayList<>();
        GranosCereales = new ArrayList<>();
        spnCategoria = (Spinner) findViewById(R.id.spnCategoria);
        lstLlista = (ListView) findViewById(R.id.lstLlista);
        spnCategoria.setAdapter(ArrayAdapter.createFromResource(this, R.array.categories, android.R.layout.simple_spinner_item));
        Intent intent = getIntent();
        productes = intent.getParcelableArrayListExtra(MainActivity.PRODUCTES);
        for (Producte p : productes) {
            switch (p.getCategoria()) {
                case "Bebidas":
                    Bebidas.add(p);
                    break;
                case "Condimentos":
                    Condimentos.add(p);
                    break;
                case "Frutas/Verduras":
                    FrutasVerduras.add(p);
                    break;
                case "Carnes":
                    Carnes.add(p);
                    break;
                case "Pescado/Marisco":
                    PescadoMarisco.add(p);
                    break;
                case "Lácteos":
                    Lacteos.add(p);
                    break;
                case "Repostería":
                    Reposteria.add(p);
                    break;
                case "Granos/Cereales":
                    GranosCereales.add(p);
                    break;
            }
        }
        spnCategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                switch (spnCategoria.getSelectedItem().toString()) {
                    case "Bebidas":
                        lstLlista.setAdapter(new AdaptadorProductes(BaseActivity.this, Bebidas));
                        break;
                    case "Condimentos":
                        lstLlista.setAdapter(new AdaptadorProductes(BaseActivity.this, Condimentos));
                        break;
                    case "Frutas/Verduras":
                        lstLlista.setAdapter(new AdaptadorProductes(BaseActivity.this,FrutasVerduras));
                        break;
                    case "Carnes":
                        lstLlista.setAdapter(new AdaptadorProductes(BaseActivity.this, Carnes));
                        break;
                    case "Pescado/Marisco":
                        lstLlista.setAdapter(new AdaptadorProductes(BaseActivity.this, PescadoMarisco));
                        break;
                    case "Lácteos":
                        lstLlista.setAdapter(new AdaptadorProductes(BaseActivity.this,  Lacteos));
                        break;
                    case "Repostería":
                        lstLlista.setAdapter(new AdaptadorProductes(BaseActivity.this, Reposteria));
                        break;
                    case "Granos/Cereales":
                        lstLlista.setAdapter(new AdaptadorProductes(BaseActivity.this, GranosCereales));
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
    }
    private class AdaptadorProductes extends BaseAdapter {
        Context contexte;
        ArrayList<Producte> producte;

        public AdaptadorProductes (Context contexte, ArrayList<Producte> dades){
            this.contexte = contexte;
            this.producte = dades;
        }

        @Override
        public int getCount() {
            return producte.size();
        }

        @Override
        public Object getItem(int position) {
            return producte.get(position);
        }

        @Override
        public long getItemId(int position) {
            return producte.get(position).getCodiProducte();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) { //Retorna la vista amb les dades donades. EX: quan es fa scroll i es mostren noves dades.
            Contenidor contenidor;
            if(convertView==null) {
                LayoutInflater inflador = getLayoutInflater();
                convertView = inflador.inflate(R.layout.producte, null);
                //convertView = inflador.inflate(R.layout.producte,parent, false);

                contenidor = new Contenidor();
                contenidor.tvCodiProducte= (TextView) convertView.findViewById(R.id.tvCodiProducte);
                contenidor.tvNomProducte= (TextView) convertView.findViewById(R.id.tvNomProducte);
                contenidor.tvEstoc= (TextView) convertView.findViewById(R.id.tvEstoc);
                contenidor.tvCategoria= (TextView) convertView.findViewById(R.id.tvCategoria);
                contenidor.tvPreu= (TextView) convertView.findViewById(R.id.tvPreu);
                convertView.setTag(contenidor);

            }
            else {

                contenidor = (Contenidor) convertView.getTag();
            }

            contenidor.tvCodiProducte.setText(Integer.toString(((Producte) getItem(position)).getCodiProducte()));
            contenidor.tvNomProducte.setText(((Producte) getItem(position)).getNomProducte());
            contenidor.tvEstoc.setText(Integer.toString(((Producte) getItem(position)).getEstoc()));
            contenidor.tvCategoria.setText(((Producte) getItem(position)).getCategoria());
            contenidor.tvPreu.setText(Double.toString(((Producte) getItem(position)).getPreu()));


            return convertView;
        }

        private class Contenidor{
            TextView tvCodiProducte;
            TextView tvNomProducte;
            TextView tvCategoria;
            TextView tvPreu;
            TextView tvEstoc;
        }
    }
}