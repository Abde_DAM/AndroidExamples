package cat.duba.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by abde on 11/12/15.
 */
public class Empleat implements Parcelable {
    private String nom="";
    private  int sou=1000;
    private  boolean capDepartament=false;
    private ArrayList<Empleat> subordinats;

    public Empleat()
    {
        setSubordinats(new ArrayList<Empleat>());
    }

    public Empleat(String nom, int sou, boolean capDepartament)
    {
        this.setNom(nom);
        this.setSou(sou);
        this.setCapDepartament(capDepartament);
        setSubordinats(new ArrayList<Empleat>());
    }

    protected Empleat(Parcel in) {
        nom = in.readString();
        sou = in.readInt();
        capDepartament = in.readByte() != 0;
        subordinats = in.createTypedArrayList(Empleat.CREATOR);
    }

    public static final Creator<Empleat> CREATOR = new Creator<Empleat>() {
        @Override
        public Empleat createFromParcel(Parcel in) {
            return new Empleat(in);
        }

        @Override
        public Empleat[] newArray(int size) {
            return new Empleat[size];
        }
    };

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getSou() {
        return sou;
    }

    public void setSou(int sou) {
        this.sou = sou;
    }

    public boolean isCapDepartament() {
        return capDepartament;
    }

    public void setCapDepartament(boolean capDepartament) {
        this.capDepartament = capDepartament;
    }

    public ArrayList<Empleat> getSubordinats() {
        return subordinats;
    }

    public void setSubordinats(ArrayList<Empleat> subordinats) {
        this.subordinats = subordinats;
    }

    @Override
    public String toString()
    {
        return  nom;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nom);
        dest.writeInt(sou);
        dest.writeBooleanArray(new boolean[]{isCapDepartament()});
        dest.writeTypedList(subordinats);
    }
}
