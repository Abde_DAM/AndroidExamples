package cat.duba.puzzle;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {
    private SeekBar sbFiles, sbColumnes;
    private EditText etNom;
    private TextView tvRecords;
    private final String FILENAME = "records";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sbFiles= (SeekBar) findViewById(R.id.sbFiles);
        tvRecords= (TextView) findViewById(R.id.tvRecords);
        sbColumnes= (SeekBar) findViewById(R.id.sbColumnes);
        etNom= (EditText) findViewById(R.id.etNom);
    }
    public void OnClick(View v){
        Intent intent;

        intent=new Intent(this, PuzzleActivity.class);
        intent.putExtra("FILES", sbFiles.getProgress()+3);
        intent.putExtra("COLUMNES", sbColumnes.getProgress()+3);
        intent.putExtra("NOM", etNom.getText().toString());
    startActivity(intent);

    }
    public void Click(View v){
            String cadena="";
            String linia="";
            try{
                BufferedReader brEntrada =new BufferedReader(new InputStreamReader(openFileInput(FILENAME)));

                while((linia=brEntrada.readLine())!=null)
                {
                    cadena+=linia+"\n";
                }
                brEntrada.close();
                tvRecords.setText(cadena.replace(';', ' '));
            }
            catch(Exception ex)
            {
                Log.e("Fitxer intern: ", "L'error ha sigut al provar de llegir del fitxer a memòria interna: " + ex.getMessage());
            }
    }
}
