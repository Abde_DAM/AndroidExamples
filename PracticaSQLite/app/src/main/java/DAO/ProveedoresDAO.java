package DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.sergi.practicasqlite.AppConstants;
import com.example.sergi.practicasqlite.NeptunoSQLHelper;

import java.util.ArrayList;
import java.util.List;

import Classes.Proveedores;

/**
 * Created by Sergi on 15/03/2016.
 */
public class ProveedoresDAO
{
    private SQLiteDatabase db;
    private SQLiteOpenHelper dbHelper;
    private String[] columnes = {AppConstants.COLUMNA_ID, AppConstants.COLUMNA_ID_PROVEEDOR, AppConstants.COLUMNA_NOMBRE_COMPANYIA, AppConstants.COLUMNA_NOMBRE_CONTACTO, AppConstants.COLUMNA_CARGO_CONTACTO
            , AppConstants.COLUMNA_DIRECCION_PROVEEDOR, AppConstants.COLUMNA_CIUDAD_PROVEEDOR, AppConstants.COLUMNA_REGION_PROVEEDOR, AppConstants.COLUMNA_POSTAL_PROVEEDOR, AppConstants.COLUMNA_PAIS_PROVEEDOR,
            AppConstants.COLUMNA_TELEFONO_PROVEEDOR, AppConstants.COLUMNA_FAX_PROVEEDOR, AppConstants.COLUMNA_PAGINA_PROVEEDOR};

    public ProveedoresDAO(Context context)
    {
        dbHelper = new NeptunoSQLHelper(context);
    }

    public void obre()
    {
        db = dbHelper.getWritableDatabase();
    }

    public void tanca()
    {
        db.close();
    }

    public Proveedores cursorAProveedor(Cursor cursor)
    {
        Proveedores proveedor = new Proveedores();
        proveedor.setCodigoProveedores(cursor.getString(1));
        proveedor.setNombreCompañia(cursor.getString(2));
        proveedor.setNombreContacto(cursor.getString(3));
        proveedor.setCargo(cursor.getString(4));
        proveedor.setDireccion(cursor.getString(5));
        proveedor.setCiudad(cursor.getString(6));
        proveedor.setRegion(cursor.getString(7));
        proveedor.setCodigoPostal(cursor.getString(8));
        proveedor.setPais(cursor.getString(9));
        proveedor.setTelefono(cursor.getString(10));
        proveedor.setFax(cursor.getString(11));
        proveedor.setWeb(cursor.getString(12));

        return proveedor;
    }

    public Proveedores crearProveedor (String codigoProveedores, String nombreCompañia, String nombreContacto, String cargo,
                                      String direccion, String ciudad, String region, String codigoPostal, String pais,
                                      String telefono, String fax, String web)
    {
        ContentValues valors = new ContentValues();
        valors.put(AppConstants.COLUMNA_ID_PROVEEDOR, codigoProveedores);
        valors.put(AppConstants.COLUMNA_NOMBRE_COMPANYIA, nombreCompañia);
        valors.put(AppConstants.COLUMNA_NOMBRE_CONTACTO, nombreContacto);
        valors.put(AppConstants.COLUMNA_CARGO_CONTACTO, cargo);
        valors.put(AppConstants.COLUMNA_DIRECCION_PROVEEDOR, direccion);
        valors.put(AppConstants.COLUMNA_CIUDAD_PROVEEDOR, ciudad);
        valors.put(AppConstants.COLUMNA_REGION_PROVEEDOR, region);
        valors.put(AppConstants.COLUMNA_POSTAL_PROVEEDOR, codigoPostal);
        valors.put(AppConstants.COLUMNA_PAIS_PROVEEDOR, pais);
        valors.put(AppConstants.COLUMNA_TELEFONO_PROVEEDOR, telefono);
        valors.put(AppConstants.COLUMNA_FAX_PROVEEDOR, fax);
        valors.put(AppConstants.COLUMNA_PAGINA_PROVEEDOR, web);

        long idInsercio = db.insert(AppConstants.TAULA_PROVEEDORES, null, valors);
        if(idInsercio == -1)
            return null;
        else
        {
            Cursor cursor = db.query(AppConstants.TAULA_PROVEEDORES, columnes, null,
                    null, null, null, null);

            cursor.moveToFirst();

            Proveedores proveedores = cursorAProveedor(cursor);
            cursor.close();
            return proveedores;
        }
    }

    /*public boolean eliminarProveedores (proveedores proveedores)
    {
        int nEsborrats = db.delete(AppConstants.TAULA_PROVEEDORES, AppConstants.COLUMNA_ID_PROVEEDOR + " = '" + provincia.getCodiProvincia() +"'", null);
        return nEsborrats >0;
    }*/

    public List<Proveedores> obteProveedores()
    {
        List<Proveedores> proveedores = new ArrayList<>();

        Cursor cursor = db.query(AppConstants.TAULA_PROVEEDORES, columnes, null, null, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast())
        {
            Proveedores proveedor = cursorAProveedor(cursor);
            proveedores.add(proveedor);
            cursor.moveToNext();
        }

        return proveedores;
    }
}


