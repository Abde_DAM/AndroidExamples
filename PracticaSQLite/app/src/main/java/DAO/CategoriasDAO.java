package DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.sergi.practicasqlite.AppConstants;
import com.example.sergi.practicasqlite.NeptunoSQLHelper;

import java.util.ArrayList;
import java.util.List;

import Classes.Categorias;
import Classes.Proveedores;

/**
 * Created by Sergi on 15/03/2016.
 */
public class CategoriasDAO
{
    private SQLiteDatabase db;
    private SQLiteOpenHelper dbHelper;
    private String[] columnes = {AppConstants.COLUMNA_ID, AppConstants.COLUMNA_ID_CATEGORIA, AppConstants.COLUMNA_NOMBRE_CATEGORIA, AppConstants.COLUMNA_DESC_CATEGORIA};

    public CategoriasDAO(Context context)
    {
        dbHelper = new NeptunoSQLHelper(context);
    }

    public void obre()
    {
        db = dbHelper.getWritableDatabase();
    }

    public void tanca()
    {
        db.close();
    }

    public Categorias cursorACategorias(Cursor cursor)
    {
        Categorias categoria = new Categorias();
        categoria.setIdCategoria(cursor.getString(1));
        categoria.setNombreCategoria(cursor.getString(2));
        categoria.setDescripcionCategoria(cursor.getString(3));

        return categoria;
    }

    public Categorias crearCategoria (String idCat, String nombreCat, String desc)
    {
        ContentValues valors = new ContentValues();
        valors.put(AppConstants.COLUMNA_ID_CATEGORIA, idCat);
        valors.put(AppConstants.COLUMNA_NOMBRE_CATEGORIA, nombreCat);
        valors.put(AppConstants.COLUMNA_DESC_CATEGORIA, desc);

        long idInsercio = db.insert(AppConstants.TAULA_CATEGORIAS, null, valors);
        if(idInsercio == -1)
            return null;
        else
        {
            Cursor cursor = db.query(AppConstants.TAULA_CATEGORIAS, columnes, null,
                    null, null, null, null);

            cursor.moveToFirst();

            Categorias categoria = cursorACategorias(cursor);
            cursor.close();
            return categoria;
        }
    }

    /*public boolean eliminarCategorias (proveedores proveedores)
    {
        int nEsborrats = db.delete(AppConstants.TAULA_PROVEEDORES, AppConstants.COLUMNA_ID_PROVEEDOR + " = '" + provincia.getCodiProvincia() +"'", null);
        return nEsborrats >0;
    }*/

    public List<Categorias> obteCategorias()
    {
        List<Categorias> categorias = new ArrayList<>();

        Cursor cursor = db.query(AppConstants.TAULA_CATEGORIAS, columnes, null, null, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast())
        {
            Categorias cat = cursorACategorias(cursor);
            categorias.add(cat);
            cursor.moveToNext();
        }

        return categorias;
    }
}


