package DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.sergi.practicasqlite.AppConstants;
import com.example.sergi.practicasqlite.NeptunoSQLHelper;

import java.util.ArrayList;
import java.util.List;

import Classes.Productos;
import Classes.Proveedores;

/**
 * Created by Sergi on 15/03/2016.
 */
public class ProductosDAO
{
    private SQLiteDatabase db;
    private SQLiteOpenHelper dbHelper;
    private String[] columnes = {AppConstants.COLUMNA_ID, AppConstants.COLUMNA_ID_PRODUCTO, AppConstants.COLUMNA_NOMBRE_PRODUCTO,
            AppConstants.COLUMNA_ID_PROVEEDOR, AppConstants.COLUMNA_ID_CATEGORIA, AppConstants.COLUMNA_CANTIDAD_UNIDAD,
            AppConstants.COLUMNA_PRECIO_UNIDAD, AppConstants.COLUMNA_UNI_EXIST, AppConstants.COLUMNA_UNI_PEDIDO,
            AppConstants.COLUMNA_NIVEL_NUEVO_PEDIDO, AppConstants.COLUMNA_SUSPENDIDO};

    public ProductosDAO(Context context)
    {
        dbHelper = new NeptunoSQLHelper(context);
    }

    public void obre()
    {
        db = dbHelper.getWritableDatabase();
    }

    public void tanca()
    {
        db.close();
    }

    public Productos cursorAProducto(Cursor cursor)
    {
        Productos producto = new Productos();
        producto.setIdProducto(cursor.getString(1));
        producto.setNombreProducto(cursor.getString(2));
        producto.setIdProveedor(cursor.getString(3));
        producto.setIdCategoria(cursor.getString(4));
        producto.setCantidadUnidad(cursor.getString(5));
        producto.setPrecioUnidad(cursor.getString(6));
        producto.setUniExist(cursor.getString(7));
        producto.setUniPedido(cursor.getString(8));
        producto.setNivelNuevoPedido(cursor.getString(9));
        producto.setSuspendido(cursor.getString(10));

        return producto;
    }

    public Productos crearProductos (String idProducto, String nombreProducto, String idProveedor, String idCategoria,
                                     String cantidadUnidad, String precioUnidad, String uniExist, String uniPedido,
                                     String nivelNuevoPedido, String suspendido)
    {
        ContentValues valors = new ContentValues();
        valors.put(AppConstants.COLUMNA_ID_PRODUCTO, idProducto);
        valors.put(AppConstants.COLUMNA_NOMBRE_PRODUCTO, nombreProducto);
        valors.put(AppConstants.COLUMNA_ID_PROVEEDOR, idProveedor);
        valors.put(AppConstants.COLUMNA_ID_CATEGORIA, idCategoria);
        valors.put(AppConstants.COLUMNA_CANTIDAD_UNIDAD, cantidadUnidad);
        valors.put(AppConstants.COLUMNA_PRECIO_UNIDAD, precioUnidad);
        valors.put(AppConstants.COLUMNA_UNI_EXIST, uniExist);
        valors.put(AppConstants.COLUMNA_UNI_PEDIDO, uniPedido);
        valors.put(AppConstants.COLUMNA_NIVEL_NUEVO_PEDIDO, nivelNuevoPedido);
        valors.put(AppConstants.COLUMNA_SUSPENDIDO, suspendido);

        long idInsercio = db.insert(AppConstants.TAULA_PRODUCTOS, null, valors);
        if(idInsercio == -1)
            return null;
        else
        {
            Cursor cursor = db.query(AppConstants.TAULA_PRODUCTOS, columnes, null,
                    null, null, null, null);

            cursor.moveToFirst();

            Productos productos = cursorAProducto(cursor);
            cursor.close();
            return productos;
        }
    }

    /*public boolean eliminarProductos (proveedores proveedores)
    {
        int nEsborrats = db.delete(AppConstants.TAULA_PROVEEDORES, AppConstants.COLUMNA_ID_PROVEEDOR + " = '" + provincia.getCodiProvincia() +"'", null);
        return nEsborrats >0;
    }*/

    public List<Productos> obteProductos()
    {
        List<Productos> productos = new ArrayList<>();

        Cursor cursor = db.query(AppConstants.TAULA_PRODUCTOS, columnes, null, null, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast())
        {
            Productos p = cursorAProducto(cursor);
            productos.add(p);
            cursor.moveToNext();
        }

        return productos;
    }
}


