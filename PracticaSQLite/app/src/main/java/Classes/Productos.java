package Classes;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sergi on 14/03/2016.
 */

public class Productos implements Parcelable{
    private String idProducto;
    private String nombreProducto;
    private String idProveedor;
    private String idCategoria;
    private String cantidadUnidad;
    private String precioUnidad;
    private String uniExist;
    private String uniPedido;
    private String nivelNuevoPedido;
    private String suspendido;

    public Productos(){}

    public Productos(String idProducto, String nombreProducto, String idProveedor, String idCategoria,
                     String cantidadUnidad, String precioUnidad, String uniExist, String uniPedido, String nivelNuevoPedido,
                     String suspendido)
    {
        this.idProducto = idProducto;
        this.nombreProducto = nombreProducto;
        this.idProveedor = idProveedor;
        this.idCategoria = idCategoria;
        this.cantidadUnidad = cantidadUnidad;
        this.precioUnidad = precioUnidad;
        this.uniExist = uniExist;
        this.uniPedido = uniPedido;
        this.nivelNuevoPedido = nivelNuevoPedido;
        this.suspendido = suspendido;
    }

    protected Productos(Parcel in) {
        this.idProducto = in.readString();
        this.nombreProducto = in.readString();
        this.idProveedor = in.readString();
        this.idCategoria = in.readString();
        this.cantidadUnidad = in.readString();
        this.precioUnidad = in.readString();
        this.uniExist = in.readString();
        this.uniPedido = in.readString();
        this.nivelNuevoPedido = in.readString();
        this.suspendido = in.readString();
    }

    //region PROPERTIES

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(String idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCantidadUnidad() {
        return cantidadUnidad;
    }

    public void setCantidadUnidad(String cantidadUnidad) {
        this.cantidadUnidad = cantidadUnidad;
    }

    public String getPrecioUnidad() {
        return precioUnidad;
    }

    public void setPrecioUnidad(String precioUnidad) {
        this.precioUnidad = precioUnidad;
    }

    public String getUniExist() {
        return uniExist;
    }

    public void setUniExist(String uniExist) {
        this.uniExist = uniExist;
    }

    public String getUniPedido() {
        return uniPedido;
    }

    public void setUniPedido(String uniPedido) {
        this.uniPedido = uniPedido;
    }

    public String getNivelNuevoPedido() {
        return nivelNuevoPedido;
    }

    public void setNivelNuevoPedido(String nivelNuevoPedido) {
        this.nivelNuevoPedido = nivelNuevoPedido;
    }

    public String getSuspendido() {
        return suspendido;
    }

    public void setSuspendido(String suspendido) {
        this.suspendido = suspendido;
    }

    //endregion

    @Override
    public String toString(){
        return nombreProducto;
    }


    //region PARCELABLE
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idProducto);
        dest.writeString(nombreProducto);
        dest.writeString(idProveedor);
        dest.writeString(idCategoria);
        dest.writeString(cantidadUnidad);
        dest.writeString(precioUnidad);
        dest.writeString(uniExist);
        dest.writeString(uniPedido);
        dest.writeString(nivelNuevoPedido);
        dest.writeString(suspendido);
    }
    public void readFromParcel(Parcel dades){
        idProducto = dades.readString();
        nombreProducto = dades.readString();
        idProveedor = dades.readString();
        idCategoria = dades.readString();
        cantidadUnidad = dades.readString();
        precioUnidad = dades.readString();
        uniExist = dades.readString();
        uniPedido = dades.readString();
        nivelNuevoPedido = dades.readString();
        suspendido = dades.readString();
    }

    public static final Creator<Productos> CREATOR = new Creator<Productos>() {
        @Override
        public Productos createFromParcel(Parcel in) {
            return new Productos(in);
        }

        @Override
        public Productos[] newArray(int size) {
            return new Productos[size];
        }
    };
    //endregion
}
