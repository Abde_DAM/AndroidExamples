package com.example.sergi.practicasqlite;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class CategoryActivity extends AppCompatActivity {
    NeptunoSQLHelper dbHelper;
    SQLiteDatabase db;
    ListView lstCateg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        lstCateg = (ListView) findViewById(R.id.lst_categorias);
        dbHelper = new NeptunoSQLHelper(this);
        try {
            db = dbHelper.getWritableDatabase();

            cargarCategoriasEnListview();
        }
        catch(Exception e){}
    }

    @Override
    protected void onStop()
    {
        try {
            if (db != null) {
                db.close();
            }
        }
        catch(Exception e){}
        super.onStop();
    }

    private void cargarCategoriasEnListview()
    {
        final Cursor cursorListView = db.query(AppConstants.TAULA_CATEGORIAS, null, null, null, null, null, AppConstants.COLUMNA_ID);

        SimpleCursorAdapter adapt = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, cursorListView, new String[]
                {AppConstants.COLUMNA_NOMBRE_CATEGORIA}, new int[]{android.R.id.text1}, 0);
        lstCateg.setAdapter(adapt);
        lstCateg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(view.getContext(), CategoryDetail.class);
            }
        });
    }
}
