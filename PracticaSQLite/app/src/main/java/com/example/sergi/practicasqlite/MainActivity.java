package com.example.sergi.practicasqlite;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import Classes.Categorias;
import Classes.Productos;
import Classes.Proveedores;
import DAO.CategoriasDAO;
import DAO.ProductosDAO;
import DAO.ProveedoresDAO;


public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    Button btnCategories;
    Button btnProveidors;
    Button btnProductes;
    Button btnComandes;
    NeptunoSQLHelper dbHelper;
    SQLiteDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnCategories = (Button) findViewById(R.id.btnCategories);
        btnComandes = (Button) findViewById(R.id.btnComandes);
        btnProductes = (Button) findViewById(R.id.btnProductes);
        btnProveidors = (Button) findViewById(R.id.btnProveidors);
        btnCategories.setOnClickListener(this);
        btnComandes.setOnClickListener(this);
        btnProductes.setOnClickListener(this);
        btnProveidors.setOnClickListener(this);
        dbHelper = new NeptunoSQLHelper(this);
        try {
            db = dbHelper.getWritableDatabase();
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.mnuRestaura:
                try {
                    db = dbHelper.getWritableDatabase();
                    /*db.rawQuery(AppConstants.DELETE_PROVEEDORES, null);
                    db.rawQuery(AppConstants.DELETE_PRODUCTOS, null);
                    db.rawQuery(AppConstants.DELETE_CATEGORIAS, null);*/
                    getApplicationContext().deleteDatabase(AppConstants.NOM_DB);

                } catch (Exception e) {
                }

                this.ompleProveedores();
                this.ompleCategorias();
                this.ompleProductos();
                db.close();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId()){
            case R.id.btnCategories:
                startActivity(new Intent(this, CategoryActivity.class));
                break;

            case R.id.btnComandes:
                break;

            case R.id.btnProductes:
                startActivity(new Intent(this, ProductosMain.class));
                break;

            case R.id.btnProveidors:
                startActivity(new Intent(this, ProveidorsMain.class));
                break;
        }
    }

    private void ompleProveedores()
    {
        ArrayList<Proveedores> llistaProveedores = new ArrayList<>();
        llistaProveedores = Fitxers.obtenerProveedores(this);

        ProveedoresDAO provDAO = new ProveedoresDAO(this);
        try
        {
            provDAO.obre();

            for(Proveedores p:llistaProveedores)
            {
                provDAO.crearProveedor(p.getCodigoProveedores(), p.getNombreCompañia(), p.getNombreContacto(), p.getCargo(),
                        p.getDireccion(), p.getCiudad(), p.getRegion(), p.getCodigoPostal(), p.getPais(),
                        p.getTelefono(), p.getFax(), p.getWeb());
            }
        }
        catch (SQLiteException e)
        {
            Log.e("SQLERROR", "ProvinciaDAO - " + e.getMessage());
        }
        finally
        {
            provDAO.tanca();
        }
    }

    private void ompleProductos()
    {
        ArrayList<Productos> llistaProductos = new ArrayList<>();
        llistaProductos = Fitxers.obtenerProductos(this);

        ProductosDAO productDAO = new ProductosDAO(this);
        try
        {
            productDAO.obre();

            for(Productos p:llistaProductos)
            {
                productDAO.crearProductos(p.getIdProducto(), p.getNombreProducto(), p.getIdProveedor(), p.getIdCategoria(),
                        p.getCantidadUnidad(), p.getPrecioUnidad(), p.getUniExist(), p.getUniPedido(), p.getNivelNuevoPedido(),
                        p.getSuspendido());
            }
        }
        catch (SQLiteException e)
        {
            Log.e("SQLERROR", "ProductDAO - " + e.getMessage());
        }
        finally
        {
            productDAO.tanca();
        }
    }

    private void ompleCategorias()
    {
        ArrayList<Categorias> llistaCategorias = new ArrayList<>();
        llistaCategorias = Fitxers.obtenerCategorias(this);

        CategoriasDAO catDAO = new CategoriasDAO(this);
        try
        {
            catDAO.obre();

            for(Categorias c:llistaCategorias)
            {
                catDAO.crearCategoria(c.getIdCategoria(), c.getNombreCategoria(), c.getDescripcionCategoria());
            }
        }
        catch (SQLiteException e)
        {
            Log.e("SQLERROR", "ProvinciaDAO - " + e.getMessage());
        }
        finally
        {
            catDAO.tanca();
        }
    }
}
