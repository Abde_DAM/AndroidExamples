package com.example.sergi.practicasqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class ProductosMain extends AppCompatActivity {
    NeptunoSQLHelper dbHelper;
    SQLiteDatabase db;
    ListView lstProd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos_main);
        lstProd = (ListView) findViewById(R.id.lst_productos);
        dbHelper = new NeptunoSQLHelper(this);
        try {
            db = dbHelper.getWritableDatabase();

            cargarProductosEnListview();
        } catch (Exception e) {
        }
    }

    @Override
    protected void onStop() {
        try {
            if (db != null) {
                db.close();
            }
        } catch (Exception e) {
        }
        super.onStop();
    }

    private void cargarProductosEnListview() {
        Cursor cursorListView = db.query(AppConstants.TAULA_PRODUCTOS, null, null, null, null, null, AppConstants.COLUMNA_ID);

        SimpleCursorAdapter adapt = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, cursorListView, new String[]
                {AppConstants.COLUMNA_NOMBRE_PRODUCTO}, new int[]{android.R.id.text1}, 0);
        lstProd.setAdapter(adapt);
    }
}