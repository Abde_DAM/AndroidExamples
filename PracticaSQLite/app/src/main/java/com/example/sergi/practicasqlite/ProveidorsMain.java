package com.example.sergi.practicasqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.util.ArrayList;

import Classes.Proveedores;
import DAO.ProveedoresDAO;

public class ProveidorsMain extends AppCompatActivity {
    NeptunoSQLHelper dbHelper;
    SQLiteDatabase db;
    ListView lvProv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proveidors_main);
        lvProv = (ListView) findViewById(R.id.lv_proveeidores);
        dbHelper = new NeptunoSQLHelper(this);
        try {
            db = dbHelper.getWritableDatabase();

            cargaProveedoresEnListview();
        }
        catch(Exception e){}
    }

    @Override
    protected void onStop()
    {
        try {
            if (db != null) {
                db.close();
            }
        }
        catch(Exception e){}
        super.onStop();
    }

    private void cargaProveedoresEnListview()
    {
        Cursor cursorListView = db.query(AppConstants.TAULA_PROVEEDORES, null, null, null, null, null, AppConstants.COLUMNA_ID);

        SimpleCursorAdapter adapt = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, cursorListView, new String[]
                {AppConstants.COLUMNA_NOMBRE_CONTACTO}, new int[]{android.R.id.text1}, 0);
        lvProv.setAdapter(adapt);

    }
}
