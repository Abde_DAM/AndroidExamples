package cat.duba.menu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final Double PREUMENU=12.0, PLATDIA=7.0;
    private RadioButton[] rbt=new RadioButton[9];
    private Button btnReiniciar, btnFactura;
    private Double sumaPreu, total=0.0;
    private String factura;
    private CheckBox chkMacarrons, chkCarnForn, chkPuro, chkCopa, chkTurro, chkCaramel;
    private TextView txtvFactura;
    private EditText txtNom;
    private SeekBar skbA,skbR,skbG,skbB;
    private LinearLayout llColors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rbt[0]= (RadioButton) findViewById(R.id.rbtMacarrons);
        rbt[1]= (RadioButton) findViewById(R.id.rbtCigrons);
        rbt[2]= (RadioButton) findViewById(R.id.rbtSopaPeix);
        rbt[3]= (RadioButton) findViewById(R.id.rbtCarnForn);
        rbt[4]= (RadioButton) findViewById(R.id.rbtPollasteArros);
        rbt[5]= (RadioButton) findViewById(R.id.rbtLasanya);
        rbt[6]= (RadioButton) findViewById(R.id.rbtGelat);
        rbt[7]= (RadioButton) findViewById(R.id.rbtFlam);
        rbt[8]= (RadioButton) findViewById(R.id.rbtPastis);

        txtvFactura= (TextView) findViewById(R.id.txtvFactura);
        btnReiniciar= (Button) findViewById(R.id.btnRestart);
        btnFactura= (Button) findViewById(R.id.btnFactura);

        //menu
        chkMacarrons= (CheckBox) findViewById(R.id.chkMacarrons);
        chkCarnForn= (CheckBox) findViewById(R.id.chkCarnForn);

        chkMacarrons.setChecked(true);
        chkCarnForn.setChecked(true);

        //extres
        chkCopa= (CheckBox) findViewById(R.id.chkCopa);
        chkPuro= (CheckBox) findViewById(R.id.chkPuro);
        chkTurro= (CheckBox) findViewById(R.id.chkTurro);
        chkCaramel= (CheckBox) findViewById(R.id.chkCaramels);

        //nom
        txtNom= (EditText) findViewById(R.id.txtNom);


        //colors
        skbA= (SeekBar) findViewById(R.id.skbA);
        skbR= (SeekBar) findViewById(R.id.skbR);
        skbG= (SeekBar) findViewById(R.id.skbG);
        skbB= (SeekBar) findViewById(R.id.skbB);
        llColors= (LinearLayout) findViewById(R.id.llColors);
    }

    public void veureColors(View vista)
    {
        if(llColors.getVisibility()==View.GONE)
        {
            llColors.setVisibility(View.VISIBLE);
        }
        else
            llColors.setVisibility(View.GONE);
    }


    public void ferFactura(View vista)
    {
        factura="Client: "+txtNom.getText().toString()+"\n";
        sumaPreu=0.0;
        if(rbt[0].isChecked()&&rbt[3].isChecked()&&chkCaramel.isChecked())
        {
            factura+="Menú del día \n"
            +rbt[0].getText()+":    -\n"
            +rbt[3].getText()+":    -";
            if(rbt[6].isChecked()) factura+="\n"+rbt[6].getText()+":    -";
            else if(rbt[7].isChecked()) factura+="\n"+rbt[7].getText()+":    -";
            else if(rbt[8].isChecked()) factura+="\n"+rbt[8].getText()+":    -";
            if(chkCopa.isChecked()){ factura+="\n"+chkCopa.getText()+":    "+chkCopa.getTag(); sumaPreu+=Double.parseDouble((String) chkCopa.getTag());}
            if(chkPuro.isChecked()){ factura+="\n"+chkPuro.getText()+":    "+chkPuro.getTag(); sumaPreu+=Double.parseDouble((String) chkPuro.getTag());}
            if(chkTurro.isChecked()){ factura+="\n"+chkTurro.getText()+":    "+chkTurro.getTag(); sumaPreu+=Double.parseDouble((String) chkTurro.getTag());}
            if(chkCaramel.isChecked()){ factura+="\n"+chkCaramel.getText()+":    -";}//arreglar esto

            factura+="\nPreu del menú: "+PREUMENU;
            //+"\nTOTAL: "+PREUMENU+sumaPreu;
            total=0.0;
            total=PREUMENU+sumaPreu;
            total=total+(total*21)/100;
            factura+="\nTOTAL: "+total;

        }
        else if(!rbt[0].isChecked()&&!rbt[1].isChecked()&&!rbt[2].isChecked()&& (rbt[3].isChecked())) {
                 factura+="Plat del dia\n"+ rbt[3].getText()+":    "+Double.parseDouble((String)rbt[3].getTag());
            if(rbt[6].isChecked()) factura+="\n"+rbt[6].getText()+":    -";
            else if(rbt[7].isChecked()) factura+="\n"+rbt[7].getText()+":    -";
            else if(rbt[8].isChecked()) factura+="\n"+rbt[8].getText()+":    -";
            if(chkCopa.isChecked()){ factura+="\n"+chkCopa.getText()+":    "+chkCopa.getTag(); sumaPreu+=Double.parseDouble((String) chkCopa.getTag());}
            if(chkPuro.isChecked()){ factura+="\n"+chkPuro.getText()+":    "+chkPuro.getTag(); sumaPreu+=Double.parseDouble((String) chkPuro.getTag());}
            if(chkTurro.isChecked()){ factura+="\n"+chkTurro.getText()+":    "+chkTurro.getTag(); sumaPreu+=Double.parseDouble((String) chkTurro.getTag());}
            if(chkCaramel.isChecked()){ factura+="\n"+chkCaramel.getText()+":    "+chkCaramel.getTag(); sumaPreu+=Double.parseDouble((String) chkCaramel.getTag());}
            total=0.0;
            total=PLATDIA+sumaPreu;
            total=total+(total*21)/100;
            factura+="\nTOTAL: "+total;
        }

        else if(!rbt[3].isChecked()&&!rbt[4].isChecked()&&!rbt[5].isChecked()&& (rbt[0].isChecked())) {
            factura+="Plat del dia\n"+ rbt[0].getText()+":    "+Double.parseDouble((String)rbt[0].getTag());
            if(rbt[6].isChecked()) factura+="\n"+rbt[6].getText()+":    -";
            else if(rbt[7].isChecked()) factura+="\n"+rbt[7].getText()+":    -";
            else if(rbt[8].isChecked()) factura+="\n"+rbt[8].getText()+":    -";
            if(chkCopa.isChecked()){ factura+="\n"+chkCopa.getText()+":    "+chkCopa.getTag(); sumaPreu+=Double.parseDouble((String) chkCopa.getTag());}
            if(chkPuro.isChecked()){ factura+="\n"+chkPuro.getText()+":    "+chkPuro.getTag(); sumaPreu+=Double.parseDouble((String) chkPuro.getTag());}
            if(chkTurro.isChecked()){ factura+="\n"+chkTurro.getText()+":    "+chkTurro.getTag(); sumaPreu+=Double.parseDouble((String) chkTurro.getTag());}
            if(chkCaramel.isChecked()){ factura+="\n"+chkCaramel.getText()+":    "+chkCaramel.getTag(); sumaPreu+=Double.parseDouble((String) chkCaramel.getTag());}
            total=0.0;
            total=PLATDIA+sumaPreu;
            total=total+(total*21)/100;
            factura+="\nTOTAL: "+total;
        }

        else{
        for (int i=0; i<rbt.length;i++) {

            if (rbt[i].isChecked()) {
                factura+=rbt[i].getText()+":    "+rbt[i].getTag()+"\n";
                sumaPreu += Double.parseDouble((String) rbt[i].getTag());

            }
        }
            if(chkCopa.isChecked()){ factura+=chkCopa.getText()+":    "+chkCopa.getTag(); sumaPreu+=Double.parseDouble((String) chkCopa.getTag());}
            if(chkPuro.isChecked()){ factura+="\n"+chkPuro.getText()+":    "+chkPuro.getTag(); sumaPreu+=Double.parseDouble((String) chkPuro.getTag());}
            if(chkTurro.isChecked()){ factura+="\n"+chkTurro.getText()+":    "+chkTurro.getTag(); sumaPreu+=Double.parseDouble((String) chkTurro.getTag());}
            if(chkCaramel.isChecked()){ factura+="\n"+chkCaramel.getText()+":    "+chkCaramel.getTag(); sumaPreu+=Double.parseDouble((String) chkCaramel.getTag());}
            total=sumaPreu;
            total=total+(total*21)/100;
            factura+="\nTOTAL: "+total+"€";
        }
        txtvFactura.setText(factura);
    }
    public void clickPuro(View vista)
    {
        if(chkPuro.isChecked()) chkCopa.setChecked(true);
    }
    public void clickCopa(View vista)
    {
       if (chkPuro.isChecked()) chkCopa.setChecked(true);
    }
    public void Reiniciar(View vista)
    {
        for(int i=0; i<rbt.length;i++)
        {
            rbt[i].setChecked(false);
        }
    }

}
