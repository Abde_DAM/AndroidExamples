package com.example.informatica.practicaneptuno.Adaptadors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.informatica.practicaneptuno.POJO.DetallesDePedidos;
import com.example.informatica.practicaneptuno.R;

import java.util.List;

/**
 * Created by abde on 1/04/16.
 */
public class AdaptadorDetalles extends BaseAdapter {
    private List<DetallesDePedidos> detalles;
    private Context context;
    LayoutInflater layoutInflater;

    public AdaptadorDetalles(List<DetallesDePedidos> detalles, Context context) {
        this.detalles = detalles;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);

    }
    @Override
    public int getCount() {
        return detalles.size();
    }

    @Override
    public Object getItem(int position) {
        return detalles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return detalles.get(position).getIdPedido();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        /*int idPedido, idProducto, cantidad;
        double precioUnidad, descuento;*/
        View view= layoutInflater.inflate(R.layout.detalles_pedidos_view_adapter, null);
        TextView tvIdProducto= (TextView) view.findViewById(R.id.tvIdProducto);
        TextView tvPrecioPorUnidad= (TextView) view.findViewById(R.id.tvPrecioPorUnidad);
        TextView tvCantidad= (TextView) view.findViewById(R.id.tvCantidad);
        TextView tvDescuento= (TextView) view.findViewById(R.id.tvDescuento);
        TextView tvPrecioConDescuento= (TextView) view.findViewById(R.id.tvPrecioConDescuento);

        tvIdProducto.setText("ID Pedido: "+String.valueOf(detalles.get(position).getIdPedido()));
        tvPrecioPorUnidad.setText("Precio por unidad: "+ String.valueOf(detalles.get(position).getPrecioUnidad())+"€");
        tvCantidad.setText("Cantidad: "+String.valueOf(detalles.get(position).getCantidad()));
        tvDescuento.setText("Descuento del "+String.valueOf(detalles.get(position).getDescuento())+"%");
        double precio=detalles.get(position).getCantidad()*detalles.get(position).getPrecioUnidad(), descuento=detalles.get(position).getDescuento();
        tvPrecioConDescuento.setText("Total con descuento: "+String.valueOf(precio-((precio*descuento)/100))+"€");
        return view;
    }
}









































