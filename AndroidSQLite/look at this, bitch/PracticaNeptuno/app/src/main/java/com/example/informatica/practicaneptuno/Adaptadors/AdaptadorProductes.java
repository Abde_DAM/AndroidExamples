package com.example.informatica.practicaneptuno.Adaptadors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.informatica.practicaneptuno.POJO.Productos;
import com.example.informatica.practicaneptuno.R;

import java.util.List;

/**
 * Created by abde on 29/03/16.
 */
public class AdaptadorProductes extends BaseAdapter {
    List<Productos> productos;
    Context context;
    LayoutInflater layoutInflater;

    public AdaptadorProductes(List<Productos> productos, Context context) {
        this.productos = productos;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return productos.size();
    }

    @Override
    public Object getItem(int position) {
        return productos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return productos.get(position).getIdCategoria();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view= layoutInflater.inflate(R.layout.productes_per_categoria_view_adapter, null);

        TextView tvNombreProducto=(TextView)view.findViewById(R.id.tvNombreProducto);
        TextView tvSuspendido=(TextView)view.findViewById(R.id.tvSuspendido);
        TextView tvCantidadPorUnidad=(TextView)view.findViewById(R.id.tvCantidadPorUnidad);
        TextView tvPrecioPorUnidad=(TextView)view.findViewById(R.id.tvPrecioPorUnidad);


        tvNombreProducto.setText(String.valueOf(productos.get(position).getNombreProducto()));
        tvCantidadPorUnidad.setText(String.valueOf(productos.get(position).getCantidadPorUnidad()));
        tvPrecioPorUnidad.setText(String.valueOf(productos.get(position).getPrecioUnidad()));
        if(productos.get(position).getSuspendido()==1)
        {
            tvSuspendido.setText("SUSPENDIDO");
        }
        else
        {
            tvSuspendido.setText("NO SUSPENDIDO");
        }

        return view;
    }
}
