package com.example.informatica.practicaneptuno;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.informatica.practicaneptuno.DAO.CategoriasDAO;
import com.example.informatica.practicaneptuno.DAO.ClientesDAO;
import com.example.informatica.practicaneptuno.DAO.CompaniasEnvioDAO;
import com.example.informatica.practicaneptuno.DAO.DetallesDePedidosDAO;
import com.example.informatica.practicaneptuno.DAO.EmpleadosDAO;
import com.example.informatica.practicaneptuno.DAO.PedidosDAO;
import com.example.informatica.practicaneptuno.DAO.ProductosDAO;
import com.example.informatica.practicaneptuno.DAO.ProveedoresDAO;
import com.example.informatica.practicaneptuno.Helpers.PrincipalSQLiteHelper;
import com.example.informatica.practicaneptuno.POJO.Categorias;
import com.example.informatica.practicaneptuno.POJO.Clientes;
import com.example.informatica.practicaneptuno.POJO.CompaniasDeEnvio;
import com.example.informatica.practicaneptuno.POJO.DetallesDePedidos;
import com.example.informatica.practicaneptuno.POJO.Empleados;
import com.example.informatica.practicaneptuno.POJO.Pedidos;
import com.example.informatica.practicaneptuno.POJO.Productos;
import com.example.informatica.practicaneptuno.POJO.Proveedores;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    PrincipalSQLiteHelper bdHelper;
    SQLiteDatabase bd;
    ClientesDAO clientesDAO;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bdHelper=new PrincipalSQLiteHelper(this);
        bd=bdHelper.getWritableDatabase();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnuRestaura:
                omple();
                return true;
            case R.id.mnuCategories:
                startActivity(new Intent(this, Categories.class));
                return true;
            case R.id.mnuComandes:
                startActivity(new Intent(this, ComandesActivity.class));
                return true;
            case R.id.mnuProductes:
                startActivity(new Intent(this, ProductesActivity.class));
                return true;
            case R.id.mnuProveidors:
                startActivity(new Intent(this, Proveidors.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void omple() {
        try
        {
            bd.execSQL("DELETE FROM " + Constants.TAULA_PROVEEDORES);
            bd.execSQL("DELETE FROM " + Constants.TAULA_CATEGORIAS);
            bd.execSQL("DELETE FROM " + Constants.TAULA_PRODUCTOS);
            bd.execSQL("DELETE FROM " + Constants.TAULA_EMPLEADOS);
            bd.execSQL("DELETE FROM " + Constants.TAULA_CLIENTES);
            bd.execSQL("DELETE FROM " + Constants.TAULA_COMPANIAS_DE_ENVIO);
            bd.execSQL("DELETE FROM " + Constants.TAULA_PEDIDOS);
            bd.execSQL("DELETE FROM " + Constants.TAULA_DETALLES_PEDIDO);
        }
        catch (Exception e)
        {
            Log.d("NoEsborra", e.getMessage());
        }
        ompleProveedores();
        ompleCategorias();
        ompleProductos();
        ompleEmpleados();
        ompleClientes();
        ompleCompanias();
        omplePedidos();
        ompleDetalles();

    }

    private void ompleProveedores()
    {
        ProveedoresDAO proveedoresDAO = new ProveedoresDAO(this);
        ArrayList<Proveedores> proveedores = Fitxers.obtenerProveedores(this);
        try{
            proveedoresDAO.obre();
            for (Proveedores proveedor:proveedores) {
                proveedoresDAO.creaProveedores(proveedor.getIdProveedor(), proveedor.getNombreCompania(), proveedor.getNombreContacto(), proveedor.getCargoContacto(),
                        proveedor.getDireccion(), proveedor.getCiudad(), proveedor.getRegion(), proveedor.getCodPostal(), proveedor.getPais(),
                        proveedor.getTelefono(), proveedor.getFax(), proveedor.getPaginaPrincipal());
            }
        }
        catch(SQLiteException e)
        {
            Log.e("SQLERROR", "ProveedoresDAO - "+e.getMessage());
        }
        finally {
            proveedoresDAO.tanca();
        }
    }

    private void ompleCategorias()
    {
        CategoriasDAO categoriasDAO = new CategoriasDAO(this);
        ArrayList<Categorias> categorias = Fitxers.obtenerCategorias(this);
        try{
            categoriasDAO.obre();
            for (Categorias categoria:categorias) {
                categoriasDAO.creaCategorias(categoria.getIdCategoria(), categoria.getNombreCategoria(), categoria.getDescripcion(), categoria.getImagen());
            }
        }
        catch(SQLiteException e)
        {
            Log.e("SQLERROR", "CategoriasDAO - "+e.getMessage());
        }
        finally {
            categoriasDAO.tanca();
        }
    }

    private void ompleProductos()
    {
        ProductosDAO productosDAO = new ProductosDAO(this);
        ArrayList<Productos> productos = Fitxers.obtenerProductos(this);
        try{
            productosDAO.obre();
            for (Productos producto:productos) {
                productosDAO.creaProductos(producto.getIdProducto(), producto.getNombreProducto(), producto.getIdProveedor(), producto.getIdCategoria(),
                        producto.getCantidadPorUnidad(), producto.getPrecioUnidad(), producto.getUnidadesEnExistencia(), producto.getUnidadesEnPedido(),
                        producto.getNivelNuevoPedido(), producto.getSuspendido());
            }
        }
        catch(SQLiteException e)
        {
            Log.e("SQLERROR", "ProductosDAO - "+e.getMessage());
        }
        finally {
            productosDAO.tanca();
        }
    }

    private void ompleEmpleados()
    {
        EmpleadosDAO empleadosDAO = new EmpleadosDAO(this);
        ArrayList<Empleados> empleados = Fitxers.obtenerEmpleados(this);
        try{
            empleadosDAO.obre();
            for (Empleados empleado:empleados) {
                empleadosDAO.creaEmpleados(empleado.getIdEmpleado(), empleado.getApellidos(), empleado.getNombre(), empleado.getCargo(),
                        empleado.getTratamiento(), empleado.getFechaNacimiento(), empleado.getFechaContratacion(), empleado.getDireccion(),
                        empleado.getCiudad(), empleado.getRegion(), empleado.getCodPostal(), empleado.getPais(), empleado.getTelDomicilio(),
                        empleado.getExtension(), empleado.getFoto(), empleado.getNotas(), empleado.getJefe());
            }
        }
        catch(SQLiteException e)
        {
            Log.e("SQLERROR", "EmpleadosDAO - "+e.getMessage());
        } finally {
            empleadosDAO.tanca();
        }
    }



    private void ompleClientes() {
        clientesDAO=new ClientesDAO(this);
        ArrayList<Clientes> clientes=Fitxers.obtenerClientes(this);
        try{
            clientesDAO.obre();
            for (Clientes cliente:clientes) {
                clientesDAO.creaCliente(cliente.getIdCliente(), cliente.getNombreCompania(), cliente.getNombreContacto(), cliente.getCargoContacto(), cliente.getDireccion(),cliente.getCiudad(),cliente.getRegion(),cliente.getCodpostal(),cliente.getPais(),cliente.getTelefono(),cliente.getFax());

            }
        }
        catch(SQLiteException e)
        {
            Log.e("SQLERROR", "ClientesDAO - "+e.getMessage());
        }
        finally {
            clientesDAO.tanca();
        }
    }

    private void ompleCompanias()
    {
        CompaniasEnvioDAO companiasEnvioDAO = new CompaniasEnvioDAO(this);
        ArrayList<CompaniasDeEnvio> companias = Fitxers.obtenerCompaniasDeEnvio(this);
        try{
            companiasEnvioDAO.obre();
            for (CompaniasDeEnvio compania:companias) {
                companiasEnvioDAO.creaCompania(compania.getIdCompaniaEnvio(), compania.getNombreCompania(), compania.getTelefono());

            }
        }
        catch(SQLiteException e)
        {
            Log.e("SQLERROR", "CompaniasDAO - "+e.getMessage());
        }
        finally {
            companiasEnvioDAO.tanca();
        }
    }

    private void omplePedidos()
    {
        PedidosDAO pedidosDAO = new PedidosDAO(this);
        ArrayList<Pedidos> pedidos = Fitxers.obtenerPedidos(this);
        try{
            pedidosDAO.obre();
            for (Pedidos pedido:pedidos) {
                pedidosDAO.creaPedido(pedido.getIdPedido(), pedido.getIdCliente(), pedido.getIdEmpleado(), pedido.getFechaPedido(), pedido.getFechaEntrega(),
                        pedido.getFechaEnvio(), pedido.getFormaEnvio(), pedido.getCargo(), pedido.getDestinatario(), pedido.getDireccionDestintatario(),
                        pedido.getCiudadDestinatario(), pedido.getRegionDestinatario(), pedido.getCodpostalDestinatario(), pedido.getRegionDestinatario());

            }
        }
        catch(SQLiteException e)
        {
            Log.e("SQLERROR", "PedidosDAO - "+e.getMessage());
        }
        finally {
            pedidosDAO.tanca();
        }
    }

    private void ompleDetalles()
    {
        DetallesDePedidosDAO detallesDAO = new DetallesDePedidosDAO(this);
        ArrayList<DetallesDePedidos> detalles = Fitxers.obtenerDetallesDePedidos(this);
        try{
            detallesDAO.obre();
            for (DetallesDePedidos detalle:detalles) {
                detallesDAO.creaDetalles(detalle.getIdPedido(), detalle.getIdProducto(),  detalle.getCantidad(),detalle.getPrecioUnidad(),
                        detalle.getDescuento());
            }
        }
        catch(SQLiteException e)
        {
            Log.e("SQLERROR", "DetallesDAO - "+e.getMessage());
        }
        finally {
            detallesDAO.tanca();
        }
    }
}
