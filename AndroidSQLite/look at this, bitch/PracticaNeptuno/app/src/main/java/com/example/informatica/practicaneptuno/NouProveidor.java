package com.example.informatica.practicaneptuno;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.informatica.practicaneptuno.DAO.ProveedoresDAO;
import com.example.informatica.practicaneptuno.POJO.Proveedores;

public class NouProveidor extends AppCompatActivity {
    ProveedoresDAO proveedoresDAO;
    EditText etIdProveidor, etNombreCompania, etNombreContacto, etCargoContacto, etDireccion, etCiudad, etRegion, etCodPostal, etPais, etTelefono, etFax, etPagPrincipal;
    Button btnGuardar;
    Intent intent;
    int idInicial;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nou_proveidor);
        etIdProveidor= (EditText) findViewById(R.id.etIdProveidor);
        etNombreCompania= (EditText) findViewById(R.id.etNombreCompania);
        etNombreContacto= (EditText) findViewById(R.id.etNombreContacto);
        etCargoContacto= (EditText) findViewById(R.id.etCargoContacto);
        etDireccion= (EditText) findViewById(R.id.etDireccion);
        etCiudad= (EditText) findViewById(R.id.etCiudad);
        etRegion= (EditText) findViewById(R.id.etRegion);
        etCodPostal= (EditText) findViewById(R.id.etCodPostal);
        etPais= (EditText) findViewById(R.id.etPais);
        etTelefono= (EditText) findViewById(R.id.etTelefono);
        etFax= (EditText) findViewById(R.id.etFax);
        etPagPrincipal= (EditText) findViewById(R.id.etPagPrincipal);
        btnGuardar= (Button) findViewById(R.id.btnGuardar);

        intent=getIntent();
        if(intent.getBooleanExtra(Proveidors.ESMODIFICAT, false))
        {
            Proveedores proveedores=intent.getParcelableExtra(Proveidors.MODIFICARPROVEIDOR);
            idInicial=proveedores.getIdProveedor();
            etIdProveidor.setText(String.valueOf(proveedores.getIdProveedor()));
            etNombreCompania.setText(proveedores.getNombreCompania());
            etNombreContacto.setText(proveedores.getNombreContacto());
            etCargoContacto.setText(proveedores.getCargoContacto());
            etDireccion.setText(proveedores.getDireccion());
            etCiudad.setText(proveedores.getCiudad());
            etRegion.setText(proveedores.getRegion());
            etCodPostal.setText(proveedores.getCodPostal());
            etPais.setText(proveedores.getPais());
            etTelefono.setText(proveedores.getTelefono());
            etFax.setText(proveedores.getFax());
            etPagPrincipal.setText(proveedores.getPaginaPrincipal());
        }
        proveedoresDAO=new ProveedoresDAO(this);
        proveedoresDAO.obre();

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(intent.getBooleanExtra(Proveidors.ESMODIFICAT, false)) {
                    proveedoresDAO.updateProveedores(idInicial, Integer.parseInt(etIdProveidor.getText().toString()), etNombreCompania.getText().toString(), etNombreContacto.getText().toString(), etCargoContacto.getText().toString(), etDireccion.getText().toString(), etCiudad.getText().toString(),
                            etRegion.getText().toString(), etCodPostal.getText().toString(), etPais.getText().toString(), etTelefono.getText().toString(), etFax.getText().toString(), etPagPrincipal.getText().toString());
                }
                else {
                    proveedoresDAO.creaProveedores(Integer.parseInt(etIdProveidor.getText().toString()), etNombreCompania.getText().toString(), etNombreContacto.getText().toString(), etCargoContacto.getText().toString(), etDireccion.getText().toString(), etCiudad.getText().toString(),
                            etRegion.getText().toString(), etCodPostal.getText().toString(), etPais.getText().toString(), etTelefono.getText().toString(), etFax.getText().toString(), etPagPrincipal.getText().toString());
                }
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        proveedoresDAO.tanca();
        super.onDestroy();
    }
}
