package com.example.informatica.practicaneptuno;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.example.informatica.practicaneptuno.Adaptadors.AdaptadorDetalles;
import com.example.informatica.practicaneptuno.DAO.DetallesDePedidosDAO;
import com.example.informatica.practicaneptuno.POJO.DetallesDePedidos;

import java.util.ArrayList;

public class DetallePedidoActivity extends AppCompatActivity {
    ListView lvProductes;
    TextView tvTotal, tvGastosEnvio,tvPrecioFinal;
    DetallesDePedidosDAO detallesDePedidosDAO;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_pedido);
        double total, gastosEnvio, precioFinal;
        tvTotal= (TextView) findViewById(R.id.tvTotal);
        tvGastosEnvio= (TextView) findViewById(R.id.tvGastosEnvio);
        tvPrecioFinal= (TextView) findViewById(R.id.tvPrecioFinal);
        Intent intent=getIntent();
        detallesDePedidosDAO =new DetallesDePedidosDAO(this);
        detallesDePedidosDAO.obre();
        ArrayList<DetallesDePedidos> detalles= (ArrayList<DetallesDePedidos>) detallesDePedidosDAO.obteDetallesPorPedido((int) intent.getLongExtra(ComandesActivity.IDPEDIDO, -1));
        lvProductes= (ListView) findViewById(R.id.lvDetalles);
        lvProductes.setAdapter(new AdaptadorDetalles(detalles, this));
        total=detallesDePedidosDAO.Total((int) intent.getLongExtra(ComandesActivity.IDPEDIDO, -1));
        tvTotal.setText("Total: "+String.valueOf(total)+"€");
        gastosEnvio=total-intent.getDoubleExtra(ComandesActivity.CARGO, -1);
        //TODO: Finish this shit
        tvGastosEnvio.setText("Gastos de envío: "+String.valueOf(gastosEnvio));
        tvPrecioFinal.setText("Precio final: "+String.valueOf(intent.getDoubleExtra(ComandesActivity.CARGO, -1)));
    }

    @Override
    protected void onDestroy() {
        detallesDePedidosDAO.tanca();
        super.onDestroy();
    }
}
