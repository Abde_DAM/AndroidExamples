package com.example.informatica.practicaneptuno.Helpers;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;

/**
 * Created by Informatica on 25/03/2016.
 */
public class CompaniasSQLiteHelper extends SQLiteOpenHelper {
    String sqlCreateClientes="CREATE TABLE "+ Constants.TAULA_COMPANIAS_DE_ENVIO+" ("+ Constants.COLUMNA_ID+" integer primary key autoincrement, "+ Constants.COMPANIAS_ID_COMPANIA+" integer not null, "+ Constants.COMPANIAS_NOMBRE_COMPANIA+" TEXT, "+ Constants.COMPANIAS_TELEFONO+" TEXT);";
    public CompaniasSQLiteHelper(Context context)
    {
        super(context, Constants.NOM_DB, null, Constants.VERSIO_DB);
    }

    public CompaniasSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public CompaniasSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreateClientes);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
