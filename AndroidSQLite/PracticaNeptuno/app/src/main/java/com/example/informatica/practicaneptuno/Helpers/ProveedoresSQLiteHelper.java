package com.example.informatica.practicaneptuno.Helpers;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;

/**
 * Created by Informatica on 25/03/2016.
 */
public class ProveedoresSQLiteHelper extends SQLiteOpenHelper{
    String sqlCreateProveedores="CREATE TABLE "+ Constants.TAULA_PROVEEDORES+" ("+ Constants.COLUMNA_ID+" integer primary key autoincrement, "+ Constants.PROVEEDORES_ID_PROVEEDOR+" integer not null, "+ Constants.PROVEEDORES_NOMBRE_COMPANIA+" TEXT, "+ Constants.PROVEEDORES_NOMBRE_CONTACTO+" TEXT, "
            + Constants.PROVEEDORES_CARGO_CONTACTO+" TEXT, "+ Constants.PROVEEDORES_DIRECCION+" TEXT, " + Constants.PROVEEDORES_CIUDAD+ " TEXT, " + Constants.PROVEEDORES_REGION+" TEXT, "+ Constants.PROVEEDORES_COD_POSTAL+" TEXT, "+ Constants.PROVEEDORES_PAIS+" TEXT, "+ Constants.PROVEEDORES_TELEFONO+" TEXT, "+ Constants.PROVEEDORES_FAX+" TEXT, " +
            Constants.PROVEEDORES_PAGINA_PRINCIPAL+" TEXT);";

    public ProveedoresSQLiteHelper(Context context)
    {
        super(context, Constants.NOM_DB, null, Constants.VERSIO_DB);
    }

    public ProveedoresSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public ProveedoresSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreateProveedores);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}