package com.example.informatica.practicaneptuno.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;
import com.example.informatica.practicaneptuno.Helpers.PedidosSQLiteHelper;
import com.example.informatica.practicaneptuno.POJO.Pedidos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Informatica on 25/03/2016.
 */
public class PedidosDAO {
    private SQLiteDatabase bd;
    private SQLiteOpenHelper bdHelper;
    private String[] columnes={
            Constants.COLUMNA_ID,
            Constants.PEDIDOS_ID_PEDIDO,
            Constants.PEDIDOS_ID_CLIENTE,
            Constants.PEDIDOS_ID_EMPLEADO,
            Constants.PEDIDOS_FECHA_PEDIDO,
            Constants.PEDIDOS_FECHA_ENTREGA,
            Constants.PEDIDOS_FECHA_ENVIO,
            Constants.PEDIDOS_FORMA_ENVIO,
            Constants.PEDIDOS_CARGO,
            Constants.PEDIDOS_DESTINATARIO,
            Constants.PEDIDOS_DIRECCION_DESTINATARIO,
            Constants.PEDIDOS_CIUDAD_DESTINATARIO,
            Constants.PEDIDOS_REGION_DESTINATARIO,
            Constants.PEDIDOS_CODPOSTAL_DESTINATARIO,
            Constants.PEDIDOS_PAIS_DESTINATARIO
    };

    public PedidosDAO(Context context)
    {
        bdHelper=new PedidosSQLiteHelper(context);
    }

    public void obre()
    {
        bd=bdHelper.getWritableDatabase();
    }

    public void tanca()
    {
        bd.close();
    }

    public Pedidos cursorAPedido(Cursor cursor)
    {
        return new Pedidos(cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getDouble(8), cursor.getString(9), cursor.getString(10),cursor.getString(11),cursor.getString(12), cursor.getString(13), cursor.getString(14));
    }

    public Pedidos creaPedido(int idPedido, int idCliente, int idEmpleado, String fechaPedido, String fechaEntrega, String fechaEnvio, String formaEnvio, Double cargo, String destinatario, String direccionDestinatario, String ciudadDestinatario, String regionDestinatario, String codpostalDestinatario, String paisDestinatario)
    {
        ContentValues valors = new ContentValues();
        valors.put(Constants.PEDIDOS_ID_PEDIDO, idPedido);
        valors.put(Constants.PEDIDOS_ID_CLIENTE, idCliente);
        valors.put(Constants.PEDIDOS_ID_EMPLEADO, idEmpleado);
        valors.put(Constants.PEDIDOS_FECHA_PEDIDO, fechaPedido);
        valors.put(Constants.PEDIDOS_FECHA_ENTREGA, fechaEntrega);
        valors.put(Constants.PEDIDOS_FECHA_ENVIO, fechaEnvio);
        valors.put(Constants.PEDIDOS_FORMA_ENVIO, formaEnvio);
        valors.put(Constants.PEDIDOS_CARGO, cargo);
        valors.put(Constants.PEDIDOS_DESTINATARIO, destinatario);
        valors.put(Constants.PEDIDOS_DIRECCION_DESTINATARIO, direccionDestinatario);
        valors.put(Constants.PEDIDOS_CIUDAD_DESTINATARIO, ciudadDestinatario);
        valors.put(Constants.PEDIDOS_REGION_DESTINATARIO, regionDestinatario);
        valors.put(Constants.PEDIDOS_CODPOSTAL_DESTINATARIO, codpostalDestinatario);
        valors.put(Constants.PEDIDOS_PAIS_DESTINATARIO, paisDestinatario);

        long idInsercio = bd.insert(Constants.TAULA_PEDIDOS, null, valors);
        if(idInsercio == -1)
            return null;
        else
        {
            Cursor cursor = bd.query(Constants.TAULA_PEDIDOS, columnes, Constants.PEDIDOS_ID_PEDIDO+ " = '" + idPedido + "'",
                    null, null, null, null);

            cursor.moveToFirst();

            Pedidos pedido = cursorAPedido(cursor);
            cursor.close();
            return pedido;
        }
    }
    public boolean eliminaPedido(Pedidos pedido)
    {
        int nEsborrats=bd.delete(
                Constants.TAULA_PEDIDOS,
                Constants.PEDIDOS_ID_PEDIDO+" = '"
                        +pedido.getIdPedido()+"'", null);
        return nEsborrats>0;
    }

    public List<Pedidos> obtePedidos()
    {
        List<Pedidos> pedidos=new ArrayList<Pedidos>();

        Cursor cursor=bd.query(
                Constants.TAULA_PEDIDOS,
                columnes,
                null, null, null, null, null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            Pedidos pedido= cursorAPedido(cursor);
            pedidos.add(pedido);
            cursor.moveToNext();
        }

        return  pedidos;
    }
}
