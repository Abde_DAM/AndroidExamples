package com.example.informatica.practicaneptuno.POJO;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Informatica on 25/03/2016.
 */
public class CompaniasDeEnvio implements Parcelable
{
    int idCompaniaEnvio;
    String nombreCompania, telefono;

    public CompaniasDeEnvio() {
    }

    public CompaniasDeEnvio(int idCompaniaEnvio, String nombreCompania, String telefono) {
        this.idCompaniaEnvio = idCompaniaEnvio;
        this.nombreCompania = nombreCompania;
        this.telefono = telefono;
    }

    public int getIdCompaniaEnvio() {
        return idCompaniaEnvio;
    }

    public void setIdCompaniaEnvio(int idCompaniaEnvio) {
        this.idCompaniaEnvio = idCompaniaEnvio;
    }

    public String getNombreCompania() {
        return nombreCompania;
    }

    public void setNombreCompania(String nombreCompania) {
        this.nombreCompania = nombreCompania;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "CompaniasDeEnvio{" +
                "idCompaniaEnvio=" + idCompaniaEnvio +
                ", nombreCompania='" + nombreCompania + '\'' +
                ", telefono='" + telefono + '\'' +
                '}';
    }

    /*************** Parcelable ***********/
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idCompaniaEnvio);
        dest.writeString(nombreCompania);
        dest.writeString(telefono);
    }

    public void readFromParcel (Parcel dades)
    {
        idCompaniaEnvio = dades.readInt();
        nombreCompania = dades.readString();
        telefono = dades.readString();
    }
    public CompaniasDeEnvio(Parcel dades)
    {
        readFromParcel(dades);
    }

    public final static Parcelable.Creator<CompaniasDeEnvio> CREATOR = new Creator<CompaniasDeEnvio>() {
        @Override
        public CompaniasDeEnvio createFromParcel(Parcel source) {
            return new CompaniasDeEnvio(source);
        }

        @Override
        public CompaniasDeEnvio[] newArray(int size) {
            return new CompaniasDeEnvio[size];
        }
    };
}