package com.example.informatica.practicaneptuno.Helpers;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;

/**
 * Created by Informatica on 25/03/2016.
 */
public class ProductosSQLiteHelper extends SQLiteOpenHelper {
    String sqlCreateProductos="CREATE TABLE "+ Constants.TAULA_PRODUCTOS+" ("+ Constants.COLUMNA_ID+" integer primary key autoincrement, "+ Constants.PRODUCTOS_ID_PRODUCTO+" integer not null, "+ Constants.PRODUCTOS_NOMBRE_PRODUCTO+" TEXT, "+ Constants.PRODUCTOS_ID_PROVEEDOR+" integer not null, "
            + Constants.PRODUCTOS_ID_CATEGORIA+" integer not null, " + Constants.PRODUCTOS_CANTIDAD_POR_UNIDAD + " TEXT," + Constants.PRODUCTOS_PRECIO_UNIDAD + " REAL," + Constants.PRODUCTOS_UNIDADES_EN_EXISTENCIA + " integer," +
            Constants.PRODUCTOS_UNIDADES_EN_PEDIDO + " integer," + Constants.PRODUCTOS_NIVEL_NUEVO_PEDIDO + " integer," + Constants.PRODUCTOS_SUSPENDIDO + " integer);";

    public ProductosSQLiteHelper(Context context)
    {
        super(context, Constants.NOM_DB, null, Constants.VERSIO_DB);
    }

    public ProductosSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public ProductosSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreateProductos);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}