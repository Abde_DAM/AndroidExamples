package com.example.informatica.practicaneptuno.Helpers;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.informatica.practicaneptuno.Constants;

/**
 * Created by Informatica on 25/03/2016.
 */
public class EmpleadosSQLiteHelper extends SQLiteOpenHelper {
    String sqlCreateEmpleados="CREATE TABLE "+ Constants.TAULA_EMPLEADOS+" ("+ Constants.COLUMNA_ID+" integer primary key autoincrement, "+ Constants.EMPLEADOS_ID_EMPLEADO+" integer not null, "+ Constants.EMPLEADOS_APELLIDOS+" TEXT, "+ Constants.EMPLEADOS_NOMBRE+" TEXT, "
            + Constants.EMPLEADOS_CARGO+" TEXT, "+ Constants.EMPLEADOS_TRATAMIENTO+" TEXT, " + Constants.EMPLEADOS_FECHA_NACIMIENTO+ " TEXT, " + Constants.EMPLEADOS_FECHA_CONTRATACION+" TEXT, "+ Constants.EMPLEADOS_DIRECCION+" TEXT, "+ Constants.EMPLEADOS_CIUDAD+" TEXT, "+ Constants.EMPLEADOS_REGION+" TEXT, "+ Constants.EMPLEADOS_COD_POSTAL+" TEXT, " +
            Constants.EMPLEADOS_PAIS+" TEXT, " + Constants.EMPLEADOS_TEL_DOMICILIO + " TEXT, " + Constants.EMPLEADOS_EXTENSION + " TEXT, " + Constants.EMPLEADOS_FOTO + " TEXT, " + Constants.EMPLEADOS_NOTAS + " TEXT, " + Constants.EMPLEADOS_JEFE + " integer);";

    public EmpleadosSQLiteHelper(Context context)
    {
        super(context, Constants.NOM_DB, null, Constants.VERSIO_DB);
    }

    public EmpleadosSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public EmpleadosSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreateEmpleados);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}